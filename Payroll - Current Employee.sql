/*
Hi jon if you have time can you create a report for me it is short all I need it name and original date of hire only current employees and all 3 stores by store
*/
select ymco# as Store, ymname as Name, 
  case 
    when cast(right(trim(ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '20'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '20'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '19'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '19'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    end as "Orig Hire Date",
  case ymactv
    when  'A' then 'Full Time'
    when  'P' then 'Part Time'
  end 
from rydedata.pymast p
where ymactv <> 'T'
and ymco# in ('RY1','RY2','RY3')
order by Store, Name


