select trim(ptinv#), sum(ptqty*ptnet)
from rydedata.pdptdet a
where a.ptdate > 20131000
  and left(trim(ptinv#), 2) = '18'
group by  trim(ptinv#)


select b.date, a.ro, a.sales, b.tax, 100.00 * b.tax/a.sales
from (
  select trim(ptinv#) as ro, sum(ptqty*ptnet) as sales
  from rydedata.pdptdet a
  where a.ptdate > 20130100
    and left(trim(ptinv#), 2) = '18'
  group by trim(ptinv#)) a
inner join (
select trim(gtdoc#) as ro, sum(abs(coalesce(gttamt, 0))) as tax, max(gtdate) as date
  from rydedata.glptrns
  where gtdate > '01/01/2013'
    and left(trim(gtdoc#),2) = '18'
    and trim(gtacct) = '132400'
  group by trim(gtdoc#))b on a.ro = b.ro
where a.sales > 2500
order by a.ro
    


select *
from rydedata.pdptdet a
where trim(ptinv#) = '18025302'

/*  PDPTDET */
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

-- this ain't working
select ptinv#, ptline, sum(ptqty*ptnet)
-- select *
from rydedata.pdptdet a
where trim(ptinv#) = '18025302'
  and exists ( -- account for removed part, ptcode SR
    select 1
    from rydedata.pdptdet
    where trim(ptinv#) = trim(a.ptinv#)
      AND ptcode = 'CP'
      and ptcode <> 'DP') -- order by ptpart
  and ptcode <> 'DP'
  and ptcode = 'CP'
group by ptinv#, ptline


select ptinv#, ptline, sum(ptqty*ptnet)
-- select *
from rydedata.pdptdet a
where trim(ptinv#) = '18025302'
  and ptline < 900
group by ptinv#, ptline

where exists a part# with ptcode = CP
sum by part#

-- this works for this one ro
select ptinv#, sum(sales) as sales
from (
select ptinv#, ptpart, sum(ptqty*ptnet) as sales
-- select *
from rydedata.pdptdet a
where trim(ptinv#) = '18025302'
  and ptline < 900
  and exists (
    select 1
    from rydedata.pdptdet
    where trim(ptinv#) = trim(ptinv#)
    and ptcode = 'CP')
group by ptinv#,ptpart) x group by ptinv#


select ro, sum(sales) as sales
from (
  select trim(ptinv#) as ro, ptpart, sum(ptqty*ptnet) as sales
  -- select *
  from rydedata.pdptdet a
  where left(trim(ptinv#), 2) = '18'
    and ptdate > 20130900
    and ptline < 900
    and exists (
      select 1
      from rydedata.pdptdet
      where trim(ptinv#) = trim(ptinv#)
      and ptcode = 'CP')
  group by trim(ptinv#) ,ptpart) aa
group by ro

-- need to add paint and materials acct 147900
select b.*, coalesce(c.pMat, 0) as pMat
from ( -- 1 row per ro
  select ro, sum(sales) as sales
  from (
    select trim(ptinv#) as ro, ptpart, sum(ptqty*ptnet) as sales
    -- select *
    from rydedata.pdptdet a
    where left(trim(ptinv#), 2) = '18'
      and ptdate > 20130900
      and ptline < 900
      and exists (
        select 1
        from rydedata.pdptdet
        where trim(ptinv#) = trim(ptinv#)
        and ptcode = 'CP')
    group by trim(ptinv#) ,ptpart) aa
  group by ro) b
left join ( -- 1 row per row
  select trim(gtdoc#) as ro, abs(sum(gttamt)) as pMat
  from rydedata.glptrns
  WHERE trim(gtacct) = '147900'
    and gtdate > '08/31/2013'
  group by trim(gtdoc#)) c on b.ro = c.ro

-- ok, this this is good for sales + paint/materials
--select count(*) from (
select ro, sum(sales + pMat) as sales
from (
  select b.*, coalesce(c.pMat, 0) as pMat
  from ( -- 1 row per ro
    select ro, sum(sales) as sales
    from (
      select trim(ptinv#) as ro, ptpart, sum(ptqty*ptnet) as sales
      -- select *
      from rydedata.pdptdet a
      where left(trim(ptinv#), 2) = '18'
        and ptdate > 20130600
        and ptline < 900
        and exists (
          select 1
          from rydedata.pdptdet
          where trim(ptinv#) = trim(ptinv#)
          and ptcode = 'CP')
      group by trim(ptinv#) ,ptpart) aa
    group by ro) b
  left join ( -- 1 row per row
    select trim(gtdoc#) as ro, abs(sum(gttamt)) as pMat
    from rydedata.glptrns
    WHERE trim(gtacct) = '147900'
      and gtdate > '05/31/2013'
    group by trim(gtdoc#)) c on b.ro = c.ro) d
group by ro having sum(sales + pMat) > 2500
--) x

-- tax
select trim(gtdoc#) as ro, sum(abs(coalesce(gttamt, 0))) as tax
from rydedata.glptrns
where gtdate > '05/31/2013'
  and left(trim(gtdoc#),2) = '18'
  and trim(gtacct) = '132400'
group by trim(gtdoc#)

-- this looks good
--select count(*) from (
select e.ro, sales, coalesce(tax, 0) as tax, 100.00 * coalesce(tax, 0)/sales as "tax %"
from (
  select ro, sum(sales + pMat) as sales
  from (
    select b.*, coalesce(c.pMat, 0) as pMat
    from ( -- 1 row per ro
      select ro, sum(sales) as sales
      from (
        select trim(ptinv#) as ro, ptpart, sum(ptqty*ptnet) as sales
        -- select *
        from rydedata.pdptdet a
        where left(trim(ptinv#), 2) = '18'
          and ptdate > 20130000
          and ptline < 900
          and exists (
            select 1
            from rydedata.pdptdet
            where trim(ptinv#) = trim(ptinv#)
            and ptcode = 'CP')
        group by trim(ptinv#) ,ptpart) aa
      group by ro) b
    left join ( -- 1 row per row
      select trim(gtdoc#) as ro, abs(sum(gttamt)) as pMat
      from rydedata.glptrns
      WHERE trim(gtacct) = '147900'
        and gtdate > '12/31/2012'
      group by trim(gtdoc#)) c on b.ro = c.ro) d
  group by ro having sum(sales + pMat) > 2500) e
left join (
  select trim(gtdoc#) as ro, sum(abs(gttamt)) as tax
  from rydedata.glptrns
  where gtdate > '12/31/2012'
    and left(trim(gtdoc#),2) = '18'
    and trim(gtacct) = '132400'
  group by trim(gtdoc#)) f on e.ro = f.ro 
--) x
order by e.ro

