Select data_type, count(*)
from QSYS2.syscolumns
where table_schema = 'RYDEDATA'
group by data_type


select column_name, column_text, data_type, ordinal_position
-- select *
from QSYS2.syscolumns
where table_schema = 'RYDEDATA'
  and trim(table_name) = 'PYHSHDTA'
  and data_type = 'DECIMAL'
order by ordinal_position

select *
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
and trim(data_type) = 'CLOB'


select *
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
and trim(data_type) = 'TIME'

select * from rydedata.glpjrnd

select *
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
and upper(column_text) like '%DATE%'


select *
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
and upper(column_text) like '%AMT%'


select gttrn#, gtseq#, gtdate, gtrdate, gtsdate, gttamt, gtcost
from rydedata.glptrns
where year(gtdate) = 2011

select gttrn#, gtseq#, gtdate, gtrdate, gtsdate, gttamt, gtcost
from rydedata.glptrns
order by gtdate

select *
from rydedata.glptrns
where gttrn# in (1004110, 1381112, 1393841, 1451463, 1519985)


select gttrn#, gtseq#, gtdate, gtrdate, gtsdate, gttamt, gtcost
from rydedata.glptrns
where gtpost = 'Y'
order by gttamt

select count(*) from rydedata.glptrns where gtpost <> 'Y'

select max(gtseq#) from rydedata.glptrns

select gtsdate, count(*)
from rydedata.glptrns
where gtpost = 'Y'
group by gtsdate
order by count(*)


select gmstyp, count(*)
from rydedata.glpmast
group by gmstyp

select *
from rydedata.glpmast
where gmstyp = ''

select length(trim(gttrn#)), count(*)
from rydedata.glptrns
where gtpost = 'Y'
group by length(trim(gttrn#))

-- 11/26 
-- extract data type analysis
-- these are the non char fields
select gttrn#, gtseq#, -- decimal to integer
  gtdate, gtrdate, gtsdate, -- date to date
  gttamt, gtcost -- decimal to money
from rydedata.glptrns
where gtpost = 'Y'
-- I. decimal to integer 
-- A) min/max
select min(gtseq#), max(gtseq#) 
from rydedata.glptrns
where gtpost = 'Y'
select min(gttrn#), max(gttrn#) 
from rydedata.glptrns
where gtpost = 'Y'
-- B) null/mty
select gttrn#, gtseq#, gtdate, gtrdate, gtsdate, gttamt, gtcost
from rydedata.glptrns
where gtpost = 'Y'
  and gttrn# = 0 
-- C) magic number representation of null
select gtseq#, count(gtseq#) as H
from rydedata.glptrns
where gtpost = 'Y'
group by gtseq#
order by H desc
-- II. date to date
select gtdate, gtrdate, gtsdate, -- date to date
from rydedata.glptrns
where gtpost = 'Y'
-- A) null/mty
select gttrn#, gtseq#, gtdate, gtrdate, gtsdate, gttamt, gtcost
from rydedata.glptrns
where gtpost = 'Y'
--order by gtdate desc
--order by gtrdate desc
order by gtsdate desc
-- B) magic number representation of null
select gtdate, count(*) as H
from rydedata.glptrns
where gtpost = 'Y'
group by gtdate
order by H desc
-- III. dec to money
select gttamt, gtcost -- decimal to money
from rydedata.glptrns
where gtpost = 'Y'
-- A). min/max
select min(gttamt), max(gttamt)
from rydedata.glptrns
where gtpost = 'Y'

select count(*)
from rydedata.glptrns
where gtpost = 'Y'
  and year(gtdate) = 2009
  and month(gtdate) < 8

select year(gtdate), month(gtdate), count(*)
from rydedata.glptrns
where gtpost = 'Y'
group by year(gtdate), month(gtdate)
order by year(gtdate), month(gtdate)

select count(*)
from rydedata.glptrns
where gtpost = 'Y'
and year(gtdate) <= 2009
and month(gtdate) < 7



-- unique columns
select gtco#, gttrn#, gtseq#
from rydedata.glptrns
where gtpost = 'Y'
group by gtco#, gttrn#, gtseq#
having count(*) > 1

select *
from rydedata.glptrns g
inner join (
    select gtco#, gttrn#, gtseq#, gttamt
    from rydedata.glptrns
    where gtpost = 'Y'
    group by gtco#, gttrn#, gtseq#, gttamt
    having count(*) > 1) x on g.gtco# = x.gtco#
  and g.gttrn# = x.gttrn#
  and g.gtseq# = x.gtseq#
where gtpost = 'Y'
order by g.gtco#, g.gttrn# desc, g.gtseq# desc

11-27
select count(*) -- 5077137
from rydedata.glptrns


select count(*)
from rydedata.glptrns
where gtpost = 'Y'
and year(gtdate) = 2011
and month(gtdate) = 11
and day(gtdate) <> 28

select year(gtdate), month(gtdate), day(gtdate), count(*)
from rydedata.glptrns
where gtpost = 'Y'
and year(gtdate) = 2011
and month(gtdate) = 11
group by year(gtdate), month(gtdate), day(gtdate)


order by year(gtdate), month(gtdate)

-------------------------------------------------------------------------------------------------------
-- GLPDTIM Date/Timestamp for Transactions
-------------------------------------------------------------------------------------------------------
select *
from rydedata.glpdtim
where trim(gqtrn#)= '1746917'

select count(*) 
from rydedata.glpdtim

select gqtrn#, count(*)
from rydedata.glpdtim
group by gqtrn#

-- duplicate transaction numbers
select *
from rydedata.glpdtim g 
inner join (
    select gqtrn#
    from rydedata.glpdtim
    group by gqtrn#) d on g.gqtrn# = d.gqtrn#
order by  g.gqtrn#
-- gqtrn# = 0
select count(*) -- 9595
from rydedata.glpdtim
where gqtrn# = 0

select min(gqdate), max(gqdate)
from rydedata.glpdtim
where gqtrn# = 0

select *
from rydedata.glpdtim t

where t.gqdate > 20111100
and t.gqtrn# = 0

-- of the valid glptrns records (gtpost = 'Y')
-- are there duplicate transaction numbers in glpdtim
-- glptrns, gttrn#/gtseq# unique for 
select gttrn#, gtseq#--, gttamt
from rydedata.glptrns
where gtpost = 'Y'
  and year(gtdate) = 2011
group by gttrn#, gtseq#--, gttamt 
  having count(*) > 1

-- god damnit, why are there duplicates
select *
from rydedata.glptrns
where (
    (gttrn# = 1536975 and gtseq# = 1)
  or
    (gttrn# = 1563205 and gtseq# = 1)
  or
    (gttrn# = 1536975 and gtseq# = 2))
--  or
--    (gttrn# = 1169305 and gtseq# in (1,2))
--  or
--    (gttrn# = -555555555 and gtseq# in (/*0,*/13,14,15,16,17,18,19,20)))
order by gttrn#, gtseq#

-- maybe it doesn't matter, i'm using glpdtim for scrape and extract, so the old history doesn't matter
-- 
select *
from rydedata.glpdtim

-- are there duplicate glpdtim records for valid glptrns records

select 
from (
  select gttrn#, gtseq#
  from rydedata.glptrns
  where gtpost = 'Y'
    and year(gtdate) = 2011) g
left join glpdtim t on trim(g.gttrn#) = trim(t.gqtrn#)

-- get time for all transaction from 11/28
select *
from rydedata.glptrns g
left join rydedata.glpdtim t on trim(g.gttrn#) = trim(t.gqtrn#)
where g.gtpost = 'Y'
  and year(g.gtdate) = 2011
  and month(g.gtdate) = 11
  and day(g.gtdate) = 28
order by t.gqtime

select count(*)
from rydedata.glpdtim
where gqdate > 20111125

-- min/max values
select *
from rydedata.glpdtim
order by gqdate desc

10101/0

select count(*)
from rydedata.glpdtim
where gqdate = 10101
-- all 10101 records have no matching record in glptrns
select *
from rydedata.glpdtim t
left join rydedata.glptrns g on trim(t.gqtrn#) = trim(g.gttrn#)
where t.gqdate = 10101

select count(*)
from rydedata.glpdtim t
where not exists (
  select 1
  from rydedata.glptrns
  where trim(gttrn#) = trim(t.gqtrn#))

select t.gqtrn#, count(*)
from rydedata.glpdtim t
where exists (
  select 1
  from rydedata.glptrns
  where trim(gttrn#) = trim(t.gqtrn#))
group by t.gqtrn#
  having count(*) > 1

-- all records from glpdtim, that having a matching record in glptrns (transaction #)
-- that have more than one record per gqtrn#
select t.*, g.*
from rydedata.glpdtim t
left join rydedata.glptrns g on trim(t.gqtrn#) = trim(g.gttrn#)
where trim(gqtrn#) in (
  select trim(t.gqtrn#) 
  from rydedata.glpdtim t
  where exists (
    select 1
    from rydedata.glptrns
    where trim(gttrn#) = trim(t.gqtrn#))
  group by t.gqtrn#
    having count(*) > 1)
order by t.gqdate desc

select gquser, count(*)
from rydedata.glpdtim
group by gquser
order by count(*) desc

-- valid rows in glptrns with no associated record in glpdtim
select count(*)
from rydedata.glptrns g
where gtpost = 'Y'
  and not exists(
    select 1
    from rydedata.glpdtim
    where trim(gqtrn#) = trim(g.gttrn#))

-------- **************** ----------
-- this matters, because if the glpdtim is going to be stored in the same table as the transaction,
-- that transaction/seq combination can have more than one value
-- so do we use the max datetime
-- or is a separate record for the multiple instances of the transaction with the different datetime
 values of gqfunc: what the fuck do they mean
select gqfunc as value, count(*) as count
from rydedata.glpdtim
group by gqfunc
order by gqfunc

select *
from rydedata.glpdtim t
left join rydedata.glptrns g on trim(t.gqtrn#) = trim(g.gttrn#)
where gqfunc = 'P'
order by t.gqdate

-- what is the meaning of records where gqtrn# = 0
select *
from rydedata.glpdtim
where gqtrn# = 0

select gqdate, count(*)
from rydedata.glpdtim
where gqtime = 0
group by gqdate
-- no glptrns records with gttrn# = 0
select *
from rydedata.glptrns
where gttrn# = 0
order by gtdate desc

-- min/max time values
select min(gqtime), max(gqtime)
from rydedata.glpdtim
where gqtime <> 0

-- min/max date values
select min(gqdate), max(gqdate)
from rydedata.glpdtim
where gqtime <> 0

-------------------------------------------------------------------------------------------------------
-- GLPDTIM Date/Timestamp for Transactions ******** END ***********
-------------------------------------------------------------------------------------------------------

-- all relevant glptrns records or for RY1
select gtco#, count(*) from rydedata.glptrns where gtpost = 'Y' group by gtco#
u

select *
from rydedata.glptrns
where gtco# in ('SCT',' SC1')

select 
  case when gomn01 = 'Y' then 1 end,
  case when gomn11 'Y' then 11



select *
from rydedata.glptrns g
inner join (
  select distinct goyear
  from rydedata.glpopyr
  where (
    gomn01 = 'Y' or  gomn02 = 'Y' or gomn03 = 'Y' or gomn04 = 'Y'
    or gomn05 = 'Y' or gomn06 = 'Y' or gomn07 = 'Y' or gomn08 = 'Y'
    or gomn09 = 'Y' or  gomn10 = 'Y' or gomn11 = 'Y' or  gomn12 = 'Y')) y on year(g.gtdate) = y.goyear 
order by g.gtdate desc


select gomn01,gomn03, gomn11, gomn12
from rydedata.glpopyr
where (
    gomn01 = 'Y' or  gomn02 = 'Y' or gomn03 = 'Y' or gomn04 = 'Y'
    or gomn05 = 'Y' or gomn06 = 'Y' or gomn07 = 'Y' or gomn08 = 'Y'
    or gomn09 = 'Y' or  gomn10 = 'Y' or gomn11 = 'Y' or  gomn12 = 'Y')
  and goco# = 'RY1'

-- this isn't fucking getting me anywhere
-- need the open year/month combinations
-- year is easy, month is kicking my ass
if gomn11 = yes then

-- select distinct year(gtdate), month(gtdate)
select *
from rydedata.glptrns g
inner join (
  select distinct goyear
  from rydedata.glpopyr
  where (
      gomn01 = 'Y' or  gomn02 = 'Y' or gomn03 = 'Y' or gomn04 = 'Y'
      or gomn05 = 'Y' or gomn06 = 'Y' or gomn07 = 'Y' or gomn08 = 'Y'
      or gomn09 = 'Y' or  gomn10 = 'Y' or gomn11 = 'Y' or  gomn12 = 'Y')
    and goco# = 'RY1') y on year(g.gtdate) = y.goyear 
where gtpost = 'Y'
  and gtco# = 'RY1'
  and (
    month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn01 = 'Y') > 0 then 1 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn02 = 'Y') > 0 then 2 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn03 = 'Y') > 0 then 3 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn04 = 'Y') > 0 then 4 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn05 = 'Y') > 0 then 5 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn06 = 'Y') > 0 then 6 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn07 = 'Y') > 0 then 7 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn08 = 'Y') > 0 then 8 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn09 = 'Y') > 0 then 9 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn10 = 'Y') > 0 then 10 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn11 = 'Y') > 0 then 11 end
    or month(gtdate) = case when (select count(*) from rydedata.glpopyr where gomn12 = 'Y') > 0 then 12 end)
  



select *
from rydedata.glptrns
where year(gtdate) = 2011
and month(gtdate) = 12


select gtdtyp, count(*)
from rydedata.glptrns
group by gtdtyp


-- 12/3/11

select g.*, gqdate* 10000 + gqtime
from rydedata.glpdtim g 
order by gqdate*10000 + gqtime desc


select *
from rydedata.glpdtim
where gqtrn# = 1754313

-- 12/4/11
-- aahhh, the problem is same trn# & gqfunc, but different users

select gqco#, gqtrn#, gqfunc, max(gqdate) as gdate, max(gqtime) as gtime
from rydedata.glpdtim
where left(trim(cast(gqdate as char(12))), 4) = '2011'
  and gqtrn# <> 0
group by gqco#, gqtrn#, gqfunc

select t.*
from rydedata.glpdtim t
inner join (
    select gqco#, gqtrn#, gqfunc, max(gqdate) as gdate, max(gqtime) as gtime
    from rydedata.glpdtim
    where left(trim(cast(gqdate as char(12))), 4) = '2011'
      and gqtrn# <> 0
    group by gqco#, gqtrn#, gqfunc) x on t.gqco# = x.gqco#
  and t.gqtrn# = x.gqtrn#
  and t.gqfunc = x.gqfunc 
  and t.gqdate = x.gdate
  and t.gqtime = x.gtime

select *
from rydedata.glpdtim
where gqtrn# in (
select gqtrn# 
from (
  select t.*
  from rydedata.glpdtim t
  inner join (
      select gqco#, gqtrn#, gqfunc, max(gqdate) as gdate, max(gqtime) as gtime
      from rydedata.glpdtim
      where left(trim(cast(gqdate as char(12))), 4) = '2011'
        and gqtrn# <> 0
      group by gqco#, gqtrn#, gqfunc) x on t.gqco# = x.gqco#
    and t.gqtrn# = x.gqtrn#
    and t.gqfunc = x.gqfunc 
    and t.gqdate = x.gdate
    and t.gqtime = x.gtime) y
group by gqtrn# having count(*) > 1)


select *
from rydedata.glptrns
where gttrn# =  1714540

select *
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0
  and gqdate > (
    select max(gqdate) - 45
    from rydedata.glpdtim)
order by gqdate desc

select gqdate, (select current date - 45 day from sysibm.sysdummy1),
  (select year(current date - 45 day) from sysibm.sysdummy1),
  (select month(current date - 45 day) from sysibm.sysdummy1),
  (select day(current date - 45 day) from sysibm.sysdummy1),  
  cast(left(trim(cast(gqdate as char(12))), 4) as integer)
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0

select gqdate, 
left(trim(cast(gqdate as char(12))), 4), 
substr(trim(cast(gqdate as char(12))), 5, 2),
substr(trim(cast(gqdate as char(12))), 7, 2),
cast(left(trim(cast(gqdate as char(12))), 4) || '-' || substr(trim(cast(gqdate as char(12))), 5, 2) || '-' || substr(trim(cast(gqdate as char(12))), 7, 2) as date)
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0

select current date - 45 day
from sysibm.sysdummy1

select current date - 45 day
from rydedata.glpdtim

select *
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0
and cast(left(digits(gqdate),4) || '-' || substr(digits(gqdate), 5, 2) || '-' || substr(digits(gqdate), 7, 2) as date) > current date - 45 day 

select count(*)
from rydedata.glptrns
where gtpost = 'Y'
and gttrn# in (
  select gqtrn#
  from rydedata.glpdtim
  where gqtrn# <> 0
    and gqtime <> 0
    and cast(left(digits(gqdate),4) || '-' || substr(digits(gqdate), 5, 2) || '-' || substr(digits(gqdate), 7, 2) as date) > current date - 45 day)

-- ok set the date for glpdtim from max(date) from stgArkonaGLPDTIM
-- say 20111130
select *
from rydedata.glptrns
where gtpost = 'Y'
and gttrn# in (
  select gqtrn#
  from rydedata.glpdtim
  where gqtrn# <> 0
    and gqtime <> 0
    and gqdate >= 20111130)

-- 12/5
-- get a catch up scrape for glptrns
select * -- 206426
from rydedata.glptrns
where gtpost = 'Y'
  and year(gtdate) = 2011
  and (month(gtdate) = 11 or month(gtdate) = 12) 

select count(*) from rydedata.glptrns where gtpost = 'Y' 
and year(gtdate) = 2011 and (month(gtdate) = 11 or month(gtdate) = 12)