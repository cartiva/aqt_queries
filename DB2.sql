SELECT CURRENT DATE 
FROM SYSIBM.SYSDUMMY1 

-- date of last day of previous month
select curdate() - DAYOFMONTH(curdate()) day
FROM SYSIBM.SYSDUMMY1 

values(curdate()) 



WITH t(TABLENAME, STORELIB) AS (VALUES('SDPRDET', 'RYDEDATA'))
SELECT * 
FROM   qsys2.syscolumns, t
WHERE  table_name = TABLENAME
  and table_schema = STORELIB

----------------------------------------------------------
-- Decimal Dates -
----------------------------------------------------------
-- when dates are all 8 digits
select gqdate, 
  left(trim(cast(gqdate as char(12))), 4), 
  substr(trim(cast(gqdate as char(12))), 5, 2),
  substr(trim(cast(gqdate as char(12))), 7, 2),
  cast(left(trim(cast(gqdate as char(12))), 4) || '-' || substr(trim(cast(gqdate as char(12))), 5, 2) || '-' || substr(trim(cast(gqdate as char(12))), 7, 2) as date)
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0
-- can also be done as
select gqdate,
  left(digits(gqdate), 4),
  substr(digits(gqdate), 5, 2),
  substr(digits(gqdate), 7, 2),
  cast(left(digits(gqdate), 4) || '-' || substr(digits(gqdate), 5, 2) || '-' || substr(digits(gqdate), 7, 2) as date)
from rydedata.glpdtim
where gqtrn# <> 0
  and gqtime <> 0
-- check that all are 8 character dates
select distinct length(digits(ptdate))
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate <> 0

-- when they are not: PYPRHEAD
select distinct length(digits(yrdp01))
from RYDEDATA.PYPRHEAD
-- uh oh, turns out this is not a good test, some yrdp01 are 5, some 6
-- this is better
select distinct length(trim(p.yrdp01))
from rydedata.pyprhead p
where yrdp01 <> 0

select yrdp01,
  case 
    when p.yrdp01 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp01),2) as integer) < 20 then 
      cast (
        case length(trim(p.yrdp01))
          when 5 then  '20'||substr(trim(p.yrdp01),4,2)||'-'|| '0' || left(trim(p.yrdp01),1) || '-' ||substr(trim(p.yrdp01),2,2)
          when 6 then  '20'||substr(trim(p.yrdp01),5,2)||'-'|| left(trim(p.yrdp01),2) || '-' ||substr(trim(p.yrdp01),3,2)
        end as date) 
    when cast(right(trim(p.yrdp01),2) as integer) >= 20 then  
      cast (
        case length(trim(p.yrdp01))
          when 5 then  '19'||substr(trim(p.yrdp01),4,2)||'-'|| '0' || left(trim(p.yrdp01),1) || '-' ||substr(trim(p.yrdp01),2,2)
          when 6 then  '19'||substr(trim(p.yrdp01),5,2)||'-'|| left(trim(p.yrdp01),2) || '-' ||substr(trim(p.yrdp01),3,2)
        end as date) 
  end as yrdp01
from rydedata.pyprhead p
where yrdp01 <> 0

/**********************/
Today = select curdate() from sysibm.sysdummy1
Yesterday = select curdate() - 1 day from sysibm.sysdummy1
First day of current week = select curdate() - (dayofweek(curdate()) - 1) days from sysibm.sysdummy1
First day of last week = select curdate() - (dayofweek(curdate()) - 1 + 7) days from sysibm.sysdummy1 or select curdate() - (dayofweek(curdate()) + 6) days from sysibm.sysdummy1
Last day of last week = select curdate() - dayofweek(curdate()) days from sysibm.sysdummy1
First day of current month = select curdate() - (dayofmonth(curdate()) - 1) days from sysibm.sysdummy1
Last day of current month = select curdate() + 1 month - (dayofmonth(curdate() + 1 month)) days from sysibm.sysdummy1
First day of last month = select curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days from sysibm.sysdummy1
Last day of last month = select curdate() - dayofmonth(curdate() - 1 month) days from sysibm.sysdummy1
/**********************/

-- bopmast.bmdtcap data type DATE, but the field can be null
-- detect the null
select 
  case 
    when bmdtcap < date('1899-01-01') then date('9999-12-31')
    else bmdtcap
  end
from rydedata.bopmast
where trim(bmstk#) = '12827'

