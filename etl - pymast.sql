select ymco#, ymempn, ymactv, ymname, ymhdte, ymhdto, ymrdte, ymtdte, ympper, 
  ymclas, ymsaly, ymrate, ymeeoc
from rydedata.pymast
order by ymname

select *
from rydedata.pymast
order by ymname

skip fields 
ymco#
ymempn
ymactv
ymdept
ymext skip
ymsecg skip
ymstcd
ymcncd skip
ymmucd skip  
ymname
ymnkey skip
ymstre -> ymsex
ymfp skip
ymmari
ymexss skip
ymexfd skip
ymfdex
ymfdad
ymfdap
ymvtyp skip
ymclas -> ymrate
ymsala skip
ymrata skip
ymdist
ymrete
ymretp -> ymcwfl skip
ymsttx 
ymmutx -> ymexst
ymstex
YMSTAD -> YMMUAP skip
ymsbki
ymdvac -> ymsrat skip
ymar#
ymdvst
ymdvex skip
ymfdst
ymstst
YMCNST -> YMTMP10 skip

-- min/max
select min(ymdvac), max(ymdvac),length(max(digits(ymdvac)))
from rydedata.pymast

select distinct ymsttx
from rydedata.pymast

select ymarea, count(*)
from rydedata.pymast
group by ymarea
order by ymarea


select YMCO#, YMEMPN, YMACTV, YMDEPT, YMSTCD, YMNAME, YMSTRE, 
  YMCITY, YMSTAT, YMZIP, YMAREA, YMPHON, YMSS#, YMDRIV, YMBDTE,
  YMHDTE, YMHDTO, YMRDTE, YMTDTE, YMSEX, YMMARI, YMFDEX, YMFDAD, 
  YMFDAP, YMCLAS, YMEEIC, YMECLS, YMPPER, YMSALY, YMRATE, YMDIST, 
  YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, YMAR#, YMDVST, YMFDST, YMSTST
from rydedata.pymast
where length(trim(ymname)) > 3
  and trim(ymname) <> 'TEST'
order by ymname

select YMCO#, YMEMPN, YMACTV, YMDEPT, YMSTCD, YMNAME, YMSTRE, 
  YMCITY, YMSTAT, YMZIP, YMAREA, YMPHON, YMSS#, YMDRIV, YMBDTE,
  YMHDTE, YMHDTO, YMRDTE, YMTDTE, YMSEX, YMMARI, YMFDEX, YMFDAD, 
  YMFDAP, YMCLAS, YMEEIC, YMECLS, YMPPER, YMSALY, YMRATE, YMDIST, 
  YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, YMAR#, YMDVST, YMFDST, YMSTST
from rydedata.pymast
where trim(ymempn) in (
  select trim(ymempn)
  from rydedata.pymast
  group by trim(ymempn)
    having count(*) > 1)
order by ymempn

-- emp no and co are unique
select ymco#, ymempn
from rydedata.pymast
group by ymco#, ymempn

-- format dates
-- null values for strings
select YMCO#, YMEMPN, YMACTV, YMDEPT, YMSTCD, YMNAME, YMSTRE, 
  YMCITY, YMSTAT, YMZIP, YMAREA, YMPHON, YMSS#,
  YMDRIV, 
  case 
    when cast(right(trim(p.YMBDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMBDTE))
          when 5 then  '20'||substr(trim(p.YMBDTE),4,2)||'-'|| '0' || left(trim(p.YMBDTE),1) || '-' ||substr(trim(p.YMBDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMBDTE),5,2)||'-'|| left(trim(p.YMBDTE),2) || '-' ||substr(trim(p.YMBDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMBDTE))
          when 5 then  '19'||substr(trim(p.YMBDTE),4,2)||'-'|| '0' || left(trim(p.YMBDTE),1) || '-' ||substr(trim(p.YMBDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMBDTE),5,2)||'-'|| left(trim(p.YMBDTE),2) || '-' ||substr(trim(p.YMBDTE),3,2)
        end as date) 
  end as YMBDTE,
  case 
    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '20'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '19'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
  end as YMHDTE,
  case 
    when p.YMHDTO = 0 then date('9999-12-31')
    when cast(right(trim(p.YMHDTO),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '20'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
    when cast(right(trim(p.YMHDTO),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '19'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
  end as YMHDTO,
  case 
    when p.YMRDTE = 0 then date('9999-12-31')
    when cast(right(trim(p.YMRDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMRDTE))
          when 5 then  '20'||substr(trim(p.YMRDTE),4,2)||'-'|| '0' || left(trim(p.YMRDTE),1) || '-' ||substr(trim(p.YMRDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMRDTE),5,2)||'-'|| left(trim(p.YMRDTE),2) || '-' ||substr(trim(p.YMRDTE),3,2)
        end as date) 
    when cast(right(trim(p.YMRDTE),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMRDTE))
          when 5 then  '19'||substr(trim(p.YMRDTE),4,2)||'-'|| '0' || left(trim(p.YMRDTE),1) || '-' ||substr(trim(p.YMRDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMRDTE),5,2)||'-'|| left(trim(p.YMRDTE),2) || '-' ||substr(trim(p.YMRDTE),3,2)
        end as date) 
  end as YMRDTE,
  case 
    when p.YMTDTE = 0 then date('9999-12-31')
    when cast(right(trim(p.YMTDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMTDTE))
          when 5 then  '20'||substr(trim(p.YMTDTE),4,2)||'-'|| '0' || left(trim(p.YMTDTE),1) || '-' ||substr(trim(p.YMTDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMTDTE),5,2)||'-'|| left(trim(p.YMTDTE),2) || '-' ||substr(trim(p.YMTDTE),3,2)
        end as date) 
    when cast(right(trim(p.YMTDTE),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMTDTE))
          when 5 then  '19'||substr(trim(p.YMTDTE),4,2)||'-'|| '0' || left(trim(p.YMTDTE),1) || '-' ||substr(trim(p.YMTDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMTDTE),5,2)||'-'|| left(trim(p.YMTDTE),2) || '-' ||substr(trim(p.YMTDTE),3,2)
        end as date) 
  end as YMTDTE, 
  YMSEX, YMMARI, YMFDEX, YMFDAD, 
  YMFDAP, YMCLAS, YMEEIC, YMECLS, YMPPER, YMSALY, YMRATE, YMDIST, 
  YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, YMAR#, YMDVST, YMFDST, YMSTST
from rydedata.pymast p
where trim(ymempn) = '170150'

where length(trim(ymname)) > 3
  and trim(ymname) <> 'TEST'

-- shit looks like digits is not the answer - padds with zeros to fit field size
-- forget trim, do the ads SP

select *
from rydedata.pymast
where ymactv = 'A'
and ymdept = ''

select ymdist, count(*)
from rydedata.pymast
group by ymdist


select *
from rydedata.pyactgr
order by ytadic

select * from rydedata.sdptime
order by sidate desc

SELECT * 
FROM rydedata.PYMAST 
WHERE trim(ymempn) = '11725'
-- 12/16
select ymdept, count(*)
FROM rydedata.PYMAST 
group by ymdept

select 
  case 
    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '20'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '19'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
  end as YMHDTE, ymhdte
FROM rydedata.PYMAST p
where trim(ymempn) = '115255'


select *
from rydedata.ffpxrefdta
where (fxconsol <> '' or fxgact = '')

select count(*) from (
select fxcyy, fxfact, fxgact, fxmpge, fxmlne 
from rydedata.ffpxrefdta fs
left join eisglobal.sypffxmst sy on fs.fxcyy = sy.fxmcyy
and fs.fxfact = sy.fxmact and fs.fxcode = sy.fxmcde 
where fs.fxconsol = ''
  and fs.fxgact <> '') x

select * from rydedata.glpdept

/******** 1/20/12 add ymdist *************************************/
-- looks like distribution codes come from pyactgr (Distribution for Gross Amounts)
select *
from rydedata.pyactgr

-- ytaseq is part of the unique key, but i don't know how that is applied elsewhere
select ytaco#, ytadic, ytaseq
from rydedata.pyactgr
group by ytaco#, ytadic, ytaseq
  having count(*) > 1

-- ytadic/ytadsc are not unique
-- stupid shit like ytadic = SALE ytadsc = SALES DEPARTMENT & SLAES DEPARTMENT
-- therefor i'm thinking, distribution code clean up doesn't necessarily belong in EmpDim
-- just stick the fucking codes in there and then wait and see how we use them
select *
from (
  select ymdist, count(*)
  from rydedata.pymast
  group by ymdist) p
left join (
  select distinct ytadic, ytadsc
  from rydedata.pyactgr) d on trim(p.ymdist) = trim(d.ytadic)
where d.ytadic in (
  select ytadic
  from (
    select distinct ytadic, ytadsc
    from rydedata.pyactgr) x
    group by ytadic 
       having count(*) > 1)
order by ymdist

select *
from rydedata.pyactgr
where trim(ytadic) in (-- dups
  'DRI', 'OWN', 'PDQW','SALE','SMGR','SRVM','USED')
order by ytadic, ytaco#

-- oh shit, are they unique by store?
-- bingo, yep they are
select ytaco#, ytadic, ytadsc 
from (
select ytaco#, ytadic, ytadsc 
from rydedata.pyactgr
group by ytaco#, ytadic, ytadsc) x
group by ytaco#, ytadic, ytadsc 
  having count(*) > 1 

-- so the question becomes
-- as part of EmpDim (dependent on dist code, part of a multiple field candidate key comprised of type2 fields)
-- argument for: for any query utilizing the EmpDim.DistCode any changes in the pyactgr file will be relevant


select * from rydedata.pyactgr
where trim(ytadic) = 'OWN'

select min(ytaseq), max(ytaseq), min(ytagpp), max(ytagpp)
from rydedata.pyactgr

/************ 1/22/12  sdptech ********************************/
select stco#, sttech, stname, stactv, stpyemp#
from rydedata.sdptech

select ymco#, ymempn, ymname, ymactv, stco#, sttech, stname, stactv, stpyemp#
from rydedata.pymast p
left join (
    select stco#, sttech, stname, stactv, stpyemp#
    from rydedata.sdptech) t on p.ymco# = t.stco#
  and trim(p.ymempn) = trim(t.stpyemp#)

/*
1/24/12
ymhdto orig hire date
ymhdte last hire date
FUCK we reuse employee numbers for rehires
*/

select *
from rydedata.pymast


select ymco#, ymempn, ymactv, ymname, 
  case 
    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '20'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '19'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
  end as YMHDTE,
  case 
    when p.YMHDTO = 0 then date('9999-12-31')
    when cast(right(trim(p.YMHDTO),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '20'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
    when cast(right(trim(p.YMHDTO),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '19'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
  end as YMHDTO
from rydedata.pymast p
where ymhdte <> ymhdto

-- whew, at least there aren't any where ymhdto > ymhdte
select *
from (
select ymco#, ymempn, ymactv, ymname, 
  case 
    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '20'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '19'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
  end as YMHDTE,
  case 
    when p.YMHDTO = 0 then date('9999-12-31')
    when cast(right(trim(p.YMHDTO),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '20'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
    when cast(right(trim(p.YMHDTO),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '19'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
  end as YMHDTO
from rydedata.pymast p
where ymhdte <> ymhdto) x
where ymhdto > ymhdte








