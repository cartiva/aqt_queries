select a.employee_, a.employee_last_name, a.employee_first_name, a.total_gross_pay, b.description, 
    date(trim(char(check_month))||'/'||trim(char(check_day))||'/'||'20'||trim(char(check_year))) as CheckDate, 
    c.amount, c.description
--select *
from rydedata.pyhshdta a
inner join  RYDEDATA.PYPTBDTA b on a.payroll_run_number = b.payroll_run_number
  and a.company_number = b.company_number
inner join rydedata.pyhscdta c on a.payroll_run_number = c.payroll_run_number
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and trim(c.code_id) in ('79','78') --commissions, draws
-- where trim(a.employee_) = '128530'
where trim(a.distrib_code) = 'SALE'
  and a.payroll_cen_year = 116
  and a.payroll_ending_month between 9 and 11
  and a.company_number = 'RY1'
order by a.employee_, a.payroll_run_number desc 


select *
from rydedata.pyhshdta a
inner join rydedata.pyhscdta c on a.payroll_run_number = c.payroll_run_number
--   and a.company_number = c.company_number
--   and a.employee_ = c.employee_number
--   and trim(c.code_id) in ('79','78') --commissions, draws  
where trim(a.distrib_code) = 'SALE'
  and a.payroll_cen_year = 116
  and a.payroll_ending_month between 9 and 11
  and a.company_number = 'RY1'