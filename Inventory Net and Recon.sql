-- Looking to find the numbers displayed in Vehicle Information Cost Disclosure
Select * from RYDEDATA.GLPTRNS
where trim(gtctl#) = '99411B'

select * from inpmast where trim(imstk#) = '99411B'

select i.imstk#, i.imcost, i.imwkip as WIP, g.Recon
from inpmast i
left join (
  select gtctl#, sum(gttamt) as Recon
  from glptrns
  where gtjrnl = 'SVI'
  group by gtctl#) g on g.gtctl# = i.imstk#
where i.imstat = 'I'
and i.imtype = 'U'
and left(trim(i.imiact), 1) = '1'


select i.imstk#, i.imcost, i.imwkip as WIP, g.Recon, g1.Cost
from inpmast i
left join (
  select gtctl#, sum(gttamt) as Recon
  from glptrns
  where gtjrnl in ('SVI', 'POT')
  group by gtctl#) g on g.gtctl# = i.imstk#
left join (
  select gtctl#, sum(gttamt) as Cost
  from glptrns
  where gtjrnl = 'VSU'
  group by gtctl#) g1 on g1.gtctl# = i.imstk#  
where i.imstat = 'I'
and i.imtype = 'U'
and left(trim(i.imiact), 1) = '1'
order by i.imstk#

-- take out cost from glptrns
-- take out cash for clunkers
select trim(i.imstk#), cast(i.imcost as INTEGER) as NET, cast(i.imwkip as Integer) as WIP, cast(coalesce(g.Recon, 0) as integer) as Recon
from inpmast i
left join (
  select gtctl#, sum(gttamt) as Recon
  from glptrns
  where gtjrnl in ('SVI', 'POT')
  group by gtctl#) g on g.gtctl# = i.imstk#
where i.imstat = 'I'
and i.imtype = 'U'
--and left(trim(i.imiact), 1) = '1'
and right(trim(i.imstk#),1) <> 'G'
and trim(i.imstk#) <> ''
order by trim(i.imstk#)