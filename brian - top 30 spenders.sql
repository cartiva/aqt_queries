select bmbuyr, bmsnam, count(*) -- t.parks: 247907 - auction
from rydedata.bopmast
where bmbuyr in (
  select bmbuyr
  from rydedata.bopmast
  group by bmbuyr
  having count(*) > 10)
group by bmbuyr, bmsnam


-- 1044510 Jerry Schwan
select *
from rydedata.bopname
where bnkey = 1044510

-- 278247 Wilwand farms
select *
from rydedata.bopname
where bnkey = 278247
good one
previous 12 months 
  11 car deals
   4 parts invoices
   5 service invoices

select year(bmdtaprv), bmdtaprv, bmpric, a.*
from rydedata.bopmast a
where bmbuyr = 278247

select year(bmdtaprv), sum(bmpric)
from rydedata.bopmast a
where bmbuyr = 278247
group by year(bmdtaprv)

-- 2010
select bmbuyr, bmsnam, sum(bmpric) as spend, year(bmdtaprv) as theyear
from rydedata.bopmast
where year(bmdtaprv) = 2010
  and bmbuyr not in (247907, 319, 316850, 205008, 210836,448,257263,284945,
    1002037,210836,287919,1020240,1000387,302977, 290672) 
group by year(bmdtaprv), bmbuyr, bmsnam
order by spend desc, theyear desc 
fetch first 30 rows only
-- 2011
select bmbuyr, bmsnam, sum(bmpric) as spend, year(bmdtaprv) as theyear
from rydedata.bopmast
where year(bmdtaprv) = 2011
  and bmbuyr not in (247907, 319, 316850, 205008, 210836,448,257263,284945,
    1002037,210836,287919,1020240,1000387,302977, 290672, 251340, 262063,287992,
    306896,289746,289691,290844) 
group by year(bmdtaprv), bmbuyr, bmsnam
order by spend desc, theyear desc 
fetch first 30 rows only
-- 2012
select bmbuyr, bmsnam, sum(bmpric) as spend, year(bmdtaprv) as theyear
from rydedata.bopmast
where year(bmdtaprv) = 2012
  and bmbuyr not in (247907, 319, 316850, 205008, 210836,448,257263,284945,
    1002037,210836,287919,1020240,1000387,302977, 290672, 251340, 262063,287992,
    306896,289746,289691,290844,287798,226274,1012877,269776,1028710,1025655) 
group by year(bmdtaprv), bmbuyr, bmsnam
order by spend desc, theyear desc 
fetch first 30 rows only

--
-- service
--
-- 2012
select ptckey, ptcnam, sum(ptpbmf) as spend, year(ptcreate)
from rydedata.sdprhdr
where ptckey not in (0,319,302977,275745)
  and year(ptcreate) = 2012
group by ptckey, ptcnam, year(ptcreate)
order by spend desc
fetch first 30 rows only
-- 2011
select ptckey, ptcnam, sum(ptpbmf) as spend, year(ptcreate)
from rydedata.sdprhdr
where ptckey not in (0,319,302977,275745,284945,285611)
  and year(ptcreate) = 2011
group by ptckey, ptcnam, year(ptcreate)
order by spend desc
fetch first 30 rows only
-- 2010
select ptckey, ptcnam, sum(ptpbmf) as spend, year(ptcreate)
from rydedata.sdprhdr
where ptckey not in (0,319,302977,275745,284945,285611,252250,323005,289742,
  251429,316511,318856,289743,289742,291826,289970)
  and year(ptcreate) = 2010
group by ptckey, ptcnam, year(ptcreate)
order by spend desc
fetch first 30 rows only


-- 
-- parts
--
-- 2012
select year(ptdate) as theyear, ptckey as custKey, ptsnam as custName, sum(ptptot + ptshpt) as spend
from rydedata.pdpthdr
where year(ptdate) = 2012
  and ptckey not in (0,289742,254262)
group by year(ptdate), ptckey, ptsnam
order by spend desc
fetch first 30 rows only
-- 2011
select year(ptdate) as theyear, ptckey, ptsnam, sum(ptptot + ptshpt) as spend
from rydedata.pdpthdr
where year(ptdate) = 2011
  and ptckey not in (0,289742,254262)
group by year(ptdate), ptckey, ptsnam
order by spend desc
fetch first 30 rows only
-- 2010
select year(ptdate) as theyear, ptckey, ptsnam, sum(ptptot + ptshpt) as spend
from rydedata.pdpthdr
where year(ptdate) = 2010
  and ptckey not in (0,289742,254262)
group by year(ptdate), ptckey, ptsnam
order by spend desc
fetch first 30 rows only
 


select coalesce(a.cusname, b.cusname, c.cusname), coalesce(a.cuskey, b.cuskey,c.cuskey),
  coalesce(salesspend,0) + coalesce(servspend,0) + coalesce(partsspend,0) as totalspend, 
  coalesce(salesspend,0) as sales, coalesce(servspend, 0) as service, coalesce(partsspend, 0) as parts
from (
  select bmbuyr as cuskey, bmsnam as cusname, sum(bmpric) as salesspend, year(bmdtaprv) as theyear
  from rydedata.bopmast -- sales 2012
  where year(bmdtaprv) = 2012
    and bmbuyr not in (247907, 319, 316850, 205008, 210836,448,257263,284945,
      1002037,210836,287919,1020240,1000387,302977, 290672, 251340, 262063,287992,
      306896,289746,289691,290844,287798,226274,1012877,269776,1028710,1025655,251370) 
  group by year(bmdtaprv), bmbuyr, bmsnam) a
full outer join (
  select ptckey as cuskey, ptcnam as cusname, sum(ptpbmf) as servspend, year(ptcreate) as theyear
  from rydedata.sdprhdr -- service 2012
  where ptckey not in (0,319,302977,275745,251370)
    and year(ptcreate) = 2012
  group by ptckey, ptcnam, year(ptcreate)) b on a.cuskey = b.cuskey
full outer join (
  select ptckey as cuskey, ptsnam as cusname, sum(ptptot + ptshpt) as partsspend, year(ptdate) as theyear
  from rydedata.pdpthdr -- parts 2012
  where year(ptdate) = 2012
    and ptckey not in (0,289742,254262,251370)
  group by year(ptdate), ptckey, ptsnam) c on a.cuskey = c.cuskey or b.cuskey = c.cuskey
  order by totalspend desc
fetch first 50 rows only


