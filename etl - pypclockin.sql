Select * from RYDEDATA.PYPCLOCKIN
Select count(*) from RYDEDATA.PYPCLOCKIN -- 201106
select month(yiclkind), year(yiclkind), count(*)
from RYDEDATA.PYPCLOCKIN
group by month(yiclkind), year(yiclkind)
order by year(yiclkind), month(yiclkind)

Select * from RYDEDATA.PYPCLOCKIN order by yiclkind desc

select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
from rydedata.pypclockin
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  having count(*) > 1
order by yiclkind desc

select yiclkint, count(*)
from rydedata.pypclockin
group by yiclkint 
order by count(*) desc


select p.ymname, c.*
from rydedata.pypclockin c
left join rydedata.pymast p on trim(c.yiemp#) = trim(p.ymempn)
where yiclkint = '00:00:00'
and yiclkoutt <> '00:00:00'



select *
from rydedata.pypclockin
where (yiclkind = yiclkoutd and yiclkint = yiclkoutt)

-- not unique
select yiemp#, yiclkind, yiclkint
from rydedata.pypclockin
group by yiemp#, yiclkind, yiclkint
having count(*) > 1

select *
from rydedata.pypclockin
where trim(yiemp#) = '148275'
and yiclkind = '2011-05-31'

-- not unique
select yiemp#, yiclkind, yiclkint, yicode
from rydedata.pypclockin
group by yiemp#, yiclkind, yiclkint, yicode
having count(*) > 1

-- yicode
select yicode, count(*)
from rydedata.pypclockin
group by yicode

select yiinfo, count(*) from rydedata.pypclockin group by yiinfo

select p.*
from rydedata.pypclockin p
inner join ( -- y
    select yiemp#, yiclkind, yiclkint, yicode
    from rydedata.pypclockin
    group by yiemp#, yiclkind, yiclkint, yicode
    having count(*) > 1) y on p.yiemp# = y.yiemp#
  and p.yiclkind = y.yiclkind
  and p.yiclkint = y.yiclkint
  and p.yicode = y.yicode
order by p.yiemp#, p.yiclkind
  
-- 1/1/12
-- thinking distinct 
-- 1/1/12 initial take on scrape data
-- yiclkoutt: odbc chokes on 24:00:00
select yico#, yiemp#, yiclkind, 
  case
    when yiclkint = '24:00:00' then '23:59:59'
  else
    yiclkint
  end as yiclkint,   
  yiclkoutd, 
  case
    when yiclkoutt = '24:00:00' then '23:59:59'
  else
    yiclkoutt
  end as yiclkoutt, 
  yicode
from rydedata.pypclockin
where yiclkoutt = '24:00:00'
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode

select count(*)
from (
select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
from rydedata.pypclockin
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode) x
-- unique yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
from (
  select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from rydedata.pypclockin
  group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode) x
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  having count(*) > 1

-- extending the work in ads etl-pypclockin
-- generate unique empl/clockin

SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, max(yiclkoutt) AS yiclkoutt, yicode
FROM rydedata.pypclockin
WHERE yicode <> '666'
GROUP BY yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode

-- i think this is it, unique yico#/yiemp#/yiclkind/yiclkint
SELECT yico#, yiemp#, yiclkind, yiclkint
from (
  SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, max(yiclkoutt) AS yiclkoutt, yicode
  FROM rydedata.pypclockin
  WHERE yicode <> '666'
  GROUP BY yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode) x
group by yico#, yiemp#, yiclkind, yiclkint
  having count(*) > 1

-- so, extending the db2 query
select yico#, yiemp#, yiclkind, 
  case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,   
  yiclkoutd, 
  max(case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end) as yiclkoutt, 
  yicode
from rydedata.pypclockin
where yicode <> '666'
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode

-- returns no dups
select yico#, yiemp#, yiclkind, yiclkint from (
  select yico#, yiemp#, yiclkind, 
    case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,   
    yiclkoutd, 
    max(case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end) as yiclkoutt, 
    yicode
  from rydedata.pypclockin
  where yicode <> '666'
  group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode) x
group by yico#, yiemp#, yiclkind, yiclkint
  having count(*) > 1

'select yico#, yiemp#, yiclkind, ' +
'  case when yiclkint = ''24:00:00'' then ''23:59:59'' else yiclkint end as yiclkint, ' + 
'  yiclkoutd, ' +
'  max(case when yiclkoutt = ''24:00:00'' then ''23:59:59'' else yiclkoutt end) as yiclkoutt, ' +
'  yicode ' +
'from rydedata.pypclockin ' +
'where yicode  ''O'' ' +
'group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode');

select year(yiclkind), month(yiclkind), count(*) 
from rydedata.pypclockin where yicode = 'I'
group by year(yiclkind), month(yiclkind)

select *
from rydedata.pypclockin where yicode = 'I'
order by yiclkind desc

select yico#, yiemp#, count(*)
from rydedata.pypclockin where yicode = 'I'
group by yico#, yiemp#
order by count(*) desc

select * -- shane
from rydedata.pymast
where trim(ymempn) = '16298'

select *
from rydedata.pypclockin
where trim(yiemp#) = '16298'
order by yiclkind desc

--------------------------------------------------
-- pypclkl
select *
from rydedata.pypclkl
--where trim(ylemp#) = '16298'
order by ylclkind desc

select *

from rydedata.pypclockin i
left join rydedata.pypclkl l on i.yico# = l.ylcco#
  and trim(yiemp#) = trim(l.ylemp#)
  and i.yiclkind = l.ylclkind
where year(i.yiclkind) = 2011
  and trim(i.yiemp#) = '148275'
order by i.yiclkind desc

select ymclas, count(*)
from rydedata.pymast
group by ymclas
 
select *
from rydedata.pymast
where ymactv <> 'T'
  and ymclas <> 'H'
order by ymclas, ymname

select p.ymname, c.*
from rydedata.pypclockin c 
inner join rydedata.pymast p on trim(c.yiemp#) = trim(p.ymempn)
  and p.ymclas <> 'H'
  and p.ymactv <>'T'
order by c.yiclkind desc

where trim(yiemp#) in (
  select trim(ymempn)
  from rydedata.pymast
  where ymclas <> 'H')
order by yiclkind desc 


-- future clockins
-- managers logging vacation time in advance
select *
from rydedata.pypclockin
-- where trim(yiemp#) = '136170'
order by yiclkind desc

-- diff in pypclkl between change date and in/out date
select p.*, year(ylchgd) - year(ylclkind), month(ylchgd) - month(ylclkind)
from rydedata.pypclkl p
--where trim(ylemp#) = '16298'
order by ylchgd - ylclkind desc

-- 1/2/12
-- fact table at the granularity of store/emp#/clock in time
-- need to find those dups

-- scrape bombing on 24:00:00
Select * from RYDEDATA.PYPCLOCKIN
where yiclkind >= '08/27/2009'
  and yiclkint > '16:00:00'

select min(yiclkind), max(yiclkind)
from RYDEDATA.PYPCLOCKIN

select min(yiclkint), max(yiclkint)
from RYDEDATA.PYPCLOCKIN

select min(yiclkoutt), max(yiclkoutt)
from RYDEDATA.PYPCLOCKIN




-- greg's work with hours
select a."Company", a."EmpNum", a."Week", a."Day",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
        else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours"
    from 
      ( -- a
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join 
      ( -- b
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
       from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" and week(a."Day") = week(b."Day") and b."Day" < a."Day"
    group by a."Week", a."Company", a."EmpNum", a."Day", a."Hours"
    order by a."Week", a."Company", a."EmpNum", a."Day"


select count(*)
from rydedata.pypclockin
where yiclkind > curdate() - 60 day

-- multiple records
-- last ten sundays 
select yico#, yiemp#, yiclkind,   
  case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,   
  yiclkoutd,  
  max(case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end) as yiclkoutt,   
  yicode 
from rydedata.pypclockin 
where yicode <> '666'   
  and yiclkind >= '10/23/2011'
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode
order by yiclkind 

-- 6/6/12
-- dups
select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode
from rydedata.pypclockin
where yicode <> '666'
and yiclkind > curdate() - 30 day
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode

select yico#, yiemp#, yiclkind, yiclkint
from (
  select yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode
  from rydedata.pypclockin
  where yicode <> '666'
  and yiclkind > curdate() - 30 day
  group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode) a
GROUP BY yico#, yiemp#, yiclkind, yiclkint
HAVING COUNT(*) > 1

select *
from rydedata.pypclockin
where trim(yiemp#) = '123525'
order by yiclkind desc


/* 7//12 real time clockdata */
select yico#, yiemp#, yiclkind, 
  case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,   
  yiclkoutd, 
  max(case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end) as yiclkoutt, 
  yicode
from rydedata.pypclockin
where yicode <> '666'
  and yiclkind = curdate()
group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode
order by yiemp#

/*  7/23/12 */

