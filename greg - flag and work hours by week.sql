-- Left most table is a derived Date Table containing any day that someone punched in in any of the stores
-- top level tables: base, flag, hours
select /*base."TheDate",*/ base."TheYear", /*base."TheMonth",*/ base."TheWeek", --base."EmpNum", base."Name", flag."Tech", 
  sum(coalesce(cast(flag."Flagged" as double), 0)) as "Flagged", sum(coalesce(cast(hours."Total Hours" as double), 0)) as "Worked", 
  case when sum(coalesce(hours."Total Hours", 0)) <> 0 then trunc(sum(cast(coalesce(flag."Flagged", 0) as double)) / sum(cast(coalesce(hours."Total Hours", 0) as double)) * 100, 1)  else '0%' end as "Prod"
from ( -- base
  select distinct(yiclkind) as "TheDate", year(yiclkind) as "TheYear", month(yiclkind) as "TheMonth", 
    week(yiclkind) as "TheWeek", emps.ymempn as "EmpNum", emps.ymname as "Name"
  from rydedata.pypclockin, (select ymempn, ymname from rydedata.pymast) as emps) as base
left join ( -- hours -- left join on hours worked on any 
  select z."Date", z."EmpNum", ymname as "Name", 
    sum(z."Reg Hours") as "Reg Hours", sum(z."OT Hours") as "OT Hours", sum(z."Reg Hours") + sum(z."OT Hours") as "Total Hours",
    round(sum(z."Reg Hours") * ymrate, 2) as "Reg Pay", round(sum(z."OT Hours") * ymrate * 1.5, 2) as "OT Pay", 
    round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) as "Total Pay"
  from ( -- z
    select a."Company", a."EmpNum", a."Year", a."Week", a."Date",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
          else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours"
    from ( -- a -- 
        select yico# as "Company", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' --and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join ( -- b
        select yico# as "Company", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' --and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" 
           and year(a."Date") = year(b."Date") 
           and week(a."Date") = week(b."Date") 
           and b."Date" < a."Date"
  
    group by a."Year", a."Week", a."Company", a."EmpNum", a."Date", a."Hours") as z
    left join rydedata.pymast py on py.ymco# = z."Company" and py.ymempn = z."EmpNum"
  group by z."Date", z."EmpNum", py.ymname, py.ymrate) as hours on hours."Date" = base."TheDate" and hours."EmpNum" = base."EmpNum"
  
  
  -- left join hours flagged in the main shop
  
  -- Tech flagged hours without time adjustments
  -- Restriction on Active techs in sdptech and Non-terminated in pymast only removed to allow for accurate history reporting
left JOIN ( -- flag
  select p.ymempn, x."Tech", x."Date", sum(x."Hours") as "Flagged"
    from (
      select ptdoc# as "RO#", cast(left(trim(d.ptdate), 4)||'-'|| substr(trim(d.ptdate), 5, 2) || '-' || substr(trim(d.ptdate), 7, 2) as date) as "Date", pttech as "Tech", ptlhrs as "Hours"
      from rydedata.pdppdet d
      inner join rydedata.pdpphdr h on h.ptpkey = d.ptpkey -- no RO# in pdppdet
      where d.ptdate >= 20090728 and d.ptdate <= 20200101
      --and ptcode = 'TT'
      and pttech in (
        select sttech
        from rydedata.sdptech t
        inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
        where t.stco# = 'RY1'
         --and t.stactv <> 'N'
        and p.ymdist in ('STEC', 'PR/S')
         --and p.ymactv <> 'T'
        and not left(sttech, 1) in ('H', 'M'))
    union all
    select ptro#, cast(left(trim(ptdate), 4)||'-'|| substr(trim(ptdate), 5, 2) || '-' || substr(trim(ptdate), 7, 2) as date), pttech, ptlhrs
    from rydedata.sdprdet s
    where s.ptdate >= 20090728 and s.ptdate <= 20200101
      --and ptcode = 'TT'
    and pttech in (
      select sttech
      from rydedata.sdptech t
      inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
      where t.stco# = 'RY1'
      --and t.stactv <> 'N'
      and p.ymdist in ('STEC', 'PR/S')
      --and p.ymactv <> 'T'
      and not left(sttech, 1) in ('H', 'M'))) as x
  inner join rydedata.sdptech t on t.sttech = x."Tech"
  inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
  group by x."Date", p.ymempn, x."Tech") as flag on flag."Date" = base."TheDate" and flag.ymempn = base."EmpNum"
where not flag."Flagged" is null --and base."TheDate" = '9/20/2011'
group by base."TheYear", base."TheWeek" /*base."TheDate", base."TheMonth"*/ -- don't need this on individual report
order by base."TheYear", base."TheWeek" --flag."Tech"
