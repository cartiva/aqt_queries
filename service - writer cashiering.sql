Select date(ptcdat), ptcdat
from RYDEDATA.SDPRHDR
where date(ptcdat) = curdate()


select distinct length(ptcdat)
from rydedata.SDPRHDR




-- compare ptdate to ptcreate
select distinct a.ptro#, ptswid, b.swname, d.gquser 
from rydedata.sdprhdr a
left join rydedata.sdpswtr b on trim(a.ptswid) = trim(b.swswid)
  and b.swco# = 'RY1'
left join rydedata.glptrns c on trim(a.ptro#) = trim(c.gtdoc#)
left join rydedata.glpdtim d on c.gttrn# = d.gqtrn#
where ptco# = 'RY1'
  and 
    case 
      when PTCDAT = 0 then cast('9999-12-31' as date)
      else cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) 
    end = curdate()
  and trim(ptswid) IN ('402','403','645','704','705','714','720');


select *
from (
select swname, count(*) as xCount
from (
select distinct a.ptro#, ptswid, b.swname, d.gquser 
from rydedata.sdprhdr a
left join rydedata.sdpswtr b on trim(a.ptswid) = trim(b.swswid)
  and b.swco# = 'RY1'
left join rydedata.glptrns c on trim(a.ptro#) = trim(c.gtdoc#)
left join rydedata.glpdtim d on c.gttrn# = d.gqtrn#
where ptco# = 'RY1'
  and 
    case 
      when PTCDAT = 0 then cast('9999-12-31' as date)
      else cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) 
    end = curdate()
  and trim(ptswid) IN ('402','403','645','704','705','714','720')) a
group by swname) X

left join (

select swname, gquser, count(*) as yCount
from (
select distinct a.ptro#, ptswid, b.swname, d.gquser 
from rydedata.sdprhdr a
left join rydedata.sdpswtr b on trim(a.ptswid) = trim(b.swswid)
  and b.swco# = 'RY1'
left join rydedata.glptrns c on trim(a.ptro#) = trim(c.gtdoc#)
left join rydedata.glpdtim d on c.gttrn# = d.gqtrn#
where ptco# = 'RY1'
  and 
    case 
      when PTCDAT = 0 then cast('9999-12-31' as date)
      else cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) 
    end = curdate()
  and trim(ptswid) IN ('402','403','645','704','705','714','720')) a
group by swname, gquser) y on x.swname = y.swname
order by x.swname asc, yCount desc
