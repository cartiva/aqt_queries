WITH t(STORELIB) AS (VALUES('RYDEDATA'))
select *
--SELECT table_name, column_name, ordinal_position, data_type, "LENGTH", numeric_scale, numeric_precision, column_heading
FROM   qsys2.syscolumns, t
WHERE   table_schema = STORELIB

select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name = 'PYACTGR'



select *
from sysibm.sqlspecialcolumns
where table_schem = 'RYDEDATA'
  and table_name = 'PYACTGR'


select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name = 'PYACTGR'



select *
from sysibm.sqlprimarykeys where table_schem = 'RYDEDATA'


select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'

select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and (scope_catalog is not null or scope_schema is not null)



select *
FROM   qsys2.syscolumns
WHERE   table_schema = 'RYDEDATA'
  and table_name = 'PYACTGR'

select column_name, table_name, column_heading, column_text
FROM   qsys2.syscolumns
WHERE   table_schema = 'RYDEDATA'
  and column_heading <> column_text



-- generate sqlColumns data
select max(length(table_cat)),max(length(table_schem)),max(length(table_name)),
  max(length(column_name)),max(length(type_name)),max(length(column_text)),
  max(length(system_column_name))
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA';

select table_cat, table_schem, table_name, column_name, data_type, type_name, 
  column_size, decimal_digits, nullable, ordinal_position, pseudo_column,
  column_text, system_column_name
--select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'

-- generate sqlPrimaryKeys data
select max(length(table_cat)),max(length(table_schem)),max(length(table_name)),
  max(length(column_name)),max(length(pk_name))
from sysibm.sqlprimarykeys
where table_schem = 'RYDEDATA';

select table_cat, table_schem, table_name, column_name, key_seq, pk_name
from sysibm.sqlprimarykeys
where table_schem = 'RYDEDATA'

select distinct table_name, pk_name
from sysibm.sqlprimarykeys
where table_schem = 'RYDEDATA'
order by table_name

select company_number, dist_code, seq_number from rydedata.pyactgr group by company_number, dist_code, seq_number having count(*) > 1

-- generate sqlTables data
select max(length(table_cat)),max(length(table_schem)),max(length(table_name)),
  max(length(table_type)), max(length(table_text))
from sysibm.sqltables
where table_schem = 'RYDEDATA';

select table_cat, table_schem, table_name, table_type, table_text
--select *
from sysibm.sqltables
where table_schem = 'RYDEDATA'

-- query for generating arkMetaData columns.txt
-- unique: for generating a consistent indexing scheme
select table_cat, table_schem, table_name, column_name
select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
group by table_cat, table_schem, table_name, column_name having count(*) > 1

select table_cat, table_schem, table_name, ordinal_position
--select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
group by table_cat, table_schem, table_name, ordinal_position having count(*) > 1

select count(*) -- 30988 rows
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'

select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
order by table_cat, table_schem, table_name, ordinal_position

-- address columns
select distinct table_name
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
  and lower(column_text) like '%addr%'
order by table_name


-- anything like payee
select distinct table_name, column_text
-- select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
  and lower(column_text) like '%payee%'
order by table_name

-- anything like check
select distinct table_name, column_text
-- select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
  and lower(column_text) like '%check%'
order by table_name

-- 12/30/14
select a.table_name, a.system_column_name, a.column_name, a.ordinal_position, 
  a.data_type, a.length, a.numeric_scale, a.numeric_precision, a.column_text, a.column_heading
FROM   qsys2.syscolumns a
WHERE   a.table_schema = 'RYDEDATA'
--  and a.table_name = 'SDPRHDR'
  and trim(a.column_text) <> trim(a.column_heading)


select count(*)
from qsys2.syscolumns
where table_schema = 'RYDEDATA'


select *
from qsys2.syscst
where table_schema = 'RYDEDATA'

select table_name, table_text
from qsys2.systables
where table_schema = 'RYDEDATA'

select a.data_type, a.length, a.numeric_scale, count(*)
from qsys2.syscolumns a
where a.table_schema = 'RYDEDATA'
group by a.data_type, a.length, a.numeric_scale
order by a.data_type, a.length, a.numeric_scale

-- 1/15/15
what is the difference between sysibm.sqlcolumns and qsys2.syscolumns
select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name = 'GLPTRNS'
order by data_type, system_column_name 
  
select a.*
FROM   qsys2.syscolumns a
WHERE   a.table_schema = 'RYDEDATA'
  and a.table_name = 'GLPTRNS'
order by data_type, system_column_name  
  
some different data, but nothing that looks immediately useful


anyway, all tables with a column that has technician in description
select table_name, column_name
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
  and lower(column_text) like '%technici%'



select table_name, column_name, system_column_name, column_Text
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
  and lower(column_text) like '%adj%'
  
  
