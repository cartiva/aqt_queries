
select  * 
from qsys2.syscstcol
where table_schema = 'RYDEDATA'
  and table_name = 'GLPMAST'

select *
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and table_name = 'GLPMAST'

select table_name
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and constraint_type = 'PRIMARY KEY'
group by table_name having count(*) > 1

select *
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and table_name in ('BOPDCID','BOPDSDC','BOPSISRC','DGPMAST')

select distinct constraint_type
from qsys2.syscst
where table_schema = 'RYDEDATA'

-- the only constraint type of interest is primary key (931 row)
-- onesy twosy for all other types (foreign key, check, unique)
select *
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and constraint_type = 'PRIMARY KEY'

select a.table_name, a.constraint_name, a.constraint_keys
from qsys2.syscst a
left join qsys2.
where table_schema = 'RYDEDATA'
  and constraint_type = 'PRIMARY KEY'

select table_name
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and constraint_type = 'PRIMARY KEY'
group by table_name
having count(*) > 1

select constraint_name
from qsys2.syscst
where table_schema = 'RYDEDATA'
  and constraint_type = 'PRIMARY KEY'
group by constraint_name
having count(*) > 1



select a.table_name, a.column_count, a.row_length, table_owner, table_text
select *
FROM  qsys2.systables a
WHERE a.table_schema = 'RYDEDATA'
  and trim(a.table_name) <> 'GLPAYWARE'
  and a.table_name not like '$%'
  and a.table_name not like 'X%'
  and a.table_name like 'GLP%'


select a.constraint_name, a.table_name, a.constraint_keys
from qsys2.syscst a
inner join qsys2.systables b on a.table_name = b.table_name
  and b.table_schema = 'RYDEDATA'
  and trim(b.table_name) <> 'GLPAYWARE'
  and b.table_name not like '$%'
  and b.table_name not like 'X%'
  and b.table_name like 'GLP%'
where a.table_schema = 'RYDEDATA'
  and a.constraint_type = 'PRIMARY KEY'
--and a.table_name = 'GLPMAST'


-- table_name = syste_table_name
select *
FROM  qsys2.systables a
WHERE a.table_schema = 'RYDEDATA'
  and trim(a.table_name) <> 'GLPAYWARE'
  and a.table_name not like '$%'
  and a.table_name not like 'X%'
  and table_name <> system_table_name


select *
from qsys2.syscstcol
where table_schema = 'RYDEDATA'
  and table_name = 'GLPMAST'


select  constraint_name, column_name
from qsys2.syscstcol
where table_schema = 'RYDEDATA'
group by constraint_name, column_name
having count(*) > 1


select is_nullable, count(*)
from qsys2.syscolumns
where table_schema = 'RYDEDATA'
group by is_nullable






