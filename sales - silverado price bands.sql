Select * from RYDEDATA.BOPMAST


select bopmast_Stock_number, bopmast_vin, delivery_date, retail_price, b.*
-- select *
from RYDEDATA.BOPMAST a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where delivery_date > curdate() - 60 days
  and franchise_code = 'NCT'
  and record_status = 'U'
  and sale_type <> 'W'
  and list_price between 45000 and 49999.99




select body_style, 
  sum(case when list_price between 45000 and 45999.99 then 1 else 0 end) as "45k",
  sum(case when list_price between 46000 and 46999.99 then 1 else 0 end) as "46k",
  sum(case when list_price between 48000 and 48999.99 then 1 else 0 end) as "48k",
  sum(case when list_price between 49000 and 49999.99 then 1 else 0 end) as "49k"
from RYDEDATA.BOPMAST a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where delivery_date > curdate() - 60 days
  and franchise_code = 'NCT'
  and record_status = 'U'
  and sale_type <> 'W'
group by body_style


select * from (
select body_style, 
  sum(case when list_price between 45000 and 45999.99 then 1 else 0 end) as "45k",
  sum(case when list_price between 46000 and 46999.99 then 1 else 0 end) as "46k",
  sum(case when list_price between 48000 and 48999.99 then 1 else 0 end) as "48k",
  sum(case when list_price between 49000 and 49999.99 then 1 else 0 end) as "49k"
from RYDEDATA.BOPMAST a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where delivery_date > curdate() - 60 days
  and franchise_code = 'NCT'
  and record_status = 'U'
  and sale_type <> 'W'
group by body_style) x
where "45k" + "46k" + "48k" + "49k" <> 0

