-- Ray
select pmpart, pmdesc, pmonhd, pmbslv, pmtqty
from pdpmast
where trim(pmgrpc) = '07831'

select pmpart, pmdesc, pmonhd, pmbslv, pmtqty
from pdpmast
where trim(pmgrpc) = '01266'

-- Mark
/*SELECT   coalesce(ps.pfaprt, pd.pmpart), pmdesc,
         (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA'),
         pm.pmstkl,
         pmonhd,
         pmrsrv,
         pmbcko,
         pm.pmstkl + pmrsrv - pmonhd - pmbcko
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
         inner join pdppmex pm on coalesce(ps.pfaprt, pd.pmpart) = pm.pmpart and pd.pmco# = pm.pmco#
WHERE    pd.pmco# = 'RY1'
AND      pd.pmstat = 'A'
AND      pmsgrp = 201
AND      trim(pd.pmnprt) = ''
AND      trim(pm.pmstcd) = '02'
AND      pm.pmstkl + pmrsrv - pmonhd - pmbcko > 0
ORDER BY coalesce(ps.pfaprt, pd.pmpart)*/
/*
SELECT   coalesce(ps.pfaprt, pd.pmpart) as "Part #", pmdesc as Description,
         (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA'),
         pm.pmstkl,
         pmonhd,
         pmordr,
         pmspor,
         pmrsrv,
         pmbcko,
         pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
         inner join pdppmex pm on pd.pmpart = pm.pmpart and pd.pmco# = pm.pmco# and pm.pmstcd = '02'
WHERE    pd.pmco# = 'RY1'
AND      pd.pmstat = 'A'
AND      pmsgrp = 201
AND      trim(pd.pmnprt) = ''
AND      trim(pm.pmstcd) = '02'
AND      pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko > 0
ORDER BY coalesce(ps.pfaprt, pd.pmpart)


SELECT   coalesce(ps.pfaprt, pd.pmpart) as "Part #", pmdesc as Description,
         (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA') as "Yearly Sales", 
         pm.pmstkl as "RIM Level",
         pmonhd as "On Hand",
         pmordr as "Reserved",
         pmspor as "On Order",
         pmrsrv as "Spec Order",
         pmbcko as "Back Ordered",
         pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko as "Needed"
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
         inner join pdppmex pm on pd.pmpart = pm.pmpart and pd.pmco# = pm.pmco# and pm.pmstcd = '02'
WHERE    pd.pmco# = 'RY1'
AND      pd.pmstat = 'A' -- status = Active 
AND      pmsgrp = 201 -- source 201 = ACDelco 
AND      trim(pd.pmnprt) = ''
AND      trim(pm.pmstcd) = '02'
AND      pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko > 0
ORDER BY coalesce(ps.pfaprt, pd.pmpart)
*/

--10/08/2009

SELECT   coalesce(ps.pfaprt, pd.pmpart) as "Part #", pmdesc as Description,
         (select coalesce(sum(ptqty), 0) 
          from pdptdet 
          where (ptpart = pd.pmpart or ptpart in ((select pfoprt from pdpmrpl where pfaprt = pd.pmpart))) 
            and ptcode in (
              'CP', /*RO Customer Pay Sale*/
              'IS', /*RO Internal Sale*/ 
              'SA', /*Counter Sale*/
              'WS', /*RO Warranty Sale*/
              'SC', /*RO Service Contract Sale*/ 
              'SR', /*RO Return*/ 
              'CR', /*RO Correction*/ 
              'RT', /*Counter Return*/ 
              'FR') /*Factory Return*/ 
            and ptco# = pd.pmco#
            and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as "Yearly Sales", 
         pm.pmstkl as "RIM Level",
         pmonhd as "On Hand",
         pmordr as "Reserved",
         pmspor as "On Order",
         pmrsrv as "Spec Order",
         pmbcko as "Back Ordered",
         pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko as "Needed"
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
         inner join pdppmex pm on pd.pmpart = pm.pmpart and pd.pmco# = pm.pmco# and pm.pmstcd = '02'
WHERE    pd.pmco# = 'RY1'
AND      pd.pmstat = 'A' -- status = Active 
AND      pmsgrp = 201 -- source 201 = ACDelco 
AND      trim(pd.pmnprt) = ''
AND      trim(pm.pmstcd) = '02' -- stock code 02 = RIM
AND      pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko > 0
ORDER BY coalesce(ps.pfaprt, pd.pmpart)






-- Rick
select pmgrpc as "Class", pmpart as "Part #", pmsgrp as "Source", pmbinl as "Bin" , pmonhd as "On Hand"
-- select *
from pdpmast
where (trim(pmgrpc) >= '12182' and trim(pmgrpc) <= '13000')
and pmonhd > 0
order by pmgrpc, pmpart


select pmgrpc as "Class", pmpart as "Part #", pmbinl as "Bin" , pmonhd as "On Hand"
-- select *
from pdpmast
where (trim(pmgrpc) >= '12182' and trim(pmgrpc) <= '13000')
and pmonhd > 0
and pmco# = 'RY1'
order by pmgrpc, pmpart

16250 to 16750
-- numeric sort by part #
select pmgrpc as "Class", pmpart as "Part #", pmbinl as "Bin" , pmonhd as "On Hand"
-- select *
from pdpmast
where (trim(pmgrpc) >= '16751' and trim(pmgrpc) <= '17750')
AND pmonhd > 0
and pmco# = 'RY1'
order by pmgrpc, cast(left(pmpart,1) as integer), pmpart

select *
from pdpmast
where left(pmpart,4) = '3710'

select distinct cast(pmgrpc as integer)
from pdpmast
--where (trim(pmgrpc) >= '12182' and trim(pmgrpc) <= '14000')
order by cast(pmgrpc as integer)

select distinct pmgrpc, cast(pmgrpc as integer)
from pdpmast
where pmgrpc <> ''
order by cast(pmgrpc as integer)

select distinct real(pmgrpc)
from pdpmast



select pmpart, TRANSLATE (pmpart, 'NNNNNNNNNNX', '1234567890N')
from pdpmast
where left(pmpart,4) = '3710'

select pmpart, TRANSLATE (pmpart, 'NNNNN', '1234567890', '%')
from pdpmast
where left(pmpart,4) = '3710'



with t(KPBL_BLK_VALUE) as (values 'a00010', '55','2','12', 'test', 'abc1') 
select 
  KPBL_BLK_VALUE 
from t 
order by 
  case   
    when translate(upper(KPBL_BLK_VALUE),'','ABCDEFGHIJKLMNOPQRSTUVWXYZ') <> ''     
    then cast(concat(repeat('0',6-length(ltrim(rtrim(translate(upper(KPBL_BLK_VALUE),'','ABCDEFGHIJKLMNOPQRSTUVWXYZ'))))),rtrim(ltrim(translate(upper(KPBL_BLK_VALUE),'','ABCDEFGHIJKLMNOPQRSTUVWXYZ')))) as char(6))   
    else     cast(KPBL_BLK_VALUE as char(6)) 
  end
  
  
select pmpart, translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ') 
from pdpmast
order by 
  case   
    when translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ') <> ''     
    then cast(concat(repeat('0',25-length(ltrim(rtrim(translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ'))))),rtrim(ltrim(translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ')))) as char(25))   
    else     cast(pmpart as char(25)) 
  end  
  
  
  
select pmpart, pmgrpc,
  case   
    when translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ') <> ''     
    then cast(concat(repeat('0',25-length(ltrim(rtrim(translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ'))))),rtrim(ltrim(translate(upper(pmpart),' ','ABCDEFGHIJKLMNOPQRSTUVWXYZ')))) as char(25))   
    else    pmpart -- cast(pmpart as char(25)) 
  end as SortOrder
from pdpmast  
--where pmgrpc <> ''
--and trim(pmgrpc) <> '00000'
where trim(pmgrpc) = '00293'
order by pmpart

select pmpart, pmgrpc 
from pdpmast
where pmgrpc <> ''
and trim(pmgrpc) <> '00000'
and (
  locate('A', pmpart) <> 0 or
  locate('B', pmpart) <> 0 or
  locate('C', pmpart) <> 0 or
  locate('D', pmpart) <> 0 or
  locate('E', pmpart) <> 0)
  
  
-- Mark groups, include 0 on hand, include yearly sales, exclude Core Charge 
select pmgrpc as "Class", pmpart as "Part #", pmsgrp as "Source", pmbinl as "Bin" , pmonhd as "On Hand", pmdesc as "Description",
(select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA') as "Yearly Sales"
-- select *
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
where trim(pmgrpc) = '07831'
and pmdesc not like '%Core%'
and trim(pmco#) = 'RY1'
--and pmonhd > 0
order by pmgrpc, pmpart  

--Mark single source	
select pmgrpc as "Class", pmpart as "Part #", pmsgrp as "Source", pmbinl as "Bin" , pmonhd as "On Hand", pmdesc as "Description",
(select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA') as "Yearly Sales"
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
where trim(pmsgrp) = '530'
order by (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA') desc 



select * from pdpmrpl where pfoprt = '19177982'















-- Ray: include yearly sales, stocked items (bin <> '')
select pmgrpc as "Class", pmpart as "Part #", pmsgrp as "Source", pmbinl as "Bin" , pmonhd as "On Hand", pmdesc as "Description",
(select coalesce(sum(ptqty), 0) from pdptdet where ptpart = coalesce(ps.pfaprt, pd.pmpart) and ptcode = 'SA') as "Yearly Sales"
-- select *
FROM     pdpmast pd 
         left join pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
where trim(pmgrpc) = '07831'
and pmdesc not like '%Core%'
and trim(pmco#) = 'RY1'
--and pmonhd > 0
order by pmgrpc, pmpart  

