-- Used Car Deals
-- 446A = Certified Cars Sales    646A = Certified Cars Cost    647A = Recon
-- 446B = Other Used Cars Sales   646B = Other Used Cars Cost   647B = Recon
-- 450A = Certified Truck Sales   650A = Certified Trucks Cost  651A = Recon
-- 450B = Other Used Truck Sales  650B = Other Used Trucks Cost 651B = Recon






select x."Stock#", x."Sales" as "Sales", x."COS", x."Recon", x."COS" + x."Recon" as "Cost", x."Sales" + x."COS" + x."Recon" as "Gross" 
from
(select gtctl# as "Stock#", sum(case when trim(fxfact) in ('446A', '446B', '450A', '450B') then gttamt else 0 end) as "Sales", sum(case when trim(fxfact) in ('646A', '646B', '650A', '650B') then gttamt else 0 end) as "COS", 
  sum(case when trim(fxfact) in ('647A', '647B', '651A', '651B') then gttamt else 0 end) as "Recon"
from rydedata.glptrns gl 
  inner join rydedata.ffpxrefdta on gtacct = fxgact and fxcyy = '2011' and fxconsol = '' and trim(fxfact) in ('446A', '646A', '647A', '446B', '646B', '647B', '450A', '650A', '651A', '4580B', '650B', '651B')
where gl.gtdate >= '10/1/2011' 
  and gl.gtdate <= '10/31/2011' 
  and gl.gtpost <> 'V'
group by gtctl#) as x 
order by x."Stock#"


select x."Account", abs(x."Sales") as "Sales", x."COS", x."Recon", x."COS" + x."Recon" as "Cost", abs(x."Sales") - x."COS" - x."Recon" as "Gross" 
from

(select gtacct as "Account", sum(case when trim(fxfact) in ('446A', '446B', '450A', '450B') then gttamt else 0 end) as "Sales", sum(case when trim(fxfact) in ('646A', '646B', '650A', '650B') then gttamt else 0 end) as "COS", 
  sum(case when trim(fxfact) in ('647A', '647B', '651A', '651B') then gttamt else 0 end) as "Recon"
from rydedata.glptrns gl 
  inner join rydedata.ffpxrefdta on gtacct = fxgact and fxcyy = '2011' and fxconsol = '' and trim(fxfact) in ('446A', '646A', '647A', '446B', '646B', '647B', '450A', '650A', '651A', '450B', '650B', '651B')
where gl.gtdate >= '10/1/2011' 
  and gl.gtdate <= '10/31/2011' 
  and gl.gtpost <> 'V'
--  and gl.gtjrnl in ('GJE', 'STD', 'WTD')
  group by gtacct) as x
order by abs(x."Sales")