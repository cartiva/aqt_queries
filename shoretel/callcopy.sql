select count(*) from recordings
select * from recordings

extract raw to extCallCopy
xfmCallCopy: 
  ident, 
  callTS, -- maybe just the date, recording time does not necessarily match call time
  extension,
  guid (modified to match shoretel),
  filename

-- 5/20/14  
Select * from dbo.recordings order by recording_time desc

Select callid from dbo.recordings group by callid having count(*) > 1

-- ident is the primary key
Select ident from dbo.recordings group by ident having count(*) > 1
-- agent_number, device_id, device_alias are always the same
select *
from recordings
where (
  agent_number <> device_id
  or agent_number <> device_alias
  or device_id <> device_alias)  
  
select *
from recordings
where (
  len(rtrim(agent_number)) <> 4  
  or len(rtrim(device_id)) <> 4 
  or len(rtrim(device_alias)) <> 4)
  
select ident
from (  
select ident, agent_number, device_id, device_alias
from recordings
group by ident, agent_number, device_id, device_alias
) x group by ident having count(*) > 1

-- filename size
select min(len(rtrim(ltrim(filename)))), max(len(rtrim(ltrim(filename))))
from dbo.recordings

-- nulls valid  
select min(len(rtrim(ltrim(user2)))) from dbo.recordings
select min(len(rtrim(ltrim(ani)))), min(len(rtrim(ltrim(dnis)))), 
  min(len(rtrim(ltrim(filename)))), min(len(rtrim(ltrim(user1)))),
  min(len(rtrim(ltrim(callid))))
from dbo.recordings
 
-- all recordingtype are the same  
select recording_type, count(*)
from recordings
group by recording_type     


-- curdate:
select CAST(GETDATE() AS DATE)

-- seconds since 1-1-1970 to date

select recording_time,cast(DATEADD(SECOND, recording_time,'01/01/1970') as DATE)
from recordings
order by ident desc


select ident, recording_time, rtrim(ltrim(agent_number)) as agent_number,
  rtrim(ltrim(device_id)) as device_id, rtrim(ltrim(device_alias)) as device_alias,
  rtrim(ltrim(filename)) as filename, ani, dnis, user1, user2, duration, calldirection,
  audio_size, rtrim(ltrim(callid)) as callid
from dbo.recordings
where cast(DATEADD(SECOND, recording_time,'01/01/1970 00:00:00') as DATE) = CAST(GETDATE() AS DATE) 



