select yhdemp, 
  sum(yhcrtm) as total, -- 17 Emplr Curr Ret
  sum(yhdhrs) as Hours,  -- 7 Reg Hours
  sum(yhdtgp) as "1 Total Gross",
  sum(yhdtga) as "2 Total Adj Gross",
  sum(yhcfed) as "3 Curr Federal Tax",
  sum(yhcsse) as "4 Curr FICA",
  sum(yhcssm) as "5 Curr Emplr FICA",
  sum(yhcmde) as "6 Curr Employee Medicare",
  sum(yhcmdm) as "7 Curr Employer Medicare",
  sum(yhceic) as "8 YTD EIC Payments",
  sum(yhcst) as "9 Curr State Tax",
  sum(yhcsde) as "10 Curr Employee SDI",
  sum(yhcsdm) as "11 Curr Employer SDI",
  sum(yhccnt) as "12 Curr County Tax",
  sum(yhccty) as "13 Curr City Tax",
  sum(yhcfuc) as "14 Curr FUTA",
  sum(yhcsuc) as "15 Curr SUTA",
  sum(yhccmp) as "16 Workmans Comp",
  sum(yhcrte) as "17 Emply Curr Ret",
  sum(yhcrtm) as "18 Emplr Curr Ret",
  sum(yhdota) as "19 Overtime Amount",
  sum(yhdded) as "20 Total Deduction",
  sum(yhcopy) as "21 Total Other Pay",
  sum(yhcnet) as "22 Current Net",
  sum(yhctax) as "23 Current Tax Tot",
  sum(yhafed) as "1 Curr Adj Fed",
  sum(yhass) as "2 Curr Adj SS",
  sum(yhafuc) as "3 Curr Adj FUTA",
  sum(yhast) as "4 Curr Adj State",
  sum(yhacn) as "5 Curr Adj Cnty",
  sum(yhamu) as "6 Curr Adj City",
  sum(yhasuc) as "7 Curr Adj SUTA",
  sum(yhacm) as "8 Curr Adj Comp",
  sum(yhdvac) as "1 Vacation Taken",
  sum(yhdhol) as "2 Holday Taken",
  sum(yhdsck) as "3 Sick Leave Taken",
  sum(yhavac) as "4 Vacation Acc",
  sum(yhahol) as "5 Holiday Acc",
  sum(yhasck) as "6 Sick Leave Acc",
  sum(yhdhrs) as "7 Reg Hours",
  sum(yhdoth) as "8 Overtime Hours",
  sum(yhdahr) as "9 Alt Pay Hours",
  sum(yhvacd) as "10 Def Vac Hrs",
  sum(yhhold) as "11 Def Hol Hrs"
from rydedata.pyhshdta -- Payroll Header transaction file
where ypbcyy = '113' -- payroll cen + year
and trim(yhdemp) in ('186100','113020')
group by yhdemp





/*********** PYHSCDTA Payroll Code Transaction File *****************/

select yhcemp, yhccde, yhccds, sum(yhccam), sum(yhccea)
from rydedata.pyhscdta
where ypbcyy = '113'
and trim(yhcemp) in  ('186100','113020')
group by yhcemp, yhccde, yhccds
order by yhcemp, yhccde

select yhccde, yhccds
from rydedata.pyhscdta
where ypbcyy = '110'
group by yhccde, yhccds
order by yhccde


select distinct yhccde, yhccds
from rydedata.pyhscdta
order by yhccde

select *
from rydedata.pyhscdta
where trim(yhccds) like '%ROTH%'

/*** code definitions in pypcodes ********/
select distinct ytdco#, ytddcd, ytddsc
from rydedata.pypcodes
order by ytdco#, ytddcd

select distinct ytdco#, ytddcd, ytddsc
from rydedata.pypcodes
where trim(ytddsc) like '%ROTH%'
order by ytdco#, ytddcd

