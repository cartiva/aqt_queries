select *
from (
  Select employee_name,
    case
      when cast(right(trim(hire_date),2) as integer) < 20 then 
        cast (
          case length(trim(hire_date))
            when 5 then  '20'||substr(trim(hire_date),4,2)||'-'|| '0' || left(trim(hire_date),1) || '-' ||substr(trim(hire_date),2,2)
            when 6 then  '20'||substr(trim(hire_date),5,2)||'-'|| left(trim(hire_date),2) || '-' ||substr(trim(hire_date),3,2)
          end as date) 
      when cast(right(trim(hire_date),2) as integer) >= 20 then  
        cast (
          case length(trim(hire_date))
            when 5 then  '19'||substr(trim(hire_date),4,2)||'-'|| '0' || left(trim(hire_date),1) || '-' ||substr(trim(hire_date),2,2)
            when 6 then  '19'||substr(trim(hire_date),5,2)||'-'|| left(trim(hire_date),2) || '-' ||substr(trim(hire_date),3,2)
          end as date) 
    end as hire_date,
    case
      when cast(right(trim(org_hire_date),2) as integer) < 20 then 
        cast (
          case length(trim(org_hire_date))
            when 5 then  '20'||substr(trim(org_hire_date),4,2)||'-'|| '0' || left(trim(org_hire_date),1) || '-' ||substr(trim(org_hire_date),2,2)
            when 6 then  '20'||substr(trim(org_hire_date),5,2)||'-'|| left(trim(org_hire_date),2) || '-' ||substr(trim(org_hire_date),3,2)
          end as date) 
      when cast(right(trim(org_hire_date),2) as integer) >= 20 then  
        cast (
          case length(trim(org_hire_date))
            when 5 then  '19'||substr(trim(org_hire_date),4,2)||'-'|| '0' || left(trim(org_hire_date),1) || '-' ||substr(trim(org_hire_date),2,2)
            when 6 then  '19'||substr(trim(org_hire_date),5,2)||'-'|| left(trim(org_hire_date),2) || '-' ||substr(trim(org_hire_date),3,2)
          end as date) 
    end as org_hire_date 
  from RYDEDATA.PYMAST
  where active_code <> 'T'
    and pymast_company_number in ('RY1','RY2')) x
where month(hire_date) = 4
  or month(org_hire_date) = 4    

    