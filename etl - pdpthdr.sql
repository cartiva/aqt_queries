Select * from RYDEDATA.PDPTHDR

Select count(*) from RYDEDATA.PDPTHDR  -- 148130

select trim(ptinv#)
from RYDEDATA.PDPTHDR
group by trim(ptinv#)
  having count(*) > 1

-- query for code
select 
  PTCO#, PTINV#, PTDTYP, PTTTYP, PTCPID, PTACTP, 
  cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) as PTDATE,
  PTCKEY 
  PTSKEY, PTPHON, PTSNAM, PTPMTH, PTSTYP, PTPO#, PTREC#, PTPLVL, 
  PTPTOT, PTSHPT, PTSPOD, PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, 
  PTGROS, 
  case
    when cast(PTCREATE as date) = '0001-01-01' then cast('9999-12-31 00:00:00' as timestamp) 
    else PTCREATE 
  end as PTCREATE, 
  case
    when cast(PTCTHOLD as date) = '0001-01-01' then cast('9999-12-31 00:00:00' as timestamp) 
    else PTCTHOLD 
  end as PTCTHOLD, 
  PTAUTH#, PTAUTHID, PTDLVMTH, 
  PTSHIPSEQ, PTTAXGO
-- select count(*)
from rydedata.pdpthdr
where trim(ptinv#) <> ''
  and ptco# in ('RY1','RY2','RY3')
  and trim(ptinv#) not in (
    select trim(ptinv#)
    from RYDEDATA.PDPTHDR
    where trim(ptinv#) <> ''
      and ptco# in ('RY1','RY2','RY3')
    group by trim(ptinv#)
      having count(*) > 1)
/*
      OpenQuery(AdoQuery, 'select ' +
          'PTCO#, PTINV#, PTDTYP, PTTTYP, PTCPID, PTACTP, ' +
          'cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) as PTDATE,  ' +
          'PTCKEY,  PTSKEY, PTPHON, PTSNAM, PTPMTH, PTSTYP, PTPO#, PTREC#, PTPLVL, ' +
          'PTPTOT, PTSHPT, PTSPOD, PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTGROS, ' +
          'case ' +
          '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCREATE ' +
          'end as PTCREATE, ' +
          'case ' +
          '  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCTHOLD ' +
          'end as PTCTHOLD, ' +
          'PTAUTH#, PTAUTHID, PTDLVMTH, ' +
          'PTSHIPSEQ, PTTAXGO ' +
          'from rydedata.pdpthdr ' +
          'where trim(ptinv#) <> '''' ' +
          '  and ptco# in (''RY1'',''RY2'',''RY3'') ' +
          '  and trim(ptinv#) not in ( ' +
          '    select trim(ptinv#) ' +
          '    from RYDEDATA.PDPTHDR ' +
          '    group by trim(ptinv#) ' +
          '      having count(*) > 1)');
*/

select min(ptcreate), max(ptcreate)
from rydedata.pdpthdr
where trim(ptinv#) <> ''
  and ptco# in ('RY1','RY2','RY3')
  and trim(ptinv#) not in (
    select trim(ptinv#)
    from RYDEDATA.PDPTHDR
    where trim(ptinv#) <> ''
      and ptco# in ('RY1','RY2','RY3')
    group by trim(ptinv#)
      having count(*) > 1)

select distinct PTDLVMTH from rydedata.pdpthdr

case
  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) 
  else PTCTHOLD 
end as PTCTHOLD

select min(ptcthold), max(ptcthold)
from rydedata.pdpthdr

select *
from rydedata.pdpthdr
where trim(ptinv#) = '15110347'
