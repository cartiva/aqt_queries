select RO, "Date", "Tech", sum("Hours") as hours, curdate()
from (
  select ptdoc# as "RO", d.ptdate as "Date", pttech as "Tech", ptlhrs as "Hours" 
  from pdppdet d
  inner join rydedata.pdpphdr h on h.ptpkey = d.ptpkey -- no RO# in pdppdet
  --where d.ptdate >= 20090728 and d.ptdate <= 20200101
  where cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date) = curdate()
  and pttech in (
    select sttech
    from rydedata.sdptech t
    inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
    where t.stco# = 'RY1'
    and p.ymdist in ('STEC', 'PR/S')
    and not left(sttech, 1) in ('H', 'M'))
union all
  select  s.ptro# as "RO", s.ptdate as "Date", s.pttech as "Tech", s.ptlhrs as "Hours"
  from sdprdet s
  where s.ptdate >= 20090728 and s.ptdate <= 20200101
  --where s.ptdate = values(curdate())
  and pttech in (
    select sttech
    from rydedata.sdptech t
    inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
    where t.stco# = 'RY1'
    and p.ymdist in ('STEC', 'PR/S')
    and not left(sttech, 1) in ('H', 'M'))) x
where length(trim("RO")) <> 0 
group by RO, "Date", "Tech"



-- this looks to be best for now
Select imstk#, imdinv, curdate(), curdate()
days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date))
from RYDEDATA.INPMAST
where imstat = 'I'



  select d.ptdate,
    left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2),
    cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date),
    curdate(),
    days(curdate()) - days(cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date))
  from pdppdet d
  inner join rydedata.pdpphdr h on h.ptpkey = d.ptpkey -- no RO# in pdppdet
  where d.ptdate <> 0
and cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date) = curdate()




-- this looks pretty good
-- if i understand, closed work is in the service tables, open work is in the parts tables
-- what is the relationship between pdppdet and pdpphdr
-- joined by ptpkey (same field name in both tables) 
-- only 10 records in pdppdet without a matching key in pdpphdr
-- and only pdpphdr records without a matching key in pdppdet

-- 1 row for ro, tech, date
select RO, "Date", "Tech", sum("Hours") as hours, curdate() as TheDate
from ( -- i think this is where he
  select ptdoc# as "RO", d.ptdate as "Date", pttech as "Tech", ptlhrs as "Hours" 
  from pdppdet d
  inner join rydedata.pdpphdr h on h.ptpkey = d.ptpkey -- no RO# in pdppdet
  --where d.ptdate >= 20090728 and d.ptdate <= 20200101
  where d.ptdate <> 0
  and cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date) >= current date - 7 days
  and pttech in ( -- RY1, DIST: STEC & PR/X, no dummy techs
    select sttech
    from rydedata.sdptech t
    inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
    where t.stco# = 'RY1'
    and p.ymdist in ('STEC', 'PR/S')
    and not left(sttech, 1) in ('H', 'M'))
  union all
  select  s.ptro# as "RO", s.ptdate as "Date", s.pttech as "Tech", s.ptlhrs as "Hours"
  from sdprdet s
  --where s.ptdate >= 20090728 and s.ptdate <= 20200101
  where s.ptdate <> 0
  and cast(left(trim(s.ptdate),4)||'-'||substr(trim(s.ptdate),5,2)||'-'||right(trim(s.ptdate),2) as date) >= current date - 7 days
  and pttech in (
    select sttech
    from rydedata.sdptech t
    inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
    where t.stco# = 'RY1'
    and p.ymdist in ('STEC', 'PR/S')
    and not left(sttech, 1) in ('H', 'M'))) x
where length(trim("RO")) <> 0 
group by RO, "Date", "Tech"



-- 1 row for ro, tech, date
-- 7 days back each day?
-- curdate() 7

select h.ptttyp, h.ptdate, d.* 
from pdpphdr h
left join pdppdet d on h.ptpkey = d.ptpkey
where trim(h.ptdoc#) = '16070300'
order by d.ptseq#



-- this looks to be best for now
Select imstk#, imdinv, curdate(),
days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date))
from RYDEDATA.INPMAST
where imstat = 'I'

-- fucking finally, the problem was that pdpdet has records where ptdate is 0
  select ptpkey, d.ptdate as "Date", -- current date - 7 days as "Cur - 7",
    days(curdate()) - days(cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date)) as "Diff"
  from pdppdet d
  where ptdate <> 0
  and cast(left(trim(d.ptdate),4)||'-'||substr(trim(d.ptdate),5,2)||'-'||right(trim(d.ptdate),2) as date) >= current date - 7 days
  and trim(ptpkey) like '363%'


select * from pdppdet where ptdate = 0

select distinct(ptdate) from pdppdet

select distinct(ptdate) from pdpphdr

select distinct(ptdate) from sdprdet
select distinct(ptdate) from sdprhdr


SELECT SUBSTR(CHAR(CURRENT DATE - 30 DAYS),1,4) 
       ||                             
       SUBSTR(CHAR(CURRENT DATE - 30 DAYS),6,2) 
       ||                             
       SUBSTR(CHAR(CURRENT DATE - 30 DAYS),9,2) 
FROM SYSIBM.SYSDUMMY1 



select char(curdate()-30 days-'0000-01-01'+101) 
FROM SYSIBM.SYSDUMMY1

values(curdate()) 

values(current date - 1 DAYS)

-- 9/28, tech 522, does not match productivity report
select "RO", sum("Hours")
from (
  select  s.ptro# as "RO", s.ptdate as "Date", s.pttech as "Tech", s.ptlhrs as "Hours"
  from sdprdet s
  where s.ptdate = 20110928
  and trim(pttech) = '522') x
group by "RO"

-- here it is directly out of the main query
  select sum(ptlhrs)
  from sdprdet d
  where d.ptdate = 20110928
    and pttech in (
      select sttech
      from rydedata.sdptech t
      inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
      where t.stco# = 'RY1'
      --and t.stactv <> 'N'
      and p.ymdist in ('STEC', 'PR/S')
      --and p.ymactv <> 'T'
      and not left(sttech, 1) in ('H', 'M'))
    and trim(pttech) = '522'


-- nothing in pdppdet
  select *
  from pdppdet s
  
  --where s.ptdate >= 20090728 and s.ptdate <= 20200101
  where s.ptdate = 20110928
  and ptpkey = (select ptpkey from pdpphdr where trim(ptdoc#) in ('16070093','16070068', '16070086','16070094','16070093','16069974','18011955'))
  and trim(pttech) = '522'

-- nope, that's not the difference
  select s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, s.ptlhrs, x.*
  from sdprdet s
  left join sdpxtim x on s.ptro# = x.ptro#
  where s.ptdate = 20110928
  and trim(s.pttech) = '522'

select * from sdpxtim where ptdate = 20110928

select * from sdpxtim where trim(ptro#) in ('16070093','16070068', '16070086','16070094','16070093','16069974','18011955')

select sum(ptlhrs) from (
  select  s.ptro#, s.ptdate, s.pttech, s.ptlhrs
  from sdprdet s
  where s.ptdate = 20110928
  and trim(pttech) = '522' )a



