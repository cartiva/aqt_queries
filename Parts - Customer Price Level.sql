/*
9-14
Ray Lee looking for a list of all parts customers and their pricing level
eg Northwest Autobody: PL-6
*/

/*
10-08
Charlie's info
*/
 
select ppkey, ppdesc, ppmeth from pdppric where ppco# = 'RY1'

Select * from PDPPSAS where psco# = 'RY1'

Select distinct length(trim(psrplev)) from PDPPSAS

Select distinct length(trim(psrplev)) from PDPPSAS where psco# = 'RY1'

select pssgrp, 
  substr(trim(psrplev), 1, 3) as "1",
  substr(trim(psrplev), 4, 3) as "2",
  substr(trim(psrplev), 7, 3) as "3",
  substr(trim(psrplev), 10, 3) as "4",
  substr(trim(psrplev), 13, 3) as "5",
  substr(trim(psrplev), 16, 3) as "6",
  substr(trim(psrplev), 19, 3) as "7",
  substr(trim(psrplev), 22, 3) as "8",
  substr(trim(psrplev), 25, 3) as "9",
  substr(trim(psrplev), 28, 3) as "10",
  substr(trim(psrplev), 31, 3) as "11",
  substr(trim(psrplev), 34, 3) as "12",
  substr(trim(psrplev), 37, 3) as "13",
  substr(trim(psrplev), 40, 3) as "14",
  substr(trim(psrplev), 43, 3) as "15",
  substr(trim(psrplev), 46, 3) as "16",
  substr(trim(psrplev), 49, 3) as "17",
  substr(trim(psrplev), 52, 3) as "18",
  substr(trim(psrplev), 55, 3) as "19",
  substr(trim(psrplev), 58, 3) as "20"
from PDPPSAS 
where psco# = 'RY1'
and pssgrp = ''


-- 6/9/2011

/* 
attempting to get the data available from
Parts -> 30
goto Advanced Collision Center (124240)
F7: shows a Parts Price Level (Counter) of PL-6
query shows PDPCUST.PJPLVL = 10
wtf?
*/



--6/23
-- right column is what shows in UI
-- left column is the value from pdpcust = column header from pdppsas
-- just the fucking Pricing Stragegy Assignments screen, numbered from top to bottom
-- ie, the 10th row on that page says PL-6, and the 10 column in pdppsas has a value of 17
-- that (17) is the pdppric.ppkey value (list less 30%)
1:  Retail
2:  Service Customer Pay
3.  Interanl
4.  Cores
5.  PL-1
6.  PL-2
7.  PL-3
8.  PL-4
9.  PL-5
10. PL-6
11. PL-7
12. PL-8
13. PL-9
14. PL-10
15. PL-11
16. PL-12
17. PL-13
18. PL-14
19. PL-15
20. PL-16




-- here are the customer pricing levels
-- the column header is the price level from pdpcust
-- the values are the pdppric key values
select  
  cast(substr(trim(psrplev), 1, 3) as integer) as "1",
  cast(substr(trim(psrplev), 4, 3) as integer) as "2",
  cast(substr(trim(psrplev), 7, 3) as integer) as "3",
  cast(substr(trim(psrplev), 10, 3) as integer) as "4",
  cast(substr(trim(psrplev), 13, 3) as integer) as "5",
  cast(substr(trim(psrplev), 16, 3) as integer) as "6",
  cast(substr(trim(psrplev), 19, 3) as integer) as "7",
  cast(substr(trim(psrplev), 22, 3) as integer) as "8",
  cast(substr(trim(psrplev), 25, 3) as integer) as "9",
  cast(substr(trim(psrplev), 28, 3) as integer) as "10",
  cast(substr(trim(psrplev), 31, 3) as integer) as "11",
  cast(substr(trim(psrplev), 34, 3) as integer) as "12",
  cast(substr(trim(psrplev), 37, 3) as integer) as "13",
  cast(substr(trim(psrplev), 40, 3) as integer) as "14",
  cast(substr(trim(psrplev), 43, 3) as integer) as "15",
  cast(substr(trim(psrplev), 46, 3) as integer) as "16",
  cast(substr(trim(psrplev), 49, 3) as integer) as "17",
  cast(substr(trim(psrplev), 52, 3) as integer) as "18",
  cast(substr(trim(psrplev), 55, 3) as integer) as "19",
  cast(substr(trim(psrplev), 58, 3) as integer) as "20"
from PDPPSAS 
where psco# = 'RY1'
and pssgrp = ''

-- 6/24, this finally is it

select distinct g.gcsnam as name, trim(g.gccus#) as "Cust No", b.bncity as City, b.bnstcd as state,
  case p.pjplvl
	when 1 then 'Retail'
	when 2 then 'Service Customer Pay'
	when 3 then 'Internal'
	when 4 then 'Cores'
	when 5 then 'PL-1'
	when 6 then 'PL-2'
	when 7 then 'PL-3'
	when 8 then 'PL-4'
	when 9 then 'PL-5'
	when 10 then 'PL-6'
	when 11 then 'PL-7'
	when 12 then 'PL-8'
	when 13 then 'PL-9'
	when 14 then 'PL-10'
	when 15 then 'PL-11'
	when 16 then 'PL-12'
	when 17 then 'PL-13'
	when 18 then 'PL-14'
	when 19 then 'PL-15'
	when 20 then 'PL-16'
  end as "Price Level",
  case p.pjplvl
	when 1 then (select ppdesc from pdppric where ppkey = 22 and ppco# = 'RY1')
	when 2 then (select ppdesc from pdppric where ppkey = 1 and ppco# = 'RY1')
	when 3 then (select ppdesc from pdppric where ppkey = 1 and ppco# = 'RY1')
	when 4 then (select ppdesc from pdppric where ppkey = 2 and ppco# = 'RY1')
	when 5 then (select ppdesc from pdppric where ppkey = 6 and ppco# = 'RY1')
	when 6 then (select ppdesc from pdppric where ppkey = 4 and ppco# = 'RY1')
	when 7 then (select ppdesc from pdppric where ppkey = 6 and ppco# = 'RY1')
	when 8 then (select ppdesc from pdppric where ppkey = 3 and ppco# = 'RY1')
	when 9 then (select ppdesc from pdppric where ppkey = 1 and ppco# = 'RY1')
	when 10 then (select ppdesc from pdppric where ppkey = 17 and ppco# = 'RY1')
	when 11 then (select ppdesc from pdppric where ppkey = 19 and ppco# = 'RY1')
	when 12 then (select ppdesc from pdppric where ppkey = 20 and ppco# = 'RY1')
	when 13 then (select ppdesc from pdppric where ppkey = 14 and ppco# = 'RY1')
	when 14 then (select ppdesc from pdppric where ppkey = 7 and ppco# = 'RY1')
	when 15 then (select ppdesc from pdppric where ppkey = 2 and ppco# = 'RY1')
	when 16 then (select ppdesc from pdppric where ppkey = 10 and ppco# = 'RY1')
	when 17 then (select ppdesc from pdppric where ppkey = 5 and ppco# = 'RY1')
	when 18 then (select ppdesc from pdppric where ppkey = 22 and ppco# = 'RY1')
	when 19 then (select ppdesc from pdppric where ppkey = 21 and ppco# = 'RY1')
	when 20 then (select ppdesc from pdppric where ppkey = 23 and ppco# = 'RY1')
  end as Description
from pdpcust p
left join glpcust g on g.gckey = p.pjkey -- for customer name
left join bopname b on trim(g.gcsnam) = trim(b.bnsnam) -- for cust # & address
  and b.bnco# = 'RY1'
where p.pjplvl between 1 and 23
order by g.gcsnam, b.bncity






