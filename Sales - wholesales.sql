-- select distinct sale_type
-- Select * 
select c.*, d.date_in_invent, d.date_delivered
from (
  select bopmast_Stock_number, bopmast_vin, bopmast_search_name, date_capped,
    odometer_at_sale, 
    sum(b.gttamt) as amount, min(gtdate) as from_date, max(gtdate) as thru_date, 
    max(gtdate) - min(gtdate) as age, b.gtacct
  from RYDEDATA.BOPMAST a
  left join rydedata.glptrns b on trim(bopmast_stock_number) = trim(b.gtctl#)
    and gtjrnl = 'VSU'
  inner join rydedata.glpmast bb on b.gtacct = bb.account_number
    and bb.year = 2017
    and bb.account_type = 4
  where sale_type = 'W'
    and b.gtdate > '2015-12-31'
    and trim(bopmast_search_name) <> 'RYDELL AUTO CENTER'
  group by bopmast_Stock_number, bopmast_vin, bopmast_search_name, date_capped,
    odometer_at_sale, b.gtacct) c
left join rydedata.inpmast d on trim(c.bopmast_stock_number) = trim(d.inpmast_stock_number)    
order by trim(bopmast_stock_number)


select c.bopmast_search_name, count(*)
from (
  select bopmast_Stock_number, bopmast_vin, bopmast_search_name, date_capped,
    odometer_at_sale, 
    sum(b.gttamt) as amount, min(gtdate) as from_date, max(gtdate) as thru_date, 
    max(gtdate) - min(gtdate) as age, b.gtacct
  from RYDEDATA.BOPMAST a
  left join rydedata.glptrns b on trim(bopmast_stock_number) = trim(b.gtctl#)
    and gtjrnl = 'VSU'
  inner join rydedata.glpmast bb on b.gtacct = bb.account_number
    and bb.year = 2017
    and bb.account_type = 4
  where sale_type = 'W'
    and b.gtdate > '2015-12-31'
  group by bopmast_Stock_number, bopmast_vin, bopmast_search_name, date_capped,
    odometer_at_sale, b.gtacct) c
left join rydedata.inpmast d on trim(c.bopmast_stock_number) = trim(d.inpmast_stock_number)    
group by bopmast_search_name


