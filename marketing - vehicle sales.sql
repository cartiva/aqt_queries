/*
Hi John,

Here is the information that I was hoping that you can pull. 

If I could get this information broken down into three spreadsheets: New Vehicle Purchase, 
Used Vehicle Purchase, Leased Vehicle.  All of this with the timeframe of the last 5 years 
with the start date of July 31st 2016.

Here is the information that I would be looking for on each of these spreadsheets

Name
Address
Date of Birth
Reported Income (I am not sure if we record this information)
Email 
Purchase Date
Purchased/Leased Vehicle Make, Model and Year
Purchase Price
Length of Lease (for leased vehicles only)

Please let me know if you have any questions, need further detail, or there is something that we are not able to pull.

Thank You,
Morgan 
*/
--   sale_type, vehicle_type, 
-- New Veh Purchase
Select b.last_company_name, b.first_name, b.middle_init,
  b.address_1 as address, b.city, b.county, b.state_code as state, b.zip_code as zip,  
  b.birth_date, 
  b.email_address, b.second_email_Address2, 
  a.date_capped as sale_date, 
  c.year, c.make, c.model,
  retail_price-- , lease_price, lease_term,
  -- sale_type, vehicle_type,
  -- bopmast_stock_number
from rydedata.bopmast a 
left join rydedata.bopname b on a.buyer_number = b.bopname_record_key 
left join rydedata.inpmast c on a.bopmast_vin = c.inpmast_vin
where date_capped between '08/01/2011' and '07/31/2016'
  and vehicle_type = 'N'
  and sale_type = 'R';

-- Lease
Select b.last_company_name, b.first_name, b.middle_init,
  b.address_1 as address, b.city, b.county, b.state_code as state, b.zip_code as zip,  
  b.birth_date, 
  b.email_address, b.second_email_Address2, 
  a.date_capped as sale_date, 
  c.year, c.make, c.model,
  a.lease_price, a.lease_term
  -- sale_type, vehicle_type,
  -- bopmast_stock_number
from rydedata.bopmast a 
left join rydedata.bopname b on a.buyer_number = b.bopname_record_key 
left join rydedata.inpmast c on a.bopmast_vin = c.inpmast_vin
where date_capped between '08/01/2011' and '07/31/2016'
  and vehicle_type = 'N'
  and sale_type = 'L';
  
-- Used Veh Purchase
Select b.last_company_name, b.first_name, b.middle_init,
  b.address_1 as address, b.city, b.county, b.state_code as state, b.zip_code as zip,  
  b.birth_date, 
  b.email_address, b.second_email_Address2, 
  a.date_capped as sale_date, 
  c.year, c.make, c.model,
  retail_price-- , lease_price, lease_term,
  -- sale_type, vehicle_type,
  -- bopmast_stock_number
from rydedata.bopmast a 
left join rydedata.bopname b on a.buyer_number = b.bopname_record_key 
left join rydedata.inpmast c on a.bopmast_vin = c.inpmast_vin
where date_capped between '08/01/2011' and '07/31/2016'
  and vehicle_type = 'U'
  and sale_type = 'R';