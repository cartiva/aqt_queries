-- Current Inventory Cost
select trim(gtctl#), sum(gttamt) as Cost
from rydedata.glptrns
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
group by gtctl#
having sum(gttamt) > 0

select *
from rydedata.glptrns
where trim(gtctl#) = '17802A'

-- Current Recon Costs (Vehicles in Inventory)
select trim(gtctl#) as StockNum, sum(gttamt) as Recon
from rydedata.glptrns
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  and trim(gtjrnl) in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
  and gtctl# in (select gtctl# from rydedata.glptrns where trim(gtacct) in ('124000', '124100') and trim(gtpost) <> 'V' group by gtctl# having sum(gttamt) > 0) -- Get only the vehicles in inventory
group by gtctl#
having sum(gttamt) > 0
-- Sold Amounts for Retail Sales
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SoldAmount
from rydedata.glptrns
where trim(gtacct) in ('144600', '144601', '145000', '145001') -- Used vehicle retail sales accounts
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Sold Amounts for Wholesale Sales
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SoldAmount
from rydedata.glptrns
where trim(gtacct) in ('144800', '145200') -- Used vehicle wholesale sales accounts
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Cost at Time of Retail Sale
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SalesCost
from rydedata.glptrns
where trim(gtacct) in ('164600', '164700', '164601', '164701', '165000', '165100', '165001', '165101') -- Used vehicle retail cost of sales accounts including Recon
  and trim(gtjrnl) in ('VSU') -- Journal = Used Vehicle Sales
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Cost at Time of Wholesale Sale
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SalesCost
from rydedata.glptrns
where trim(gtacct) in ('164800', '165200') -- Used vehicle wholesale cost of sales accounts 
  and trim(gtjrnl) in ('VSU') -- Journal = Used Vehicle Sales
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Cost of Sales after Retail Sale
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SalesCost
from rydedata.glptrns
where trim(gtacct) in ('164600', '164700', '164601', '164701', '165000', '165100', '165001', '165101') -- Used vehicle retail cost of sales accounts including Recon
  and trim(gtjrnl) not in ('VSU') -- Journal = Used Vehicle Sales
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Cost of Sales after Wholesale Sale
select trim(gtctl#) as StockNum, abs(sum(gttamt)) as SalesCost
from rydedata.glptrns
where trim(gtacct) in ('164800', '165200') -- Used vehicle wholesale cost of sales accounts 
  and trim(gtjrnl) not in ('VSU') -- Journal = Used Vehicle Sales
  and trim(gtpost) <> 'V' -- Ignore VOIDS
  and gtdate >= '9/1/2010'
group by gtctl#
having sum(gttamt) <> 0
-- Get all transactions for a stock number
select *
from rydedata.glptrns
where trim(gtacct) in ('144600', '144601', '145000', '145001') -- Used vehicle retail sales accounts
  and trim(gtctl#) = '99990XX'

select *
from rydedata.glptrns
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtctl#) = '99990XX'

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Current Inventory Cost, Recon
select trim(gtctl#), sum(gttamt) as Cost,
  (select sum(gttamt) from rydedata.glptrns where trim(gtctl#) = trim(g.gtctl#) and trim(gtjrnl) in ('SVI', 'SWA', 'SCA'))
from rydedata.glptrns g
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
group by gtctl#
having sum(gttamt) > 0


select * from glptrns where trim(gtctl#) = '15022'


SELECT *
FROM rydedata.Bopmast
WHERE trim(bmvin) = '1GTEK39089Z168045'
