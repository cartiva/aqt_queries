select pt.YIEMP# as "Emp #", sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)) as "Arkona",
  sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) as "w/Secs",
  Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) -
    sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)), 2) as "Diff",
  Round(Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) -
    sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)), 2) * avg(py.ymrate), 2) as "$ Diff"
from pypclockin pt
  left outer join pymast py on pt.YICO# = py.YMCO# and pt.YIEMP# = py.YMEMPN
where pt.YICO# = 'RY1' and pt.YICLKIND between '8/2/2009' and '8/15/2009'
group by pt.YIEMP#
union all
select 'Total', sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)),
  sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)),
  Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) -
    sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)), 2),
  Round(Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) -
    sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60, 2)), 2) * avg(py.ymrate), 2)
from pypclockin pt
  left outer join pymast py on pt.YICO# = py.YMCO# and pt.YIEMP# = py.YMEMPN
where pt.YICO# = 'RY1' and pt.YICLKIND between '8/2/2009' and '8/15/2009'

select (yiclkoutt-yiclkint) as Diff, digits(yiclkoutt-yiclkint), yiclkoutt, yiclkint
--select *
from pypclockin
where yico# = 'RY1'
and yiclkind between '8/2/2009' and '8/15/2009'


select (yiclkoutt-yiclkint) as Diff, digits(yiclkoutt-yiclkint) as Digits, real(digits(yiclkoutt-yiclkint)) as "Real(Digits)",
  yiclkoutt, yiclkint
--select *
from pypclockin
where yico# = 'RY1'
and yiclkind between '8/2/2009' and '8/15/2009'

