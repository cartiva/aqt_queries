

-- 11/21/15 parts inventory
/*
As you may or may not know, we are doing inventory this weekend.  
We�re requesting your help with something if you are available.  
After we are done counting and entering, (which will be Saturday afternoon) one of the numbers 
we need to reconcile is �non GM parts on bodyshop ROs�  They would all parts on open ROs from 
stocking group 920.  Will you be around tomm?  If not, are you able to show me or 
Mark how to pull that number??  
*/

-- i believe this is all the detail i need
select ptdoc# as RO, ptpart as "Part Number", ptqty as Quantity, pmcost as Cost,
  pmlist as List
--select sum(ptqty * pmcost), sum(ptqty * pmlist)  -- 35913/54398
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
where a.ptdtyp = 'RO'
  and a.ptco# = 'RY1'
  and b.ptpart <> ''
  and c.pmmanf = 'OT'
  and c.stocking_group = '920'
  and ptqty > 0
  

-- these are the numbers dan wants  
--select a.ptco#, a.ptdoc# as RO, ptpart as "Part Number", ptqty as Quantity, pmcost as Cost,
--  pmlist as List
select sum(ptqty * pmcost), sum(ptqty * pmlist)  -- 2015: 35913/54398, 2016: 39365/56938
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
where a.ptdtyp = 'RO'
  --and left(trim(a.ptdoc#), 2) = '18'
  and b.ptpart <> ''
  and c.pmmanf = 'OT'
  and c.stocking_group = '920'
  and ptqty > 0  
  and a.ptco# = 'RY1'

-- leave out the ros, group by partnumber
--select ptdoc# as RO, ptpart as "Part Number", ptqty as Quantity, pmcost as Cost,
--  pmlist as List
--select * from ( -- there are no differences in min/max cost/list
--select b.ptpart as "Part Number", sum(ptqty) as Quantity, max(pmcost) as max_cost,
--  min(pmcost) as min_cost, max(pmlist) as max_list, min(pmlist) as min_list
select b.ptpart as "Part Number", sum(ptqty) as Quantity, max(pmcost) as cost,
  max(pmlist) as list
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
where a.ptdtyp = 'RO'
  and a.ptco# = 'RY1'
  and b.ptpart <> ''
  and c.pmmanf = 'OT'
  and c.stocking_group = '920'
  and ptqty > 0
group by b.ptpart  
order by b.ptpart
-- ) x where max_cost <> min_cost or max_list <> min_list

Acct 124205: BODY SHO P&A NON-GM INVEN

SELECT gtco#, month(gtdate),
  SUM(CASE WHEN gttamt < 0 THEN gttamt END) AS credit,
  SUM(CASE WHEN gttamt > 0 THEN gttamt END) AS debit
FROM rydedata.glptrns
WHERE gtacct = '124205'
  AND year(gtdate) = 2013
GROUP BY gtco#, month(gtdate) 

SELECT gtco#, gtdate, gttamt, gtdoc#
FROM rydedata.glptrns
WHERE gtacct = '124205'
  AND year(gtdate) = 2013
GROUP BY gtco#, month(gtdate) 



select trim(a.ptro#), a.ptdate, b.*
from rydedata.sdprdet a
inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
  and a.ptline = b.ptline
  and b.ptmanf = 'OT'
where left(trim(a.ptro#), 2) = '18'
  and a.ptdate > 20130000



SELECT month(gtdate),
  SUM(CASE WHEN gttamt < 0 THEN gttamt END) AS credit,
  SUM(CASE WHEN gttamt > 0 THEN gttamt END) AS debit
FROM rydedata.glptrns
WHERE gtacct = '124205'
  AND year(gtdate) = 2013
  and trim(gtdoc#) in (
    select trim(a.ptro#)
    from rydedata.sdprdet a
    inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
      and a.ptline = b.ptline
      and b.ptmanf = 'OT'
    where left(trim(a.ptro#), 2) = '18'
      and a.ptdate > 20130000)
GROUP BY month(gtdate) 



select *
from rydedata.pdptdet
where trim(ptinv#) = '18026042'

select *
from rydedata.pdptdet
where ptcost = 55.40
order by ptdate desc



