Select prco#, prname, prcpid, prpwrd, premp#,
-- select *
  prsc01 as "Counter Sales",
  prsc02 as "Counter Returns",
  prsc03 as "Repair Order Sales",
  prsc04 as "Repair Order Approval",
  prsc05 as "Change Counterpersons",
  prsc06 as "Change A/R Customer",
--  prsc07,
  prsc08 as "Change Price Level",
  prsc09 as "Override Price",
  prsc10 as "Sell Below Cost",
  prsc11 as "Display Gross Profit",
  prsc12 as "Display Invoices",
  prsc13 as "Move Parts to Repair Order",
  prsc14 as "Display Sales History",
  prsc15 as "Part Number Entry/Change",
  prsc16 as "Inventory Adjustments",
  prsc17 as "Repair Order Returns",
  prsc18 as "Review Journal Entries",
  prsc19 as "Accept Credit Limit Exceeded",
  prsc20 as "Internal Sales",
  prsc21 as "Internal Returns",
  prsc22 as "Repair Order Entry",
  prsc23 as "Override Special Order Deposit",
  prsc24 as "Change Bin Location",
  prsc25 as "Retrieve Invoice from Cashier",
  prsc26 as "Discounts",
  prsc27 as "Remove Restocking Charge",
  prsc28 as "Remove Fees",
  prsc29 as "Allow Release Hold"
--  prsc30
from PDPCTRP
where prco# = 'RY1'
and practive <> 'N'

-- which active Counterperson can Adjust Inventory
select * from PDPCTRP where prsc16 <> 'N' and prco# = 'RY1' and practive <> 'N'
	
