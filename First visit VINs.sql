-- First visit vins for the month of August at PDQ
select s.ptvin, s.ptro#, b.bvsdat
from bopvref b
inner join sdprhdr s on s.ptvin = b.bvvin
inner join sdprdet sd on sd.ptro# = s.ptro#
where bvsdat between 20090801 and 20090831
and sd.PTLOPC = 'PDQ'

select bvsdat, count(*)
from rydelf.bolvree b
inner join sdprhdr s on s.ptvin = b.bvvin
inner join sdprdet sd on sd.ptro# = s.ptro#
where bvsdat between 20090601 and 20090731
and (sd.PTLOPC = 'PDQ' or sd.PTLOPC = '0')
group by b.bvsdat