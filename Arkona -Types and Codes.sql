/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)
/*  PDPTDET */
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

/*  PYMAST  */
ymclas - Payroll Class
  H - Hourly
  S - Salary
  C - Commission
  E - Executive
  
/*  pyhshdta -- Payroll Header transaction file  */
/* payroll codes */
select yhdemp, 
  sum(yhcrtm) as total, -- 17 Emplr Curr Ret
  sum(yhdhrs) as Hours,  -- 7 Reg Hours
  sum(yhdtgp) as "1 Total Gross",
  sum(yhdtga) as "2 Total Adj Gross",
  sum(yhcfed) as "3 Curr Federal Tax",
  sum(yhcsse) as "4 Curr FICA",
  sum(yhcssm) as "5 Curr Emplr FICA",
  sum(yhcmde) as "6 Curr Employee Medicare",
  sum(yhcmdm) as "7 Curr Employer Medicare",
  sum(yhceic) as "8 YTD EIC Payments",
  sum(yhcst) as "9 Curr State Tax",
  sum(yhcsde) as "10 Curr Employee SDI",
  sum(yhcsdm) as "11 Curr Employer SDI",
  sum(yhccnt) as "12 Curr County Tax",
  sum(yhccty) as "13 Curr City Tax",
  sum(yhcfuc) as "14 Curr FUTA",
  sum(yhcsuc) as "15 Curr SUTA",
  sum(yhccmp) as "16 Workmans Comp",
  sum(yhcrte) as "17 Emply Curr Ret",
  sum(yhcrtm) as "18 Emplr Curr Ret",
  sum(yhdota) as "19 Overtime Amount",
  sum(yhdded) as "20 Total Deduction",
  sum(yhcopy) as "21 Total Other Pay",
  sum(yhcnet) as "22 Current Net",
  sum(yhctax) as "23 Current Tax Tot",
  sum(yhafed) as "1 Curr Adj Fed",
  sum(yhass) as "2 Curr Adj SS",
  sum(yhafuc) as "3 Curr Adj FUTA",
  sum(yhast) as "4 Curr Adj State",
  sum(yhacn) as "5 Curr Adj Cnty",
  sum(yhamu) as "6 Curr Adj City",
  sum(yhasuc) as "7 Curr Adj SUTA",
  sum(yhacm) as "8 Curr Adj Comp",
  sum(yhdvac) as "1 Vacation Taken",
  sum(yhdhol) as "2 Holday Taken",
  sum(yhdsck) as "3 Sick Leave Taken",
  sum(yhavac) as "4 Vacation Acc",
  sum(yhahol) as "5 Holiday Acc",
  sum(yhasck) as "6 Sick Leave Acc",
  sum(yhdhrs) as "7 Reg Hours",
  sum(yhdoth) as "8 Overtime Hours",
  sum(yhdahr) as "9 Alt Pay Hours",
  sum(yhvacd) as "10 Def Vac Hrs",
  sum(yhhold) as "11 Def Hol Hrs"
from pyhshdta -- Payroll Header transaction file
where ypbcyy = '110' -- payroll cen + year
and trim(yhdemp) in ('145840','1103050','196341')
group by yhdemp  

/*  SDPRDET */
Column: PTLTYP
Values:                 A  is a Header Record
                        I  Internal Deductible
                        L  Labor Line                           
                        M  Paint/Materials
                        N  Sublet Line
                        Q  Discount
                        W  Hazardous Materials

Column:  PTCODE
Values:                 SC  Service Contract Info Line
                        SL  Sublet Sale Type
                        PM  Paint/Materials Line
                        PO  Purchase Order Line                         
                        CP  Customer Pay Info Line
                        PR  Estimate Data
                        CR  Correction Data
                        WS  Warranty Info Pay Line
                        IS  Internal Flag Info          
                        TT  Tech Time Flag
                        HZ  Hazardous Materials Line

Column:  PTDBAS
Values:                 X   Exclude from Tech Time Report
                        V   Voided Tech time
                        
/*  BOPMAST */
FIELD   Field Def      VALUE      
BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer
                        
                        
/*  GLPTRNS */                        
FIELD    Field Def      VALUE      Generated From                
GTDTYP   Doc Type        B          Deals                
GTDTYP   Doc Type        C          Checks                
GTDTYP   Doc Type        D          Deposits                
GTDTYP   Doc Type        I          Inventory Purchase/Stock In            
GTDTYP   Doc Type        J          Conversion or GJE                
GTDTYP   Doc Type        O          Invoice/PO Entry                
GTDTYP   Doc Type        P          Parts Ticket                
GTDTYP   Doc Type        R          Cash Receipts                
GTDTYP   Doc Type        S          Service Tickets                
GTDTYP   Doc Type        W          Handwrittn Checks                
GTDTYP   Doc Type        X          Bank Rec (Fee, Service Charges, etc.)          
GTPOST   Post Status                In Process of Writing to GLPTRNS and GLPMAST (Most Likely)  
GTPOST   Post Status     V          Voided Entry                
GTPOST   Post Status     Y          Posted Entry                   
GTRDTYP  Recon Doc Type             N/A  (It is possible for a document to reconcile with out a Reconciling Doc Type
GTRDTYP  Recon Doc Type  B          Same as Codes for Document Type              
GTRDTYP  Recon Doc Type  C           ""             
GTRDTYP  Recon Doc Type  D           ""            
GTRDTYP  Recon Doc Type  I           ""                
GTRDTYP  Recon Doc Type  J           ""                   
GTRDTYP  Recon Doc Type  O           ""                  
GTRDTYP  Recon Doc Type  P           ""                  
GTRDTYP  Recon Doc Type  R           ""                  
GTRDTYP  Recon Doc Type  S           ""                   
GTRDTYP  Recon Doc Type  X           ""                 
GTTYPE   Record Type                Other*   *Any account that does not require a special reconciliation program.             
GTTYPE   Record Type     $          Entry to a Bank Account              
GTTYPE   Record Type     C          Entry from a Cash Drawer              
GTTYPE   Record Type     M          Other*                
GTTYPE   Record Type     P          Entry to/from Payables              
GTTYPE   Record Type     R          Entry to/from Receiveables            
                          
/*  SDPWDDIT  - Warranty Download Detail  */
WDWTYPE - Warranty Type   
W = Warranty
C = Customer Pay
I = Internal Pay
S = SVC Other

WDTYPE - Claim Type 
�  Mazda 
o  A - Basic Warranty
o  B - Emission Warranty
o  C - MNAO Internal Use
o  E - Battery Warranty
o  H - Fleet Service Plan
o  J - Anti-Perforation Warranty
o  K - OTC Parts
o  P - Parts Warranty
o  R - Recall
�  Ford 
o  A - Appeal Repair
o  C - Esp Competive Make
o  F - Fleet Service Plan
o  I - Intransit Loss/Damage
o  L - Service Loaner
o  P - Service Part
o  R - Recall/Owner Notification
�  Nissan 
o  15 - PDI Claims
o  19 - Basic Claims
o  21 - Factory Modifications
o  23 - Tire Claims
o  33 - Recall
o  35 - Recall
o  37 - Recall
o  39 - Adjustments to paid/denied

WDLSTAT - Line Status
D = Download
E = Error
P = Pending
S = Submitted

WDLTYPE = Line Code Type
A = Header
B = Labor
C = Parts
D = Comment
E = Undefined Labor
F = Cause
G = Correction
H = Sublet
I = Internal
J =
L = Subaru Job Line
M = Miscellaneous Charges Line
N = Extended Correction
V = Additional Values (ie; manufacturer specific values needed for each line item)


select ptstyp, count(*)
from rydedata.pdpthdr
group by ptstyp


select ptcpid, ptstyp, sum(ptptot)
from rydedata.pdpthdr
where ptdate > 20111130
group by ptcpid, ptstyp
order by ptcpid

                      
select *
from rydedata.pdpthdr
where trim(ptinv#) = '19093847'

select *
from rydedata.pdpthdr
where trim(ptsnam) like 'CARLSON%'
                        