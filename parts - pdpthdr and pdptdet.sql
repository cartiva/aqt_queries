Select * from RYDEDATA.PDPTHDR where trim(ptinv#)  = '16151153'

Select transaction_type, count(*) from RYDEDATA.PDPTHDR where trans_date > 20140000 group by transaction_type

Select document_type, count(*) from RYDEDATA.PDPTHDR where trans_date > 20140000 group by document_type


Select * from RYDEDATA.pdptdet where trim(ptinv#)  = '16151153'


Select distinct left(trim(invoice_number), 4) from RYDEDATA.PDPTHDR where trans_date = 20140108

Select * from RYDEDATA.PDPTHDR where trans_date = 20150108

ok, it appears that there are no rows in pdpphdr for ros, only parts invoices
so, ros will have to done from pdptdet
but the question is, do pdptdet rows reference anything in pdpphdr, ie, is there 
  anything in pdptdet that correlates to a parts invoice #, of course, the invoice #
  
it is just that there is nothing in pdpthdr that correlates to an ro transaction (inv = ro) in pdptdet  

invoice 15242677

select * from rydedata.pdpthdr where trim(invoice_number) = '15242677'

select * from rydedata.pdptdet where trim(ptinv#) = '15242677'

-- yep look at all those ros
select * from rydedata.pdptdet a where ptdate = 20150108 and ptcode 
    not in ('SA','RC','OR','DP','AP','BO') /* counter sales, received, ordered, delete part, add part, back ordered */
  and not exists (
    select 1 from rydedata.pdpthdr where trim(invoice_number) = trim(a.ptinv#))
    
    
select ptco#, ptinv#, ptline, ptseq#
from rydedata.pdptdet
where ptdate > 20130000
  and ptcode not in ('SA','RC','OR','DP','AP','BO','LA') /* counter sales, received, ordered, delete part, add part, back ordered, lifo adjustment */
group by ptco#, ptinv#, ptline, ptseq#
having count(*) > 1

-- something goofy here, returns transactions with ptcode = 'AP'
select *
from rydedata.pdptdet a
inner join (
  select ptco#, ptinv#, ptline, ptseq#
  from rydedata.pdptdet
  where ptdate > 20130000
    and ptcode not in ('SA','RC','OR','DP','AP','BO','LA') /* counter sales, received, ordered, delete part, add part, back ordered, lifo adjustment */
  group by ptco#, ptinv#, ptline, ptseq#
  having count(*) > 1) b on a.ptco# = b.ptco# and a.ptinv# = b.ptinv# and a.ptline = b.ptline and a.ptseq# = b.ptseq#
order by a.ptco#, a.ptinv#, a.ptline, a.ptseq#  
    


-- something goofy here, returns transactions with ptcode = 'AP'
-- exclude AP from a
-- can not get this to run
select *
from rydedata.pdptdet a
inner join (
  select ptco#, ptinv#, ptline, ptseq#
  from rydedata.pdptdet
  where ptdate > 20140000
    and ptcode not in ('SA','RC','OR','DP','AP','BO','LA') /* counter sales, received, ordered, delete part, add part, back ordered, lifo adjustment */
  group by ptco#, ptinv#, ptline, ptseq#
  having count(*) > 1) b on a.ptco# = b.ptco# and a.ptinv# = b.ptinv# and a.ptline = b.ptline and a.ptseq# = b.ptseq#
where a.ptcode not in ('SA','RC','OR','DP','AP','BO','LA')  
order by a.ptco#, a.ptinv#, a.ptline, a.ptseq#  
    
select * from rydedata.pdptdet where ptdate > 20130000 and ptcode in ('US','CU','WU','IU','SU')



select * from rydedata.pdptdet where trim(ptpart) = '10137486' order by ptdate desc
    
    
