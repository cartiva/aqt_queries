-- kim needs crookston total wages, position from crookston 9/1 thru curdate()

  select *
  from rydedata.pyhshdta
  where yhdco# = 'RY3'
    and (
      (yhdeyy = 12 and yhdemm > 8)
      or yhdeyy = 13)


select distinct b.ymname as Name, a.TotalGross as "Total Gross", d.yrtext as Job
from (
  select yhdemp, sum(yhdtgp) as TotalGross
  from rydedata.pyhshdta
  where yhdco# = 'RY3'
    and (
      (yhdeyy = 12 and yhdemm > 8)
      or yhdeyy = 13)
  group by yhdemp) a
left join rydedata.pymast b on a.yhdemp = b.ymempn
left join rydedata.pyprhead c on a.yhdemp = c.yrempn
left join rydedata.pyprjobd d on c.yrjobd = d.yrjobd
  and d.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE')


select * from rydedata.pymast where ymco# = 'RY3'