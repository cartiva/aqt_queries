Select * 
from RYDEDATA.GLPCUST
where trim(gcvnd#) = '3ACCE001'

select *
from rydedata.glptrns
where trim(gtctl#) = '3ACCE001'
order by gtdate desc


select distinct trim(gcvnd#) as gcvnd#
from rydedata.glpcust


select *
from rydedata.glptrns
where trim(gtctl#) = '3ACCE001'
  and year(gtdate) = 2011
order by gtdate desc


select distinct trim(gcvnd#) as gcvnd#, t.gtctl#
-- select *
from rydedata.glpcust c
left join (
  select *
  from rydedata.glptrns
  where year(gtdate) = 2011) t on trim(c.gcvnd#) = trim(t.gtctl#)
where t.gtctl# is null

-- 2011 only  601
select count(*) from (
select gcco#, trim(gcvnd#) as gcvnd#, gcsnam
from rydedata.glpcust
where trim(gcvnd#) in (
    select distinct trim(gcvnd#)
    from rydedata.glpcust c
    left join (
      select *
      from rydedata.glptrns
      where year(gtdate) = 2011) t on trim(c.gcvnd#) = trim(t.gtctl#)
    where t.gtctl# is null)
  and trim(gcvnd#) <> '') x


-- 2011 & 12 532
select gcco#, trim(gcvnd#) as gcvnd#, gcsnam
from rydedata.glpcust
where trim(gcvnd#) in (
    select distinct trim(gcvnd#)
    from rydedata.glpcust c
    left join (
      select *
      from rydedata.glptrns
      where year(gtdate) in (2011, 2012)) t on trim(c.gcvnd#) = trim(t.gtctl#)
    where t.gtctl# is null)
  and trim(gcvnd#) <> ''