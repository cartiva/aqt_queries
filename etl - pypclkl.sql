Select * from RYDEDATA.PYPCLOCKIN
where trim(yiemp#) in ('132650','190600')
order by yiclkind desc


Select * from RYDEDATA.PYPCLKL
where trim(ylemp#) = '132650'
order by ylclkind desc

select ylclkind, ylchgd, day(ylchgd)-day(ylclkind)
from rydedata.pypclkl
where year(ylclkind) = 2012
  and month(ylclkind) = month(ylchgd)
order by day(ylchgd)-day(ylclkind) desc

select a.*, day(ylchgd)-day(ylclkind)
from rydedata.pypclkl a
where year(ylclkind) = 2012
--  and month(ylclkind) = month(ylchgd)
--  and day(ylchgd)-day(ylclkind) > 10
order by ylchgd desc

select a.*, day(ylchgd)-day(ylclkind)
from rydedata.pypclkl a
where year(ylclkind) = 2012
  and month(ylclkind) in (7,8)
  and month(ylchgd) = 8
order by ylchgd desc

select count(*)
from rydedata.pypclkl a

select *
from rydedata.sdpxtim
order by ptdate desc

select *
from rydedata.sdpxtim
where trim(ptro#) = '16091110'

select *
from rydedata.sdptech
where trim(sttech) = '646'

Select a.*, b.ymname
from RYDEDATA.pypclkl a
left join rydedata.pymast b on trim(a.ylemp#) = trim(b.ymempn)
order by ylchgd desc 


select count(*) from from RYDEDATA.pypclkl

-- and on to the actual ETL

select ylcco#, ylemp#, ylclkind, 
  case when ylclkint = '24:00:00' then '23:59:59' else ylclkint end as ylclkint,
  ylclkoutd, 
  case when ylclkoutt = '24:00:00' then '23:59:59' else ylclkoutt end as ylclkoutt,
  yluser, ylwsid, ylchgc, ylchgd, ylchgt
from rydedata.pypclkl
--where trim(ylemp#) = '96580'