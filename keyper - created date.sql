Select count(*) --23516
from dbo.asset

Select count(*) -- 4644
from dbo.asset where asset_type = 'Unregistered'

-- delphi bombs on this one: 2011-07-11 not a valid date
select upper(ltrim(rtrim(name))), created_date, convert(date, created_date)
from dbo.asset
where asset_type = 'Registered'
order by created_date desc 

select upper(ltrim(rtrim(name))), created_date, convert(nvarchar(MAX), created_date, 101)
from dbo.asset
where asset_type = 'Registered'
order by created_date desc 

select upper(ltrim(rtrim(name))), created_date, convert(nvarchar(MAX), created_date, 101)
  convert(nvarchar(30), convert(date, created_date), 107)
from dbo.asset
where asset_type = 'Registered'
order by created_date desc 


SELECT 
   created_date,
   convert(date, created_date, 101),
   CONVERT(nvarchar(10), convert(date, created_date), 101),
   cast(created_date as date),
   convert(date, cast(created_date as date), 103)
from dbo.asset   


select left(upper(ltrim(rtrim(a.name))), 9) as name, 
  convert(nvarchar(MAX), created_date, 101) as created_date
from dbo.asset a
where
 
/*
allright, i fucking give up, no way to reformat sql server date output, if it is a date it is going to be yyyy-mm-dd
so extracting becomes a 2 step process
  ext into tmp: convert to varchar and format the string appropriately
  then insert into ext_keyper_assets casting the string as a date
*/

what about dups
-- holy shit, beacoup
select count(*) from (
select name
from dbo.asset
where asset_type = 'Registered'
group by name having count(*) > 1
) x


select name, created_date
-- select count(*) -- 1452
-- select max(created_date)
from dbo.asset
where name in (
  select name
  from dbo.asset
  where asset_type = 'Registered'
  group by name having count(*) > 1)
order by name  
order by created_date desc

select *
from dbo.asset
where name in (
  select name
  from dbo.asset
  where asset_type = 'Registered'
  group by name having count(*) > 1)
and convert(date, created_date) > '01/01/2015'
order by name


select *
from dbo.asset
where ltrim(rtrim(name)) = '24280'

/*
5/17/15
thinking of doing a full scrape, sort out dups or other anomalies on the scraped data as needed
into ext_keyper_assegs
*/

select *
from dbo.asset
where convert(date, created_date) > '01/01/2015'


-- sql server datetime formats
DECLARE @now datetime
SET @now = GETDATE()
select convert(nvarchar(MAX), @now, 0) as output, 0 as style 
union select convert(nvarchar(MAX), @now, 1), 1
union select convert(nvarchar(MAX), @now, 2), 2
union select convert(nvarchar(MAX), @now, 3), 3
union select convert(nvarchar(MAX), @now, 4), 4
union select convert(nvarchar(MAX), @now, 5), 5
union select convert(nvarchar(MAX), @now, 6), 6
union select convert(nvarchar(MAX), @now, 7), 7
union select convert(nvarchar(MAX), @now, 8), 8
union select convert(nvarchar(MAX), @now, 9), 9
union select convert(nvarchar(MAX), @now, 10), 10
union select convert(nvarchar(MAX), @now, 11), 11
union select convert(nvarchar(MAX), @now, 12), 12
union select convert(nvarchar(MAX), @now, 13), 13
union select convert(nvarchar(MAX), @now, 14), 14
--15 to 19 not valid
union select convert(nvarchar(MAX), @now, 20), 20
union select convert(nvarchar(MAX), @now, 21), 21
union select convert(nvarchar(MAX), @now, 22), 22
union select convert(nvarchar(MAX), @now, 23), 23
union select convert(nvarchar(MAX), @now, 24), 24
union select convert(nvarchar(MAX), @now, 25), 25
--26 not valid
union select convert(nvarchar(MAX), @now, 100), 100
union select convert(nvarchar(MAX), @now, 101), 101
union select convert(nvarchar(MAX), @now, 102), 102
union select convert(nvarchar(MAX), @now, 103), 103
union select convert(nvarchar(MAX), @now, 104), 104
union select convert(nvarchar(MAX), @now, 105), 105
union select convert(nvarchar(MAX), @now, 106), 106
union select convert(nvarchar(MAX), @now, 107), 107
union select convert(nvarchar(MAX), @now, 108), 108
union select convert(nvarchar(MAX), @now, 109), 109
union select convert(nvarchar(MAX), @now, 110), 110
union select convert(nvarchar(MAX), @now, 111), 111
union select convert(nvarchar(MAX), @now, 112), 112
union select convert(nvarchar(MAX), @now, 113), 113
union select convert(nvarchar(MAX), @now, 114), 114
union select convert(nvarchar(MAX), @now, 120), 120
union select convert(nvarchar(MAX), @now, 121), 121
--122 to 125 not valid
union select convert(nvarchar(MAX), @now, 126), 126
union select convert(nvarchar(MAX), @now, 127), 127
--128, 129 not valid
union select convert(nvarchar(MAX), @now, 130), 130
union select convert(nvarchar(MAX), @now, 131), 131
--132 not valid
order BY style


