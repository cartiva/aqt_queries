select a.yrco# as storecode, trim(a.yrempn) as employeenumber, 
  trim(a.yrjobd) as jobcode, b.yrtext as description
from rydedata.pyprhead a
left join rydedata.pyprjobd b on trim(a.yrjobd) = trim(b.yrjobd)
-- where a.yrco# = 'RY1'

select max(length(trim(yrjobd))) from rydedata.pyprhead -- 10
select max(length(trim(yrtext))) from rydedata.pyprjobd -- 44


select *
from rydedata.pyprjobd

select * 
from rydedata.pyprhead

select count(*)
from rydedata.pyprhead


from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions, eliminate dups



select distinct pym.ymname as "Name", pym.ymdept as "Department", jobd.yrtext as "Job Title",  pym.ymsex as "Gender"
from pymast pym
left join pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn) and phd.yrco# = 'RY1' -- job description (code)
left join pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) and jobd.yrco# = 'RY1' -- job description in plain english
where pym.ymco# = 'RY1'
and pym.ymactv <> 'T'
order by pym.ymname


select ymname, 
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  p.ymdept,
  j.yrtext as Occupation
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join (
  select distinct yrjobd, yrtext
  from rydedata.pyprjobd) j on j.yrjobd = ph.yrjobd 
    and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST'
and ymco# = 'RY1'
order by ymdept, ymname


-- use qsys2 to get the column_heading
select *
from qsys2.syscolumns
where trim(table_schema) = 'RYDEDATA'
  and trim(table_name) = 'INPMAST'

select count(distinct table_name)
from qsys2.syscolumns
where table_schema = 'RYDEDATA'

select *
from qsys2.syscolumns
where trim(table_schema) = 'RYDEDATA'
  and trim(table_name) = 'INPMAST'

select *
from sysibm.columns
where table_schema = 'RYDEDATA'
  and table_name = ''


select *
from rydedata.pyprjobd



-- 12/21/15
Good Morning Jon   could you create a report for me  I would like   ( store,  name,  department,  job description,  distribution code ) 



select distinct pym.ymname as "Name", pym.ymdept as "Department", jobd.yrtext as "Job Title",  pym.ymsex as "Gender"
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn) and phd.yrco# = 'RY1' -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) and jobd.yrco# = 'RY1' -- job description in plain english
where pym.ymco# = 'RY1'
and pym.ymactv <> 'T'
order by pym.ymname

select * from rydedata.pymast where active_code <> 'T'
selct 

select pymast_company_number, ymname, department_code, j.yrtext as "Job Title", distrib_code
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn and p.pymast_company_number = ph.yrco#
left join (
  select distinct yrco#, yrjobd, yrtext
  from rydedata.pyprjobd) j on j.yrjobd = ph.yrjobd 
    and ph.yrco# = j.yrco#
    and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions
where ymactv <> 'T'
and trim(p.ymname) <> 'TEST'
and ymco# in ('RY1','RY2')
order by ymdept, ymname



