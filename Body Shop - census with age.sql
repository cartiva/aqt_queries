Select distinct ymname as Name, 
  year(now()) - year(b.birthdate) -
    case 
      when
        month(b.BirthDate)*100 + day(b.BirthDate) > month(CurDate())*100 + day(CurDate()) then 1
      else 0
    end as Age,
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
    j.yrtext as Occupation
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions, eliminate dups
-- where trim(ymco#) = 'RY1'
left join ( -- generate birth date
  select ymempn, 
    case 
      when cast(right(trim(ymbdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymbdte))
            when 5 then  '20'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '20'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '19'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      end as BirthDate
  from rydedata.pymast
  where ymactv <> 'T'
  and length(trim(ymname)) > 4) b on p.ymempn = b.ymempn
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST'
  and ymco# = 'RY1'
  and ymdept = '05'
