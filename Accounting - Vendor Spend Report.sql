Select /*trns.gtco# as Company,*/ trns.gtacct as Account, mast.gmdesc as Description, 
  cust.gcsnam as VendorName, name.bnadr1 as Address1, name.bnadr2 as Address2, 
  name.bncity as City, name.bnstcd as State, name.bnzip as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by trns.gtco#, trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd, name.bnzip 
having sum(trns.gttamt) <> 0
order by trns.gtacct
-- order by gmdesc
--order by cust.gcsnam

-- 1/20/14
take zip out of the grouping
-- wait a minute, do not need account info, just vendornumber & name
Select /*trns.gtco# as Company,*/ trns.gtacct as Account, mast.gmdesc as Description, cust.gcsnam as VendorName, 
  name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, 
  max(name.bnzip) as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by /*trns.gtco#,*/ trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd
having sum(trns.gttamt) <> 0
--order by trns.gtacct
order by cust.gcsnam


-- wait a minute, do not need account info, just vendornumber & name
Select /*trns.gtco# as Company, trns.gtacct as Account, mast.gmdesc as Description,*/ 
  trim(trns.gtvnd#) as VendorNumber, cust.gcsnam as VendorName, 
  name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, 
  max(name.bnzip) as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by /*trns.gtco#,*/ trim(trns.gtvnd#), cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd
having sum(trns.gttamt) <> 0
--order by trns.gtacct
order by cust.gcsnam

-- don't know, use glpcust as the main table
select trim(a.gcvnd#) as VendorNumber, a.gcsnam as VendorName, 
  c.bnadr1 as Address1, c.bnadr2 as Address2, c.bncity as City, c.bnstcd as State, 
  max(c.bnzip) as Zip
from rydedata.glpcust a
inner join rydedata.glptrns b on trim(a.gcvnd#) = trim(b.gtvnd#)
left join rydedata.bopname c on a.gckey = c.bnkey and a.gcco# = c.bnco
where b.gtdate between '2013-01-01' and '2013-12-31'
group by trim(a.gcvnd#), a.gcsnam,
  c.bnadr1, c.bnadr2, c.bncity, c.bnstcd


select *
from rydedata.glptrns
where gtdate between '2013-12-01' and '2013-12-31'
  and trim(gtacct) = '120300'
and trim(gtctl#) <> trim(gtvnd#)

select *
from rydedata.glpcust
where trim(gcvnd#) = '114399'

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '14704'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct

select * from rydedata.glpcust where trim(gcvnd#) = '14704'

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '117326'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '1115'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct


select *
from rydedata.glptrns
where trim(gtvnd#) = '14704'
  and gtdate between '01/01/2013' and '12/31/2013'


select trim(gtvnd#) as gtvnd#, sum(gttamt)
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#
order by trim(gtvnd#) 

select count(*) from (
select trim(gtvnd#) as gtvnd#, sum(gttamt)
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#
) x

select * from (
select trim(gtvnd#) as gtvnd#, sum(gttamt) as gttamt
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#) x where abs(gttamt) > 599
order by trim(gtvnd#) 


select a.*, b.gcsnam, b.gcftid, b.gckey, 
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
  c.*
from (
  select trim(gtvnd#) as gtvnd#, sum(gttamt)
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 

-- 1/21/14
was concerned abt the large result set, jeri says ok,
she would like the stores (acct) separated

select 
  case a.gtacct
    when '120300' then 'RY1'
    when '220200' then 'RY2'
    when '320200' then 'RY3'
  end as Store,
a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, abs(a.gttamt) as Amount,
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, trim(gtacct) as gtacct, sum(gttamt) as gttamt
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#, gtacct) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 

-- 1 row per vendor
select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount", 
  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount,
  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


-- 7/14/14 june needs the same thing for 2014 ytd

-- 1 row per vendor
select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2014' and curdate()
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


/*
1/20/2015
Good Afternoon Jonny!

I need you to run the vendor spend report again for me through December 2014. Please� and Thank You! You�re the best!
*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2014' and '12/31/2014'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


select corporation_1099_, count(*) from rydedata.glpcust group by corporation_1099_


/* 12/7/15
Good Afternoon! 

Approaching the end of the year and I need a favor�..
The last couple of years we have had you run a vendor spend report� 
I need an updated one of those � right now mostly just looking for tax id�s for the vendors we are using, 
not as interested at this point in the amount of money spent yet this year. Could ya make this happen for us?

*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  
  b.gcftid as TaxID --, 
  --coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
  --case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
  --c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200')
    and gtdate between '01/01/2015' and '12/31/2015'
    and gttamt < 0
    and gtvnd# <> ''
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null


/*
1/13/16
In the past you have prepared the vendor spend report for both stores for me� can you do this again for me, with totals paid to each vendor as of 12/31/2015 � pretty please? Let me know if you have any questions! Thanks jonny!
*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2015' and '12/31/2015'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


/*
1-11-17
Good afternoon Jon!

I was hoping you could pull together the vendor spend report for me, as you have in previous years? 
This time I would like to add "vendor type" as a field. We would like to have this report reviewed and 
forwarded to Brady's within a week. Please send to this email as I don't have my Rydell email handy. 

Sorry I didn't mean vendor type I meant "1099 vendor" field
*/

DECLARE GLOBAL TEMPORARY TABLE  SESSION.TEMP_1099_TYPES (
  code  CHAR(2),
  type char(26)); 

insert into session.temp_1099_types(code, type)
select * from table(
  values
    ('1','Rent'),
    ('2','Royalties')  
    ('3','Other Income'),
    ('6','Medical & Health Care'),
    ('7','Nonemployee Compensation'),
    ('8','Interest Income'),
    ('S','Sole Proprietor'),
    ('14','Attorney'),
    ('','Not 1099 Vendor')) x  

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip, d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2016' and '12/31/2016'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
left join SESSION.TEMP_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 

