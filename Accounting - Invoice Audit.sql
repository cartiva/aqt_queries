/*  GLPTRNS */                        
FIELD    Field Def      VALUE      Generated From                
GTDTYP   Doc Type        B          Deals                
GTDTYP   Doc Type        C          Checks                
GTDTYP   Doc Type        D          Deposits                
GTDTYP   Doc Type        I          Inventory Purchase/Stock In            
GTDTYP   Doc Type        J          Conversion or GJE                
GTDTYP   Doc Type        O          Invoice/PO Entry                
GTDTYP   Doc Type        P          Parts Ticket                
GTDTYP   Doc Type        R          Cash Receipts                
GTDTYP   Doc Type        S          Service Tickets                
GTDTYP   Doc Type        W          Handwrittn Checks                
GTDTYP   Doc Type        X          Bank Rec (Fee, Service Charges, etc.)          
GTPOST   Post Status                In Process of Writing to GLPTRNS and GLPMAST (Most Likely)  
GTPOST   Post Status     V          Voided Entry                
GTPOST   Post Status     Y          Posted Entry                   
GTRDTYP  Recon Doc Type             N/A  (It is possible for a document to reconcile with out a Reconciling Doc Type
GTRDTYP  Recon Doc Type  B          Same as Codes for Document Type              
GTRDTYP  Recon Doc Type  C           ""             
GTRDTYP  Recon Doc Type  D           ""            
GTRDTYP  Recon Doc Type  I           ""                
GTRDTYP  Recon Doc Type  J           ""                   
GTRDTYP  Recon Doc Type  O           ""                  
GTRDTYP  Recon Doc Type  P           ""                  
GTRDTYP  Recon Doc Type  R           ""                  
GTRDTYP  Recon Doc Type  S           ""                   
GTRDTYP  Recon Doc Type  X           ""                 
GTTYPE   Record Type                Other*   *Any account that does not require a special reconciliation program.             
GTTYPE   Record Type     $          Entry to a Bank Account              
GTTYPE   Record Type     C          Entry from a Cash Drawer              
GTTYPE   Record Type     M          Other*                
GTTYPE   Record Type     P          Entry to/from Payables              
GTTYPE   Record Type     R          Entry to/from Receiveables    

select *
from rydedata.glptrns
where gtdate between curdate() - 90 DAY and curdate()
  and trim(gtvnd#) <> ''
  and trim(gtjrnl) not in ('PAA','PAY')
  and gtpost <> 'V'
  and gtdtyp in ('O','C','J')

/*
Select trns.gtco# as Company, trns.gtacct as Account, mast.gmdesc as Description, cust.gcsnam as VendorName, name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, name.bnzip as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2009-11-01' and '2010-10-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by trns.gtco#, trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd, name.bnzip 
having sum(trns.gttamt) <> 0
order by cust.gcsnam
*/

select distinct Company, gtdate as Date, trim(gtjrnl) as Jrnl, trim(gtacct) as Acct, trim(gtdoc#) as Doc#,
  trim(gtref#) as Ref#, trim(gtdesc) as Descr, trim(gtvnd#) as "Vend #", VendorName, Amount 
from (
Select mast.gmtype, trns.gtco# as Company, trns.gtdate, gtjrnl, gtacct, gtdoc#, gtref#, gtdesc, gtvnd#, 
  cust.gcsnam as VendorName,  trns.gttamt as Amount
from rydedata.glptrns trns
left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(trns.gtvnd#) <> ''
  and trns.gtdate between curdate() - 91 DAY and curdate()
  and trim(trns.gtjrnl) not in ('PAA','PAY')
  and trns.gtpost <> 'V'
  and trns.gtdtyp in ('O','C','J')) x
where gmtype = '8' -- expense account
  and vendorname is not null



select *
from (

select distinct Company, gtdate as Date, trim(gtjrnl) as Jrnl, trim(gtacct) as Acct, trim(gtdoc#) as Doc#,
  trim(gtref#) as Ref#, trim(gtdesc) as Descr, trim(gtvnd#) as "Vend #", VendorName, Amount 
from (
Select mast.gmtype, trns.gtco# as Company, trns.gtdate, gtjrnl, gtacct, gtdoc#, gtref#, gtdesc, gtvnd#, 
  cust.gcsnam as VendorName,  trns.gttamt as Amount
from rydedata.glptrns trns
left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(trns.gtvnd#) <> ''
  and trns.gtdate between curdate() - 91 DAY and curdate()
  and trim(trns.gtjrnl) not in ('PAA','PAY')
  and trns.gtpost <> 'V'
  and trns.gtdtyp in ('O','C','J')) x
where gmtype = '8' -- expense account
  and vendorname is not null) a
left join (
select Doc#, sum(Amount) 
from (
select distinct Company, gtdate as Date, trim(gtjrnl) as Jrnl, trim(gtacct) as Acct, trim(gtdoc#) as Doc#,
  trim(gtref#) as Ref#, trim(gtdesc) as Descr, trim(gtvnd#) as "Vend #", VendorName, Amount 
from (
Select mast.gmtype, trns.gtco# as Company, trns.gtdate, gtjrnl, gtacct, gtdoc#, gtref#, gtdesc, gtvnd#, 
  cust.gcsnam as VendorName,  trns.gttamt as Amount
from rydedata.glptrns trns
left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(trns.gtvnd#) <> ''
  and trns.gtdate between curdate() - 91 DAY and curdate()
  and trim(trns.gtjrnl) not in ('PAA','PAY')
  and trns.gtpost <> 'V'
  and trns.gtdtyp in ('O','C','J')) x
where gmtype = '8' -- expense account
  and vendorname is not null ) x
group by Doc#) b on a.doc# = b.doc#

/*  5/4 **/
-- jeri concerned not enough entries to jrnl POT
-- for example 225125034

select * from rydedata.glptrns where gtdoc# = '225125034'
-- looks good, offsetting amounts but to an exp acct and a liab acct
-- so the query should have eliminated the Liab acct
-- wtf i removed gmtype = 8 and it returns a single row???
select distinct Company, gtdate as Date, trim(gtjrnl) as Jrnl, trim(gtacct) as Acct, trim(gtdoc#) as Doc#,
  trim(gtref#) as Ref#, trim(gtdesc) as Descr, trim(gtvnd#) as "Vend #", VendorName, Amount 
from (
Select mast.gmtype, trns.gtco# as Company, trns.gtdate, gtjrnl, gtacct, gtdoc#, gtref#, gtdesc, gtvnd#, 
  cust.gcsnam as VendorName,  trns.gttamt as Amount
from rydedata.glptrns trns
left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(trns.gtvnd#) <> ''
  and trns.gtdate between curdate() - 91 DAY and curdate()
  and trim(trns.gtjrnl) not in ('PAA','PAY')
  and trns.gtpost <> 'V'
  and trns.gtdtyp in ('O','C','J')) x
where trim(gtdoc#) = '225125034'
and gmtype = '8'

-- i am leary of the gtdtyp filter, i don't really know what it means
select *
from rydedata.glpmast
where trim(gmacct) in ('130000', '16903')
and gmco# = 'RY1' and gmyear = 2012


select * from rydedata.glptrns where gtdoc# = '225125034'
  and gmtype = '8' -- expense account
  and vendorname is not null

-- this looks sold for a start
-- 1 row per acct in glpmast
select * 
from rydedata.glptrns t
inner join (
  select *
  from rydedata.glpmast
--where trim(gmacct) in ('130000', '16903')
  where gmco# = 'RY1' 
  and gmyear = 2012
  and gmactive = 'Y'
  and gmtype = '8') g on trim(t.gtacct) = trim(g.gmacct)
where gtdoc# = '225125034'

-- uh oh, gtvnd# is null
select * 
from rydedata.glptrns t
inner join (
  select *
  from rydedata.glpmast
--where trim(gmacct) in ('130000', '16903')
  where gmco# = 'RY1' 
  and gmyear = 2012
  and gmactive = 'Y'
  and gmtype = '8') g on trim(t.gtacct) = trim(g.gmacct)
where gtdoc# = '225125034'
  --and trim(t.gtvnd#) <> ''
  and t.gtdate between curdate() - 91 DAY and curdate()
  and trim(t.gtjrnl) not in ('PAA','PAY')
  and t.gtpost <> 'V'

select *
from rydedata.glptrns
where gtco# = 'RY1'
  and gtdtyp = 'O'
  and trim(gtjrnl) = 'POT'
  and gtdate between curdate() - 91 DAY and curdate()


select distinct Company, gtdate as Date, trim(gtjrnl) as Jrnl, trim(gtacct) as Acct, trim(gtdoc#) as Doc#,
  trim(gtref#) as Ref#, trim(gtdesc) as Descr, trim(gtvnd#) as "Vend #", VendorName, Amount 
from (
Select mast.gmtype, trns.gtco# as Company, trns.gtdate, gtjrnl, gtacct, gtdoc#, gtref#, gtdesc, gtvnd#, 
  cust.gcsnam as VendorName,  trns.gttamt as Amount
from rydedata.glptrns trns
left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(trns.gtvnd#) <> ''
  and trns.gtdate between curdate() - 91 DAY and curdate()
  and trim(trns.gtjrnl) not in ('PAA','PAY')
  and trns.gtpost <> 'V'
  and trns.gtdtyp in ('O','C','J')) x
where gmtype = '8' -- expense account
  and vendorname is not null


select t.gtco# as Company, gtdate as Date, trim(gtjrnl) as Jrnl,
  trim(gtacct) as Acct, trim(gtdoc#) as Doc#, trim(gtref#) as Ref#,
  trim(gtdesc) as Descr, trim(gtvnd#) as Vend#, cust.gcsnam,
  t.gttamt as Amount 
from rydedata.glptrns t
inner join (
  select *
  from rydedata.glpmast
  where gmco# = 'RY1' 
    and gmyear = 2012
    and gmactive = 'Y'
    and gmtype = '8') g on trim(t.gtacct) = trim(g.gmacct)
left join rydedata.glpcust cust on t.gtco# = cust.gcco# and t.gtvnd# = cust.gcvnd#
left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
where t.gtdate between curdate() - 91 DAY and curdate()
  and trim(t.gtjrnl) not in ('PAA','PAY')
  and t.gtpost <> 'V'
  and t.gtdtyp in ('O','C','J')


select count(*) from ( -- 39936, 17828
select t.gtco# as Company, gtdate as Date, trim(gtjrnl) as Jrnl,
  trim(gtacct) as Acct, trim(gtdoc#) as Doc#, trim(gtref#) as Ref#,
  trim(gtdesc) as Descr, trim(gtvnd#) as Vend#, t.gttamt as Amount 
from rydedata.glptrns t
inner join (
    select *
    from rydedata.glpmast
  --where trim(gmacct) in ('130000', '16903')
    where gmco# = 'RY1' 
    and gmyear = 2012
    and gmactive = 'Y'
    and gmtype = '8') g on trim(t.gtacct) = trim(g.gmacct)
--where gtdoc# = '225125034'
  --and trim(t.gtvnd#) <> ''
where t.gtdate between curdate() - 91 DAY and curdate()
  and trim(t.gtjrnl) not in ('PAA','PAY')
  and t.gtpost <> 'V'
  and t.gtdtyp in ('O','C','J')) x
