select *
from rydedata.sdprdet
where trim(ptro#) = '16139150'
  and ptline = 3

select *
from rydedata.sdprdet
where trim(ptro#) = '16139305'
  and ptline = 1

select * from rydedata.sdpxtim where trim(ptro#) in ( '16139150', '16139305') order by ptro#, ptline

select ptdate, ptro#, ptline, pttech, sum(ptlhrs) from rydedata.sdpxtim where trim(ptro#) in ( '16139150', '16139305') group by ptdate, ptro#, pttech, ptline order by ptro#, ptline


  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20121231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date), 
    ptco#, ptro#, ptline, pttech
order by ro


select a.*, b.ptseq#, b.ptlhrs, b.ptdbas
from rydedata.sdpxtim a
left join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  and a.ptline = b.ptline
  and b.ptltyp = 'L'
  and b.ptcode = 'TT'
where a.ptdate > 20131000
  and a.ptco# = 'RY1'
  and left(trim(a.ptro#), 2) = '16'
--  and trim(a.ptro#) = '16139150'
  and b.ptro# is not null 
order by a.ptro#, a.ptline

select * from rydedata.sdprdet a where trim(a.ptro#) = '16137702' and ptline = 5
select * from rydedata.sdprdet a where trim(a.ptro#) = '16139150' and ptline = 3

select * from rydedata.sdpxtim a where trim(a.ptro#) = '16139150'

select ptdbas, count(*) from rydedata.sdprdet group by ptdbas
select ptpadj, ptdbas, count(*) from rydedata.sdprdet group by ptpadj,ptdbas

select *
from rydedata.sdprdet
where ptpadj <> ''
  and ptdate > 20130000
  and ptlhrs <> 0
  and ptcode = 'TT'

select *
from rydedata.pdppdet
where ptsvctyp = 'OT'


select *
from rydedata.sdprdet
where ptsvctyp = 'OT'

select * from rydedata.sdpxtim order by ptdate desc 



SELECT b.*
FROM rydedata.PDPPHDR a 
INNER JOIN rydedata.PDPPDET b on a.ptpkey = b.ptpkey
WHERE trim(ptdoc#) = '16139551'
  AND ptcode = 'TT'


select *
from rydedata.sdprdet
where trim(ptro#) = '16130131'

select * from rydedata.glptrns where trim(gtdoc#) = '16130131'

select *
from rydedata.sdprdet
where ptdate > 20130000 
  and ptlhrs <> 0
  and ptlamt = 0

SELECT a.ptdoc#, b.*
FROM rydedata.PDPPHDR a 
INNER JOIN rydedata.PDPPDET b on a.ptpkey = b.ptpkey
where b.ptsvctyp = 'OT'

select *
from rydedata.sdprdet
where trim(ptro#) = '16135397'



select * from rydedata.sdpxtim where trim(ro_number) = '16140592' order by service_line_no

select * from rydedata.sdprdet where trim(ptro#) = '16140592' and ptline = 1

select b.* from rydedata.pdpphdr a
inner join rydedata.pdppdet b on a.ptpkey = b.ptpkey where trim(ptdoc#) = '16140592' and b.ptline = 1

select * from rydedata.sdpxtim where trim(technician_id) = '522'

select ro_number, service_line_no, sum(labor_Hours) as flaghours 
from rydedata.sdpxtim 
where trim(technician_id) = '522'
group by ro_number, service_line_no


select * from (
select ro_number, service_line_no, technician_id, sum(labor_Hours) as flaghours 
from rydedata.sdpxtim 
--where trim(technician_id) = '522'
group by ro_number, service_line_no, technician_id
) x where flaghours <> 0

select a.*
from rydedata.sdpxtim a
inner join (
  select * from (
  select ro_number, service_line_no, technician_id, sum(labor_Hours) as flaghours 
  from rydedata.sdpxtim 
  --where trim(technician_id) = '522'
  group by ro_number, service_line_no, technician_id
  ) x where flaghours <> 0) y on a.ro_number = y.ro_number and a.service_line_no = y.service_line_no


select * from rydedata.sdpxtim where trim(ro_number) = '16117974' order by ptline


select a.*
from rydedata.sdpxtim a
inner join (
  select * from (
  select ro_number, service_line_no, technician_id, sum(labor_Hours) as flaghours 
  from rydedata.sdpxtim 
  --where trim(technician_id) = '522'
  group by ro_number, service_line_no, technician_id
  ) x where flaghours <> 0) y on a.ro_number = y.ro_number and a.service_line_no = y.service_line_no


seems like, at least on those that balance to 0 hours, the positive amount is the time originally booked,
the negative is the removal of that time

-- 1-17-14
16142407: a dummy ro i opened on 1/16

select * from rydedata.sdpxtim where trim(ro_number) = '16142407' order by service_line_no

select * from rydedata.sdprdet where trim(ptro#) = '16142407' and ptline = 1

select b.ptline, b.ptlsts, b.ptltyp, b.ptseq#, b.ptcode, b.ptdate, b.ptcmnt, b.pttech, b.ptpadj, b.ptlhrs
from rydedata.pdpphdr a
inner join rydedata.pdppdet b on a.ptpkey = b.ptpkey where trim(ptdoc#) = '16142407' and b.ptline = 1

-- 5/19/15
interesting situation today.  Bev was gone friday thru monday.  kim is leaving early this week, so
payroll needded to be in to her by 10 AM on Tuesday morning.
Bev shows up tuesday and says she has to make adjustments: team leaders get a portion of the flag hours
of hourly techs that they are mentoring,  so she added that time this morning.
This seriously impacts payroll, i can not wait until tomorrow (at which time, overnight processing
will have included the adjustments bev made today_), so i have to figure out how to get the
adjustments made this morning into the payroll for today.
Bev said that she would be putting the hours on shop time ros for the pay period, they are already
closed so she will add the time via tech time adjustment, she will make the flag date 5/15/15

bear tech 574, 11.5 hours on ro 16193159
waldbauer tech 608, 15.3 hours on ro 16193166
anderson tech 640, 11.9 hours on rol 16193164

After she made her changes, i ran a Tech Time Adjustment Report for 5/15/15:


 SD3021R                                           Rydell Auto Center Inc.                             5/19/15   8:31:27
 JANDR561                                        Technician Time Adjustments                                    Page   1
 From  5/15/15 to  5/15/15                           Flag Time Adjustments
 ========================================================================================================================
                  -- Repair --   --------------- Tech Hours --------------                    ------- Created by -------
 Tech  Flag Date  Order   Line   Cust Pay  Warranty  Internal  Srv-Contrct       Cost         User        Date      Time
 ========================================================================================================================
 574   5/15/15   16193159    1      11.50                                        1.00         RYDEBEVERL  5/19/15   8:05
 ========================================================================================================================
 Totals:                            11.50                                        1.00


 590   5/15/15   16193955    4                            .50                    9.00         RYDEBEVERL  5/16/15  16:07
 590   5/15/15   16193988    4                            .50                    9.00         RYDEBEVERL  5/16/15  16:08
 590   5/15/15   16194064    4                            .50                    9.00         RYDEBEVERL  5/16/15  16:09
 ========================================================================================================================
 Totals:                                                 1.50                   27.00


 608   5/15/15   16193166    2      15.30                                        1.00         RYDEBEVERL  5/19/15   8:05
 ========================================================================================================================
 Totals:                            15.30                                        1.00


 627   5/15/15   16193990    4                            .50                    7.44         RYDEBEVERL  5/16/15  15:57
 627   5/15/15   16194049    4                            .50                    7.44         RYDEBEVERL  5/16/15  15:57
 627   5/15/15   16194069    4                            .50                    7.44         RYDEBEVERL  5/16/15  15:58
 627   5/15/15   16194079    4                            .50                    7.44         RYDEBEVERL  5/16/15  15:58
 ========================================================================================================================
 Totals:                                                 2.00                   29.76


 636   5/15/15   16193860    4                            .50                    7.25         RYDEBEVERL  5/16/15  16:02
 636   5/15/15   16194036    4                            .50                    7.25         RYDEBEVERL  5/16/15  16:03
 636   5/15/15   16194077    4                            .50                    7.25         RYDEBEVERL  5/16/15  16:04
 636   5/15/15   16194095    4                            .50                    7.25         RYDEBEVERL  5/16/15  16:04
 ========================================================================================================================
 Totals:                                                 2.00                   29.00


 640   5/15/15   16193164    2      11.90                                        1.00         RYDEBEVERL  5/19/15   8:06
 ========================================================================================================================
 Totals:                            11.90                                        1.00



 Grand Total:                                                                   88.76
 
 Which is all well and good, but where is all this fucking information stored:
 
 not in SDPXTIM
 Select * from RYDEDATA.SDPXTIM where trim(ro_number) in ('16193159','16193955','16193988','16194064','16193166','16193164')
 

but they do fucking show up in SDPRDET with patpadj = X and ptdbas = V
but they DO NOT show on the 5250 display of the RO
select *
from rydedata.sdprdet
where trim(ptro#) in ('16193159','16193955','16193988','16194064','16193166','16193164')
--  and ptlhrs <> 0
order by ptro#, ptline, ptseq#

Now, that is all well and good, found the hours assigned to the tech with the correct flag date, but how the
fuck does the tech time adj report know that these were added by bev on 5/19??
where is that information stored?

 
-- 11/13/16 --------------------------------------------------------------------------------------------------------------------------------
realized i have been fucking up the processing of bevs tech time adjustments
each pay period, team leaders are gifted with hours to compensate for their efforts in training
young hourly techs

these hours are entered through tech time adjustments, and invariably are assigned to shop time ros, often on the same
line on which the team leader has actual shop time
i have been proccessing (categorizing) those hours as shop time
they are not
and should not be
just like above, they do not show up in the UI

this came up because i ran a shop/training/fence report for bev/andrew.  jeri saw it and flipped out because
she runs the sales by labor operation report, which, of course, DOES NOT INCLUDE THE ADJUSTMENT HOURS

so, once again i need to take a stab at processing this shit correctly




Select * from RYDEDATA.GLPTRNS where trim(gtctl#) = '16254357'

Select * from RYDEDATA.sdprdet where trim(ptro#) = '16046015'

-- xtim
Select * from RYDEDATA.GLPTRNS where trim(gtctl#) = '16251212'


Select * from RYDEDATA.sdprdet where trim(ptro#) = '16254356'

Select * from RYDEDATA.sdprdet where trim(ptro#) = '16255425' order by ptline, ptseq#


Select * from RYDEDATA.sdprdet where trim(ptro#) in('16254357','16254356') and ptdbas = 'V' order by ptro#, ptline

Select * from RYDEDATA.sdprdet where trim(ptro#) = '16255616' and ptline between 1 and 3 order by ptline

Select * from RYDEDATA.sdprdet where trim(ptro#) = '16256205'

Select * from RYDEDATA.sdprhdr where trim(ptro#) = '16256205'

select ro_number, sum(labor_hours) 
from rydedata.sdpxtim 
where trans_date > 20161000
group by ro_number
having sum(labor_hours) <> 0

select *
from rydedata.sdprdet a
inner join (
Select trim(ptro#) as ptro#
from RYDEDATA.sdprdet 
where ptdate > 20161100
  and ptdbas = 'V') b on trim(a.ptro#) = trim(b.ptro#)


select *
from rydedata.sdprdet a
inner join (
Select trim(ptro#) as ptro#
from RYDEDATA.sdprdet 
where ptdate > 20161100
  and ptpadj = 'X') b on trim(a.ptro#) = trim(b.ptro#)
order by a.ptro#, a.ptline



select *
from rydedata.sdprdet a
inner join (
Select trim(ptro#) as ptro#
from RYDEDATA.sdprdet 
where ptdate > 20160600
  and ptdbas = 'V'
  and ptcode <> 'TT') b on trim(a.ptro#) = trim(b.ptro#)

select ptcode, ptltyp, ptdbas, ptpadj, count(*) as the_count, min(ptro#) as ro_1, max(ptro#) as ro_2, min(ptdate) as date_1,  max(ptdate) as date_2
from rydedata.sdprdet
where ptdbas <> '' or ptpadj <> ''
group by ptcode, ptltyp, ptdbas, ptpadj
order by ptpadj, ptdbas






-- 11/18/16
-- from wiki
this is why i can not find the account for the tech time adjustments
shared this information with bev, she is going to talk to jeri about getting trained on posting to GL
/*
All technician time adjustments are done for closed repair orders. If the repair order is still open, 
the tech time must be adjusted through option Technician Time Log in the Repair Order.
A technician time adjustments report can be printed using from Reports, Technician Time Adjustments.
Technician Time Adjustments can be "backdated" to correct flags for a specific pay period.
Creating a Technician Time Adjustment does NOT post a correcting entry to the G/L for the Service 
Work-in-Process (WIP) or Labor Cost of Sale accounts. A user with access to the G/L will need to 
create an Adjusting Entry on the original Repair Order. The adjusting entry needs to post to the 
original SCA, SVI, or SWA journal that the Repair Order posted to (rather than the GJE), otherwise, 
the WIP account will not clear when using the Tech Flag Time Payroll Importand the Gross Profit will 
be overstated on the DOC and Financial Statement.
*/

select * from rydedata.sdpxtim

-- what's the relationship between ptdbas/ptpadj & sdpxtim
select a.ptro#, a.ptline, a.ptcode, a.ptltyp, a.ptdbas, a.ptpadj, b.*
from rydedata.sdprdet a
inner join rydedata.sdpxtim b on trim(a.ptro#) = trim(b.ro_number)
  and a.ptline = b.service_line_no
where a.ptdbas <> '' or a.ptpadj <> ''

select ptcode, ptltyp, ptdbas, ptpadj, count(*) as the_count, min(a.ptro#) as ro_1, max(a.ptro#) as ro_2, min(a.ptdate) as date_1,  max(a.ptdate) as date_2
from rydedata.sdprdet a
inner join rydedata.sdpxtim b on trim(a.ptro#) = trim(b.ro_number)
  and a.ptline = b.service_line_no
where a.ptdbas <> '' or a.ptpadj <> ''
group by ptcode, ptltyp, ptdbas, ptpadj
order by ptpadj, ptdbas

-- join sdprdet to sdpxtim
select a.ptro#, a.ptline, a.ptcode, a.ptltyp, a.ptdbas, a.ptpadj, b.*
from rydedata.sdprdet a
inner join rydedata.sdpxtim b on trim(a.ptro#) = trim(b.ro_number)
  and a.ptline = b.service_line_no
where a.ptdbas = 'V' and a.ptpadj = 'X'

-- aha, when i group sdpxtim with non zero labor hours: no rows 
-- so, assuming that all tech time adjustments are indicated by ptdbas = V and ptpadj = X
-- this shows that tech time adjustments are independent of sdpxtim
select a.ptro#, a.ptline, a.ptcode, a.ptltyp, a.ptdbas, a.ptpadj, b.*
from rydedata.sdprdet a
inner join (
  select ro_number, service_line_no, sum(labor_hours) as labor_hours
  from rydedata.sdpxtim
  group by ro_number, service_line_no
  having sum(labor_hours) <> 0) b on trim(a.ptro#) = trim(b.ro_number)
  and a.ptline = b.service_line_no
where a.ptdbas = 'V' and a.ptpadj = 'X'

select *
from rydedata.sdpxtim
where trim(ptro#) = '16222438'


select ro_number, service_line_no, trans_date, sum(labor_hours) as labor_hours,
  (select sum(labor_hours) from rydedata.sdpxtim where trim(ro_number) = trim(a.ro_number) and ptline = a.ptline)
from rydedata.sdpxtim a
where trans_date > 20160000
group by ro_number, service_line_no, trans_date
having sum(labor_hours) <> 0
order by ro_number, service_line_no


-- would really like to start decoding the ptdbas/ptpadj combinations

-- 1. ptdbas = V ptpadj = ''

select a.*
from rydedata.sdprdet a
inner join (
  select distinct ptro#
  from rydedata.sdprdet 
  where ptdbas = 'V'
    and ptpadj = '') b on trim(a.ptro#) = trim(b.ptro#)
where a.ptdate > 20160600
  and left(trim(a.ptro#), 1) = '1'
order by a.ptro#, a.ptline


