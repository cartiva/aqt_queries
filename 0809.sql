/*

SELECT tc.table_name, tc.table_text, tc.column_name, tc.column_heading, rc.recordcount
FROM vtablescolumnsp tc
INNER JOIN systablerecordcount rc ON rc.table_name = tc.table_name
WHERE rc.recordcount <> 0
AND tc.column_heading LIKE '%KEY%'
*/

select bnco#, bnkey, bnsnam from bopname
select ccco#, cckey from bopccnt

select bnco#, bnkey, bnsnam, ccco#, cckey
from bopname b
inner join bopccnt c on c.ccco# = b.bnco# and c.cckey = b.bnkey

select bnco#, bnkey, bnsnam, ccco#, cckey
from bopname b
left join bopccnt c on c.cckey = b.bnkey
where c.cckey is not null

-- bnekey is not the same as bnkey, don't know it's use, very few values (161)
select bnkey, bnekey
from bopname
where bnekey <> '0'

-- there are no bopccnt records that do not exist in bopname
select *
from bopccnt c
where not exists (
  select 1 
  from bopname
  where bnkey = c.cckey)
  
select *
from cspchdr c
where not exists (
  select 1 
  from bopname where bnkey = c.ccnkey)
  
select *
from cspstwd c
where not exists (
  select 1
  from bopname where bnkey = c.cwnkey)
  
select *
from glparsh c
where not exists (
  select 1
  from bopname where cast(bnkey as CHAR(9)) = c.wkcus#)  
  
    
select *
from glpcrhd c
where not exists (
  select 1
  from bopname where cast(bnkey as CHAR(9)) = c.gnckey)    
  
select *
from glpcust c
where not exists (
  select 1
  from bopname where bnkey = c.gckey)
  
select b.bnsnam, g.*
from glpcust g
left join bopname b on b.bnkey = g.gckey

select * from glppmhs order by ghdate desc

select b.bnsnam, i.imstk#, i.imvin, i.immake, i.immodl, i.imkey
from inpmast i 
left join bopname b on b.bnkey = i.imkey

select ptckey
from pdpphdr p
where not exists (
  select 1
  from bopname 
  where bnkey = p.ptckey)
  
select distinct ptskey 
from pdpthdr p
where not exists (
  select 1
  from bopname where bnkey = p.ptskey)
  
select *
from pdpwhsl p  
where not exists (
  select 1
  from bopname where bnkey = p.pbckey)
  
select *
from bopname 
where bnkey in (
  select imkey
  from inpmast
  where trim(imstk#) like '97640%')
  
select * from pdpwhsl 
where left(trim(pbcus#),1) = '2'

  
  