select week(pt.yiclkind) as "Week", 
  sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) as "Total Hours",
  case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then 40
    else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) 
  end as "Reg Hours",
  case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40, 2)
    else 0 
  end as "OT Hours",
  case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then Round(40 * Avg(py.ymrate), 2)
    else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2) * py.ymrate) 
  end as "Reg Pay",
  case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
  then Round((sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40) * Min(py.ymrate * 1.5), 2)
    else 0 
  end as "OT Pay",
  Round(case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then Round(40 * Avg(py.ymrate), 2)
    else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2) * py.ymrate) 
  end + 
  case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
  then Round((sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40) * Min(py.ymrate * 1.5), 2)
    else 0 
  end, 2) as "Total Pay"
from pypclockin pt
  left outer join pymast py on pt.YICO# = py.YMCO# and pt.YIEMP# = py.YMEMPN
where pt.YICO# = 'RY1' and pt.YICLKIND between '8/1/2009' and '8/31/2009' 
  and trim(pt.YIEMP#) = '64015'
group by week(pt.YICLKIND)
