

select 'production', count(*) from rydedata.pdppdet where ptdate = 20140417

select 'production', count(*) from rydedata.sdprdet where ptdate = 20140417

select 'production', count(*) from rydedata.sdprhdr where ptdate = 20140417

select 'production', 'pdppdet', count(*) from rydedata.pdppdet where ptdate = 20140416
union
select 'production', 'sdprdet', count(*) from rydedata.sdprdet where ptdate = 20140416
union
select 'production', 'sdprhdr', count(*) from rydedata.sdprhdr where ptdate = 20140416


select 'production', 'pdppdet', count(*) from rydedata.pdppdet where ptdate = 20140417
union
select 'production', 'sdprdet', count(*) from rydedata.sdprdet where ptdate = 20140417
union
select 'production', 'sdprhdr', count(*) from rydedata.sdprhdr where ptdate = 20140417

select count(*) from rydedata.pypclockin where yiclkind = curdate() and yiclkoutt = '00:00:00'


select 'production', 'sdprdet', count(*) from rydedata.sdprdet where ptdate = 20140317

select ptdate, 'production', 'sdprdet', count(*) from rydedata.sdprhdr
where ptdate between 20140201 and 20140417
group by ptdate
order by ptdate

select gtdate, count(*) from rydedata.glptrns where gtdate between '2014-04-01' and '2014-04-17' group by gtdate order by gtdate

-- all months prior to april look ok
select month(gtdate), count(*) from rydedata.glptrns where year(gtdate) = 2014 group by month(gtdate) 


select 'production', 'pdppdet', count(*) from rydedata.pdppdet where ptdate = 20140419
union
select 'production', 'sdprdet', count(*) from rydedata.sdprdet where ptdate = 20140419
union
select 'production', 'sdprhdr', count(*) from rydedata.sdprhdr where ptdate = 20140419
union
select 'production', 'glptrnsr', count(*) from rydedata.glptrns where gtdate = curdate()
union
select 'production', 'pypclockin', count(*) from rydedata.pypclockin where yiclkind = curdate() and yiclkoutt = '00:00:00'


select curdate(),curdate() - 3,  year(curdate()), month(curdate()), day(curdate())from sysibm.sysdummy1

select curdate(), 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1


select * from (
select ptdate, 'production' as server, 'sdprdet' as "theTable", count(*) from rydedata.sdprdet 
  where ptdate between 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate() - 20 day)from sysibm.sysdummy1)
    and 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1)
group by ptdate
union
select ptdate, 'production', 'sdprhdr', count(*) from rydedata.sdprhdr 
  where ptdate between 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate() - 20 day)from sysibm.sysdummy1)
    and 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1)
group by ptdate
union
select ptdate, 'production', 'pdppdet', count(*) from rydedata.pdppdet 
  where ptdate between 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate() - 20 day)from sysibm.sysdummy1)
    and 
    (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1)
group by ptdate
) x order by "theTable", ptdate

-- today only
select 'production' as server, 'sdprdet' as theTable, count(*) from rydedata.sdprdet 
  where ptdate = (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1) 
group by ptdate
union
select 'production', 'sdprhdr', count(*) from rydedata.sdprhdr 
  where ptdate = (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1)
group by ptdate
union
select 'production', 'pdppdet', count(*) from rydedata.pdppdet 
  where ptdate = (select 10000 * year(curdate()) + 100 * month(curdate()) + day(curdate())from sysibm.sysdummy1)
group by ptdate
union
select 'production', 'glptrns', count(*) from rydedata.glptrns where gtdate = curdate()


-- this is the coolest !
-- stolen from :http://stackoverflow.com/questions/29923987/recursive-query-for-date-range

/*  
WITH DATE_RANGE(DATE_FOR_SHIFT)
     AS (SELECT DATE('2015-04-01')
         FROM   SYSIBM.SYSDUMMY1
         UNION ALL
         SELECT DATE_FOR_SHIFT + 1 DAY
         FROM   DATE_RANGE
         WHERE  DATE_FOR_SHIFT <= '2015-05-01')
SELECT DATE_FOR_SHIFT
FROM   DATE_RANGE;
*/


WITH 
  jan(ptdate)
     AS (SELECT 20160120
         FROM   SYSIBM.SYSDUMMY1
         UNION ALL
         SELECT ptdate + 1 
         FROM   jan
         WHERE  ptdate <= 20160130),
  feb(ptdate)
     AS (SELECT 20160201
         FROM   SYSIBM.SYSDUMMY1
         UNION ALL
         SELECT ptdate + 1 
         FROM   feb
         WHERE  ptdate <= 20160203)

select a.ptdate, /*'production' as server, 'pdpphdr' as the_table,*/ coalesce(b.the_count, 0) as the_count
from (         
  SELECT ptdate
  FROM   jan
  union 
  select ptdate
  from feb) a
left join (
  select ptdate, count(*) as the_count
  from rydedata.pdpphdr
  group by ptdate) b on a.ptdate = b.ptdate  
order by a.ptdate 

-- need to do a nightly check, 

  select 'pdpdet', count(*)
  from rydedata.pdppdet
  where ptdate = 20160202



WITH
--  feb(ptdate)
--     AS (SELECT 20160220
--         FROM   SYSIBM.SYSDUMMY1
--         UNION ALL
--         SELECT ptdate + 1 
--         FROM   feb
--         WHERE  ptdate <= 20160228),
  mar(ptdate) 
     AS (SELECT 20160301
         FROM   SYSIBM.SYSDUMMY1
         UNION ALL
         SELECT ptdate + 1 
         FROM   mar
         WHERE  ptdate <= 20160314)                 
--select a.ptdate, coalesce(b.the_count, 0) as the_count
--from feb a
--left join (
--  select ptdate, count(*) as the_count
--  from rydedata.pdpphdr
--  group by ptdate) b on a.ptdate = b.ptdate  
--union
select c.ptdate, coalesce(d.the_count, 0) as the_count
from mar c
left join (
  select ptdate, count(*) as the_count
  from rydedata.pdpphdr
  group by ptdate) d on c.ptdate = d.ptdate    
order by ptdate 


