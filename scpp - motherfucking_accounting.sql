select *
from (
  select trim(a.gtctl#) as gtctl#, a.gtdate, a.gttamt
  FROM  rydedata.glptrns a
  inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.account_number)
  where  b.year = 2016
      and b.account_type = '4'
      and b.department in ('NC','UC')
      and b.account_desc not like '%%WH%%'
      and b.account_sub_type in ('A','B')
      and a.gtpost <> 'V'
      -- and a.gttamt < 0
      and a.gtjrnl in ('VSN','VSU')
      and a.gtdate > '2015-01-01') x
inner join (      
  select trim(a.gtctl#) as gtctl#, a.gtdate
  FROM  rydedata.glptrns a
  inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.account_number)
  where  b.year = 2016
      and b.account_type = '4'
      and b.department in ('NC','UC')
      and b.account_desc not like '%%WH%%'
      and b.account_sub_type in ('A','B')
      and a.gtpost <> 'V'
      -- and a.gttamt < 0
      and a.gtjrnl in ('VSN','VSU')
      and a.gtdate > '2015-01-01'
  group by trim(a.gtctl#), a.gtdate
  having count(*) > 1) y on x.gtctl# = y.gtctl# and x.gtdate = y.gtdate   
order by x.gtctl#, x.gtdate  