Select * from RYDEDATA.GLPTRNS

Select count(*) from RYDEDATA.GLPTRNS

select gtdesc, count(*) as howmany
from rydedata.glptrns
group by gtdesc
order by howmany desc

select gttype, count(*)
from RYDEDATA.GLPTRNS
group by gttype

-- gttrn#/gtseq# not unique
select gttrn#, gtseq#
from RYDEDATA.GLPTRNS
group by gttrn#, gtseq#
having count(*) > 1

select gttrn#, gtseq#
from RYDEDATA.GLPTRNS
WHERE gttrn# <> -555555555
group by gttrn#, gtseq#
having count(*) > 1



order by g.gttrn#

select count(*)
from rydedata.glptrns
where gttrn# = -555555555
    

select *
from rydedata.glptrns
where gttrn# = 1736956

select *
from rydedata.glptrns
where gttrn# = 1351515



Select day(gtdate), count(*)
from RYDEDATA.GLPTRNS
where year(gtdate) = 2011
  and month(gtdate) = 10
group by day(gtdate)

Select count(*)
from RYDEDATA.GLPTRNS
where year(gtdate) = 2011
  and month(gtdate) = 10
  and day(gtdate) = 1

select *
from RYDEDATA.GLPTRNS
where year(gtdate) = 2011
  and month(gtdate) = 10
  and day(gtdate) = 1

-- records with duplicate gttrn#/gtseq#
select g.*
from rydedata.glptrns g 
inner join (
  select gttrn#, gtseq#
  from RYDEDATA.GLPTRNS
  group by gttrn#, gtseq#
  having count(*) > 1) s on g.gttrn# = s.gttrn# and g.gtseq# = s.gtseq#


journal or control
-- nope, not journal
select g.*
from rydedata.glptrns g 
inner join (
  select gttrn#, gtseq#, gtjrnl
  from RYDEDATA.GLPTRNS
  group by gttrn#, gtseq#, gtjrnl
  having count(*) > 1) s on g.gttrn# = s.gttrn# and g.gtseq# = s.gtseq# and g.gtjrnl = s.gtjrnl

-- nope, not journal control
select g.*
from rydedata.glptrns g 
inner join (
  select gttrn#, gtseq#, gtjrnl, gtctl#
  from RYDEDATA.GLPTRNS
  group by gttrn#, gtseq#, gtjrnl, gtctl#
  having count(*) > 1) s on g.gttrn# = s.gttrn# and g.gtseq# = s.gtseq# and g.gtjrnl = s.gtjrnl and g.gtctl# = s.gtctl#

-- account control journal seq
select gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#
from RYDEDATA.GLPTRNS
group by gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#
having count(*) > 1

-- there is no combination of unique fields
-- although adding where trim(g.gtacct) <> '*VOID' gets it down to 12 rows
select g.*
from rydedata.glptrns g 
inner join (
  select gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#, gttamt
  from RYDEDATA.GLPTRNS
where year(gtdate) = 2011
and gtpost <> 'V'
  group by gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#, gttamt
  having count(*) > 1) s on g.gtacct = s.gtacct and g.gtctl# = s.gtctl# and g.gtseq# = s.gtseq# and g.gtdate = s.gtdate and g.gttrn# = s.gttrn# and g.gttamt = s.gttamt
where trim(g.gtacct) <> '*VOID'




  select gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#, gttamt
  from RYDEDATA.GLPTRNS
  where trim(gtacct) <> '*VOID'
  group by gtacct,gtctl#, gtjrnl, gtseq#, gtdate, gttrn#, gttamt
  having count(*) > 1

--11/23/11
-- journal/document/sequence
select gtjrnl, gtseq#, gtdoc#
from RYDEDATA.GLPTRNS
where trim(gtacct) <> '*VOID'
group by gtjrnl, gtseq#, gtdoc#
having count(*) > 1

select *
from rydedata.glptrns g
inner join (
    select gtjrnl, gtseq#, gtdoc#
    from RYDEDATA.GLPTRNS
    where trim(gtacct) <> '*VOID'
    group by gtjrnl, gtseq#, gtdoc#
    having count(*) > 1) c on g.gtjrnl = c.gtjrnl 
  and g.gtseq# = c.gtseq# 
  and g.gtdoc# = c.gtdoc#
where year(g.gtdate) = 2011
order by g.gtjrnl, g.gtseq#,g.gtdoc#


select *
from rydedata.glpdtim
where gqtrn# = 1746048

-- 12/15
/*
turns out that records disappear from glptrns when docs get voided
gtpost is changed from Y to V
so, the strategy to pick this up is to do a count glptrns/month vs stgArkonaGLPTRNS/month
if the count differs, the month has to be reloaded
to do a single month (on the server, 10/11 186K) takes 14 minutes
the question is, which months need to be counted
i'm thinking the previous month is adequate
*/
select month(current date - 1 month) as countMonth, year(current date - 1 month) as countYear
from sysibm.sysdummy1

select *
-- select count(*)
from rydedata.glptrns
where month(gtdate) = 11
  and year(gtdate) = 2011 
  and gtpost = 'Y'

select 
  sum(case when month(gtdate) = 11 then 1 else 0 end) as Nov,
  sum(case when month(gtdate) = 10 then 1 else 0 end) as Oct,
  sum(case when month(gtdate) = 9 then 1 else 0 end) as Sep,
  sum(case when month(gtdate) = 8 then 1 else 0 end) as Aug,
  sum(case when month(gtdate) = 7 then 1 else 0 end) as Jul,
  sum(case when month(gtdate) = 6 then 1 else 0 end) as Jun,
  sum(case when month(gtdate) = 5 then 1 else 0 end) as May,
  sum(case when month(gtdate) = 4 then 1 else 0 end) as Apr,
  sum(case when month(gtdate) = 3 then 1 else 0 end) as Mar,
  sum(case when month(gtdate) = 2 then 1 else 0 end) as Feb,
  sum(case when month(gtdate) = 1 then 1 else 0 end) as Jan  
from rydedata.glptrns
where gtpost = 'Y'
  and year(gtdate) = 2011


select gttype, gtdtyp, gtpost, count(*)
from rydedata.glptrns
where gtpost = 'Y'
group by gttype, gtdtyp, gtpost

--12/20
-- back to unique records, need a natural key
select gttrn#, gtseq#
from rydedata.glptrns
where gtpost = 'Y'
  and gtdate > '2009-01-01'
group by gttrn#, gtseq#
  having count(*) > 1

select *
from rydedata.glptrns
where gttrn# in (1169305, 1536975, 1563205, 1758869)
order by gttrn#, gtseq#


select gtacct, sum(gttamt)
from rydedata.glptrns
where gttrn# in (1169305, 1536975, 1563205, 1758869)
group by gtacct


--8/1/15
-- back to unique records, need a natural key
select gttrn#, gtseq#
from rydedata.glptrns
where gtpost = 'Y'
  and gtdate > '2009-01-01'
  and gttrn# > 0
group by gttrn#, gtseq#
  having count(*) > 1

select *
from (
  select gttrn#, gtseq#
  from rydedata.glptrns
  where gtpost = 'Y'
    and gtdate > '2009-01-01'
    and gttrn# > 0
  group by gttrn#, gtseq#
    having count(*) > 1) a
left join rydedata.glptrns b on a.gttrn# = b.gttrn#
  and a.gtseq# = b.gtseq#
  
select * from (    
  select
  gtco#, gttrn#, gtseq#, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct) as gtacct, trim(gtctl#) as gtctl#, gtdoc#, trim(gtrdoc#) as gtrdoc#, gtrdtyp,
  gtodoc#, gtref#, gtvnd#, gtdesc, gttamt, gtcost, gtctlo,gtoco#
FROM  rydedata.glptrns
where gtdate between '2015-06-01' and '2015-06-30'
) x where gtacct = '120300'


select gtdate, month(gtdate), year(gtdate) 
from rydedata.glptrns
where gttrn# = 2952742

