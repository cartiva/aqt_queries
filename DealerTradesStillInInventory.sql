Select * from inpmast
where trim(imstk#) in ('99103','99137','99437','99443')
where imcost = 0
and imstat = 'I'

-- Document Inquiry on 99103

select *
from rydelf.glltrnb
where trim(gtdoc#) = '99103'

select *
-- select sum(gttamt)
from rydedata.glptrns
where trim(gtdoc#) = '99103'

select *
-- select sum(gttamt)
from rydedata.glptrns
where trim(gtdoc#) = '99452'

-- vehicles with imstat = I and 0 balance in glptrns
select i.imco#, trim(i.imstk#), i.imvin, i.imyear, i.immake, i.immodl, i.imcost
from inpmast i
left join (
  select gtdoc#, sum(gttamt) as TAmt
  from glptrns
  group by gtdoc#) g on g.gtdoc# = i.imdoc#
where i.imstat = 'I'
and g.Tamt = 0
and g.gtdoc# <> ''
-- and trim(imstk#) in ('99103','99137','99437','99443')
order by trim(i.imstk#)

-- nope looks like this is not it, if i was correct, this should show all the inventory
select i.imco#, trim(i.imstk#), i.imvin, i.imyear, i.immake, i.immodl, i.imcost, g.tamt
from inpmast i
left join (
  select gtdoc#, sum(gttamt) as TAmt
  from glptrns
  group by gtdoc#) g on g.gtdoc# = i.imdoc#
where i.imstat = 'I'
and g.Tamt <> 0
and g.gtdoc# <> ''
-- and trim(imstk#) in ('99103','99137','99437','99443')
order by trim(i.imstk#)



select *
from inpmast
where trim(imstk#) in ('99610','99602','99695','99860','99944')

select gtdoc#, gtseq#, gtdtyp, gttype, gtpost, gtjrnl, gtacct, gttamt
from glptrns
where trim(gtdoc#) in ('99610','99602','99695','99860','99944')
order by gtdoc#

select *
from inpmast i
where exists (
  select imvin
  from inpmast
  where imvin = i.imvin
  group by imvin
  having count(*) > 1)
order by i.imvin  
  