select 'f3429528-9e83-40cb-81b1-070f305500f1' as DealerKey, trim(imstk#) as StockNumber, imvin as VIN, imyear as Year, immake as Make, 
  immodl as Model, imtype as "N/U", imodom as Miles, imcost as Cost, impric as Price
from inpmast
where imstat = 'I'
order by trim(imstk#)