Select swco#, swswid, swname, swemp#, swactive, trim(swmisc)
from RYDEDATA.SDPSWTR

Select swco#, swswid, swname, swemp#, swactive, swmisc
from RYDEDATA.SDPSWTR
where swswid = '666'

select trim(swmisc), swactive, count(*)
from rydedata.sdpswtr
--where swmisc not like '%ALL%'
group by trim(swmisc), swactive

Select swco#, swswid, swname, swemp#, swactive,
  case 
    when trim(swmisc) like '%QL%' then 'QL'
    when trim(swmisc) like '%AM%' then 'AM'
    when trim(swmisc) like '%BS%' then 'BS'
    when trim(swmisc) like '%MR%' then 'MR'
    when trim(swmisc) like '%RE%' then 'RE'
    else 'XX'
  end as DefaultServiceType
from rydedata.sdpswtr
where swco# in ('RY1','RY2')


select swco#,swswid
from (
  Select swco#, swswid, swname, swemp#, swactive,
    case 
      when trim(swmisc) like '%QL%' then 'QL'
      when trim(swmisc) like '%AM%' then 'AM'
      when trim(swmisc) like '%BS%' then 'BS'
      when trim(swmisc) like '%MR%' then 'MR'
      when trim(swmisc) like '%RE%' then 'RE'
      else 'ALL'
    end as DefaultServiceType
  from rydedata.sdpswtr
  --where (swmisc like '%QL%' or swmisc like '%AM%' or swmisc like '%BS%' or swmisc like '%MR%' or swmisc like '%RE%')
  where swco# in ('RY1','RY2')) a
group by swco#, swswid
having count(*) > 1