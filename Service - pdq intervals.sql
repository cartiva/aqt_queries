select * from sdprhdr where length(trim(ptro#)) = 8 and left(trim(ptro#),2) = '19' and ptco# = 'RY1' and ptcnam not like '*VOID%'
-- pdq are 190

select ptvin, count(*) 
from sdprhdr 
where length(trim(ptro#)) = 8 
and left(trim(ptro#),2) = '19' 
and ptco# = 'RY1' 
and ptcnam not like '*VOID%'
group by ptvin
having count(*) > 1

select ptvin, ptdate
from sdprhdr 
where ptvin in (
  select ptvin 
  from sdprhdr 
  where length(trim(ptro#)) = 8 
  and left(trim(ptro#),2) = '19' 
  and ptco# = 'RY1' 
  and ptcnam not like '*VOID%'
  and ptdate > '20090800'
  group by ptvin
  having count(*) > 1)
and ptdate > '20090800'


