select * from inpmast where trim(immake) = 'HONDA' and imtype = 'N'
-- 4/28/10
-- Service Group definitions stored in SDPFRND
-- where does "Warranty Other" surface, may be a way to find those vins which are causing recurring problems
-- haven't figured that out yet
-- for now, add known problematic VINS eg Honda: 19X
select distinct left(trim(imvin),3) from inpmast where trim(immake) = 'HONDA' and imtype = 'N' -- 10
select distinct left(trim(imvin),3) from inpmast where trim(immake) = 'HONDA' and imyear > 2005 -- 16
select distinct left(trim(imvin),3) from inpmast where trim(immake) = 'HONDA' and imyear > 2005  and substring(imvin, 2,1) <> 'H'

select left(trim(imvin),3), immake, count(*) as HowMany from inpmast group by left(trim(imvin),3), immake order by HowMany desc

--Honda
-- ok to use H
select immake, count(immake) as HowMany from inpmast where substring(imvin, 2,1) = 'H' group by immake order by HowMany desc
select left(trim(imvin),3), count(imvin) as HowMany from inpmast where trim(immake) = 'HONDA' and substring(trim(imvin), 2,1) <> 'H' group by left(trim(imvin),3) order by HowMany desc
select immake, count(immake) from inpmast where left(trim(imvin),3) in ('5FN','5J6','4S6','19X','5FP','3CZ') group by immake

--Nissan
-- just N looks iffy
select immake, count(immake) as HowMany from inpmast where substring(imvin, 2,1) = 'N' group by immake order by HowMany desc
-- looks ok to go with this list
select left(trim(imvin),3), count(imvin) as HowMany from inpmast where trim(immake) = 'NISSAN' group by left(trim(imvin),3) order by HowMany desc
select immake, count(immake) from inpmast where left(trim(imvin),3) in ('1N4','JN8','JN1','1N6','3N1','5N1','4N2','JN6','1N1') group by immake

select * from inpmast where trim(immake) = 'CHEVROLET' and left(trim(imvin),3) in ('1N4','JN8','JN1','1N6','3N1','5N1','4N2','JN6','1N1')

select immake, count(*) as HowMany from inpmast group by immake order by HowMany desc

select * from inpmast where left(trim(imvin),3) = '1N8'

-- GM
select immake, count(immake) as HowMany from inpmast where substring(imvin, 2,1) = 'G' group by immake order by HowMany desc
select left(trim(imvin),3), count(imvin) as HowMany from inpmast where trim(immake) in ('CHEVROLET','PONTIAC','BUICK','GMC','OLDSMOBILE','CADILLAC','SATURN','HUMMER') and substring(trim(imvin), 2,1) <> 'G' group by left(trim(imvin),3) order by HowMany desc

select * from inpmast where left(trim(imvin),3) = 'CCL'




select immake, count(immake) as HowMAny from inpmast group by immake order by howmany desc


select immake, count(*)
from inpmast
where left(trim(imvin),3) in (select distinct left(trim(imvin),3) from inpmast where trim(immake) = 'HONDA' and imtype = 'N')
group by immake

select immake, count(*)
from inpmast
where substring(imvin, 2,1) = 'H'
--and imtype = 'N'
and imyear > 2005
group by immake

select immake, imvin
from inpmast
where substring(imvin, 2,1) = 'H'
and imtype = 'N'

select distinct immake from inpmast where left(trim(imvin),3) = '4S6'

select * from sdprhdr s where not exists(select 1 from inpmast where imvin =s.ptvin)

select 

select * from sdprdet where ptlpym = 'W' and ptsvctyp = 'OT'

select * from sdprhdr where left(trim(ptvin),3) = '19X'

select * from sdprdet where trim(ptro#) = '2614824'

select * from glptrns where trim(gtdoc#) = '2614824'