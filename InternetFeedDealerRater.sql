/*
Attached you will find our feed specs and our ftp information. 
When the feed is complete please notify of the filename used. 
You will also need your Dealer ID which is 22365. 
------------------------------------------------------------
ftp://classifiedfeed.dealerrater.com
Username: feed
Password: dealer##123
------------------------------------------------------------
Dealer_ID (DealerRater Internal ID)
Listing_ID (Used by DMS sometimes, optional)
VIN_No (required)
New_Used (New, NEW, Used, USED, N, U)
Stock_No (Used by dealer sometimes, optional)
Year (integer)
Make (string)
Model (string)
Body_Style (string, optional)
Doors (string, optional)
Trim (string, optional)
Ext_Color (string, optional)
Int_Color (string, optional)
Engine (string, optional)
Fuel (string, optional)
Drivetrain (string, optional)
Transmission (string, optional)
Mileage (string, optional)
Internet_Price (string, optional)
Certified (string (Yes, YES, No, NO, 1, 0, Y, N), optional)
Options (string, optional)
Description (string, optional)
Photo_URLs (string, comma delimited, optional)
Date_In_Stock (string, optional)
*/
/*
-- basic concept, with all inventory accounts
Select gtctl#, in.imstat, in.imtype, sum(gttamt) as Cost
from glptrns gl
left join inpmast in on gl.gtctl# = in.imstk#
where trim(gtacct) in (
  select distinct gmacct
  from glpmast
  where gmdesc like 'INV%'
  and gmdesc not like '%PARTS%'
  and gmdesc not like '%GAS%'
  and gmdesc not like '%PAINT%'
  and gmdesc not like '%SUBLET%'
  and gmdesc not like '%WIP%'
  and gmdesc not like 'INVEST%'
  and gmdesc not like '%OTHER AUTOMOTIVE%'
  and gmdesc not like '%ACCESS%'
  and gmdesc not like '%PROCESS%'
  and gmdesc not like '%MISC%'
  and gmdesc not like '%TIRES%'
  and gmdesc not like '%G.O%'
  and trim(gmdesc) <> 'INV-OTHER')
and in.imstat = 'I' -- Inventory
and in.imtype = 'N' -- New
and gl.gtpost <> 'V' -- Void
group by gtctl#, in.imstat, in.imtype
having sum(gttamt) <> 0
order by trim(gtctl#)
*/
-- DealerRater Feed
select '22365' as Dealer_ID, '' as Listing_ID, imvin as VIN_No, 'NEW' as New_Used, imstk# as Stock_No, imyear as Year, immake as Make, immodl as Model, imbody as BodyStyle, '' as Doors, '' as Trim, imcolr as Ext_Color, imtrim as Int_Color,'' as Engine, '' as Fuel,'' as Drivetrain,'' as Transmission, '' as Mileage,
  '' as Internet_Price,'' as Certified,'' as Options,'' as Description,'' as Phot_URLs,'' as Date_In_Stock
from inpmast i
where imtype = 'N'
and imstat = 'I'  
and imstk# in (
  Select gtctl#
  from glptrns gl
  left join inpmast in on gl.gtctl# = in.imstk#
  where trim(gtacct) in (
    select distinct gmacct
    from glpmast
    where gmdesc like 'INV%'
    and gmdesc not like '%PARTS%'
    and gmdesc not like '%GAS%'
    and gmdesc not like '%PAINT%'
    and gmdesc not like '%SUBLET%'
    and gmdesc not like '%WIP%'
    and gmdesc not like 'INVEST%'
    and gmdesc not like '%OTHER AUTOMOTIVE%'
    and gmdesc not like '%ACCESS%'
    and gmdesc not like '%PROCESS%'
    and gmdesc not like '%MISC%'
    and gmdesc not like '%TIRES%'
    and gmdesc not like '%G.O%'
    and trim(gmdesc) <> 'INV-OTHER')
  and in.imstat = 'I' -- Inventory
  and in.imtype = 'N' -- New
  and gl.gtpost <> 'V' -- Void
  group by gtctl#
  having sum(gttamt) <> 0)
