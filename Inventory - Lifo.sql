/*
-- Current Inventory Cost
select trim(gtctl#), sum(gttamt) as Cost
from rydedata.glptrns
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
--  and trim(gtctl#) = '12472A'
group by gtctl#

select stocknumber, cost
from (
select trim(gtctl#) as stocknumber, sum(gttamt) as Cost
from rydedata.glptrns
where trim(gtacct) in ('124000', '124100') -- Inventory accounts only
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
--  and trim(gtctl#) = '12472A'
group by gtctl#) wtf
where COST <> 0

Cost = Source = "Auction" & Recon > 0; (Net - Recon - 400);  Source = "Auction"; (Net - 400);   Source != "Auction" & Recon > 0; (Net - Recon);   Source != "Auction"; Net; Net
*/
/*
1/10/2011
added gtdate(transaction date) < 1/1/2011
which cleaned up a bunch of discrepancies with Marlee

1-8-2013
trying to remember what the fuck this all is
ok
run zjonLiroArkona to generate insert statements (for ads)
lists 543 vehicles, seems low compared to the tool
check deals for used cars sold in january that were inventory in 2012

select 
  case 
    when right(trim(i.imstk#),1) = 'X' then 'Purchase'
    else 'Trade'
  end as Source,
  trim(i.imstk#) as stocknumber, trim(i.imvin) as VIN, i.imyear, i.immake, i.immodl,
  i.imtrim, i.imbody, imodom, 
  substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4) imdinv ,
  b.bmdtor, b.bmdtaprv, b.bmdtcap
from rydedata.inpmast i
inner join rydedata.bopmast b on i.imvin = b.bmvin
  and b.bmvtyp = 'U'
  and year(b.bmdtaprv) = 2013
where i.imstat = 'C'
  and i.imtype = 'U'
  and left(trim(i.imstk#), 1) <> 'C'
  and trim(i.imstk#) not in ('HTEST','22002VB')
  and i.imdinv < 20130000

select * from rydedata.bopmast

*/
/*
1/5/14
  *b* generation of net, limit transactions to < 2014/01/01
  coerced i.imyear to character

1/3/2015: just change the dates
when exporting to excel, specify representing nulls as NULL
1/2/17 gtpost = 'Y' : avoids the mess of unposted lines
*/
-- zjonLifoArkona2015
-- zjonLifoArkona2016

select 
  case 
    when right(trim(i.imstk#),1) = 'X' then 'Purchase'
    else 'Trade'
  end as Source,
  trim(i.imstk#) as stocknumber, trim(i.imvin) as VIN, char(i.imyear), i.immake, i.immodl,
  i.imtrim, i.imbody, imodom, 
  substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4) imdinv,
  cast(c.net as integer) as net,
  cast(r.recon as integer) as recon,
  cast(
    case 
    when right(trim(i.imstk#),1) = 'X' then
    case
      when coalesce(recon, 0) > 0 then
      net - recon - 400
      else
      net - 400
    end 
    else
    case
      when coalesce(recon, 0) > 0 then
      net - recon
      else
      net - 400
    end  
    end as integer) as Cost
from rydedata.inpmast i
left join (
  select trim(gtctl#) as stocknumber, sum(gttamt) as Net
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) = 'Y'  -- Ignore VOIDS
    and gtdate < '2017-01-01' -----------------------------------------------------------------------------------------------------------------
  group by gtctl#
    having sum(gttamt) > 0) c on trim(i.imstk#) = c.stocknumber
left join (  
  select trim(gtctl#) as stocknumber, sum(gttamt) as Recon
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) = 'Y'  -- Ignore VOIDS
  and trim(gtjrnl) in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
    and gtdate < '2017-01-01' -----------------------------------------------------------------------------------------------------------------
  group by gtctl#
    having sum(gttamt) > 0) r on trim(i.imstk#) = r.stocknumber      
where i.imstat = 'I'
  and i.imtype = 'U'
  and left(trim(i.imstk#), 1) <> 'C'
  and trim(i.imstk#) not in ('HTEST','22002VB')
  and i.imdinv < 20170000 ---------------------------------------------------------------------------------------------------------------------

union
-- january sales
select 
  case 
    when right(trim(i.imstk#),1) = 'X' then 'Purchase'
    else 'Trade'
  end as Source,
  trim(i.imstk#) as stocknumber, trim(i.imvin) as VIN, char(i.imyear), i.immake, i.immodl,
  i.imtrim, i.imbody, imodom, 
  substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4) imdinv,
  cast(c.net as integer) as net,
  cast(r.recon as integer) as recon,
  cast(
    case 
    when right(trim(i.imstk#),1) = 'X' then
    case
      when coalesce(recon, 0) > 0 then
      net - recon - 400
      else
      net - 400
    end 
    else
    case
      when coalesce(recon, 0) > 0 then
      net - recon
      else
      net - 400
    end  
    end as integer) as Cost
from rydedata.inpmast i
inner join rydedata.bopmast b on i.imvin = b.bmvin
  and b.bmvtyp = 'U'
  and year(b.bmdtaprv) = 2017 -----------------------------------------------------------------------------------------------------------------
left join (
  select trim(gtctl#) as stocknumber, sum(gttamt) as Net
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) = 'Y'  -- Ignore VOIDS
-- *b*
    and gtdate < '2017-01-01' -------------------------------------------------------------------------------------------------------------------
  group by gtctl#
    having sum(gttamt) > 0) c on trim(i.imstk#) = c.stocknumber
left join (  
  select trim(gtctl#) as stocknumber, sum(gttamt) as Recon
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) = 'Y'  -- Ignore VOIDS
    and trim(gtjrnl) in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
  group by gtctl#
    having sum(gttamt) > 0) r on trim(i.imstk#) = r.stocknumber    
where i.imstat = 'C'
  and i.imtype = 'U'
  and left(trim(i.imstk#), 1) <> 'C'
  and trim(i.imstk#) not in ('HTEST','22002VB')
  and i.imdinv < 20170000 -------------------------------------------------------------------------------------------------------------------------



/*
1/8/2012
left out those inventory vehicles sold in november, here is the revised addtional vehicles list
-- added stocknumber list
*/
select 
  case 
    when right(trim(i.imstk#),1) = 'X' then 'Purchase'
    else 'Trade'
  end as Source,
  trim(i.imstk#) as stocknumber, trim(i.imvin) as VIN, i.imyear, i.immake, i.immodl,
  i.imtrim, i.imbody, imodom, 
  substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4) imdinv,
  cast(c.net as integer) as net,
  cast(r.recon as integer) as recon,
  cast(
    case 
    when right(trim(i.imstk#),1) = 'X' then
    case
      when coalesce(recon, 0) > 0 then
      net - recon - 400
      else
      net - 400
    end 
    else
    case
      when coalesce(recon, 0) > 0 then
      net - recon
      else
      net - 400
    end  
    end as integer) as Cost
from rydedata.inpmast i
left join (
  select trim(gtctl#) as stocknumber, sum(gttamt) as Net
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) <> 'V'  -- Ignore VOIDS
--    and gtdate < '2012-01-01'
  group by gtctl#
    having sum(gttamt) > 0) c on trim(i.imstk#) = c.stocknumber
left join (  
  select trim(gtctl#) as stocknumber, sum(gttamt) as Recon
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) <> 'V'  -- Ignore VOIDS
  and trim(gtjrnl) in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
--    and gtdate < '2012-01-01'
  group by gtctl#
    having sum(gttamt) > 0) r on trim(i.imstk#) = r.stocknumber
where trim(i.imstk#) in (
'12502A',
'13254B',
'14478A',
'14952XX',
'14994A',
'15228A',
'15357A',
'15539A',
'15540A',
'15564P',
'15602A',
'15683X',
'15693A',
'15700A',
'15728A',
'15964X',
'14551XX',
'14952XX',
'14987XB',
'15120XX',
'15121XX',
'15295XX',
'15640XX',
'14511XXB',
'14909XXA',
'15304XXA',
'15379XXA',
'15394XXB',
'15450XXA',
'15514XXA',
'15598A',
'H4347G',
'C4747X',
'H4342G',
'C4748X')
order by i.imstk#

