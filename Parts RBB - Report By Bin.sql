select pmbinl as "Bin", pmpart as "Part #", pmonhd as "On Hand", pmgrpc as Group, pmdesc as Description
-- select *
from pdpmast
--where trim(pmgrpc) = '02187'
WHERE pmonhd > 0
and pmco# = 'RY1'
--and pmbinl > '20D'
order by pmbinl, pmgrpc, pmpart, pmonhd

-- 14164 part numbers
select count(*)
FROM pdpmast
where pmonhd >0 
and pmco# = 'RY1'

-- 3135 bins
select count(*)
from (
  select distinct pmbinl
  from pdpmast
  where pmco# = 'RY1') bin

-- count of records per bin 
select pmbinl, count(pmbinl)
from pdpmast
where pmco# = 'RY1'
and pmonhd >0 
group by pmbinl
order by pmbinl desc
order by count(pmbinl) desc

-- this looks like what we need for the page
select pmbinl as "Bin", pmpart as "Part #", pmonhd as "On Hand", pmgrpc as Group, pmdesc as Description
from pdpmast
where pmco# = 'RY1'
and pmonhd >0 
and trim(pmbinl) like '343%' 
order by pmbinl, pmpart, pmgrpc


select pmbinl, pmpart, pmonhd, pmgrpc, pmdesc
from pdpmast
where pmco# = 'RY1'
and pmonhd >0 
and trim(pmbinl) like '343%' 
order by pmbinl, pmpart, pmgrpc

-- 4/14/10
-- Rick needs month/year (date) of the last sale

select p.pmbinl as "Bin", p.pmpart as "Part #", p.pmonhd as "On Hand", p.pmgrpc as Group, p.pmdesc as Description,
  substr(wtf.LastSale, 5,2)||'-'||substr(wtf.LastSale, 7,2)||'-'||left(wtf.LastSale, 4) as "Last Sale"
from pdpmast p
left join (
  select ptpart, max(ptdate) as LastSale 
  from pdptdet 
  group by ptpart) AS wtf on wtf.ptpart = p.pmpart
where p.pmco# = 'RY1'
and p.pmonhd >0 
and trim(p.pmbinl) like '34%' 
order by p.pmbinl, p.pmpart, pmgrpc
