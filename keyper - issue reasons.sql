select *
from asset
order by checkout_date desc

select issue_reason_id, count(*)
from asset
group by issue_reason_id

select issue_reason_id, count(*)
from asset_transactions
group by issue_reason_id

select *
from asset_Transactions a
where a.issue_reason_id = 8

select *
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
where a.issue_reason_id is not null 


select transaction_Date, a.message, a.asset_status, asset_name, c.name, rtrim(d.first_name) + ' ' + d.last_name
-- select 
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
left join [user] d on a.user_id = d.user_id
where a.issue_reason_id is not null 
  --and c.name = 'Demo'
order by transaction_date desc  



select *
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
where asset_name = 'H8583A'
  --and c.name = 'Demo'
order by transaction_Date
-- goofy times, like midnight, the transaction_date field of
-- asset_transactions is datetime with a default of getutcdate()

do a check, are all goofy times from the same cabinets?

-- sql server current date time
SELECT SYSDATETIME()
    ,SYSDATETIMEOFFSET()
    ,SYSUTCDATETIME()
    ,CURRENT_TIMESTAMP
    ,GETDATE()
    ,GETUTCDATE();
    
    
-- demo checkouts per vehicle
select asset_name, count(*), max(cast(transaction_date as date))
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
left join [user] d on a.user_id = d.user_id
where c.name = 'Demo'
group by asset_name
order by count(*) desc 
  

select transaction_date, cast(transaction_date as date)
from asset_transactions


    
-- ry1 used, grouped by vehicle
select asset_name as stock_number, count(*) as demo_checkouts, max(cast(transaction_date as date)) as last_checkout
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
left join [user] d on a.user_id = d.user_id
where c.name = 'Demo'
  and left(asset_name, 1) <> 'H'
  and right(rtrim(asset_name), 1) not in ('1','2','3','4','5','6','7','8','9','0')
group by asset_name
order by count(*) desc 


select count(*) from asset_Transactions

select count(*) from asset


-- all demos
select asset_name as stock_number, cast(transaction_date as date) as the_date
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
left join [user] d on a.user_id = d.user_id
where c.name = 'Demo'
order by the_date desc 

-- count of demo checkouts/stocknumber
select asset_name as stock_number, count(*), max(cast(transaction_date as date)) as most_recent_date
from asset_Transactions a
left join asset b on a.asset_id = b.asset_id
left join issue_reason c on a.issue_reason_id = c.issue_reason_id
left join [user] d on a.user_id = d.user_id
where c.name = 'Demo'
group by asset_name
order by max(cast(transaction_date as date)) desc 
