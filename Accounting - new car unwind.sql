Select a.gttrn#, a.gtseq#, a.gtacct, a.gtpost, a.gtjrnl, a.gtdate, a.gttamt, a.gtdoc#, a.gtdesc, b.gmdesc 
from RYDEDATA.GLPTRNS a 
left join rydedata.glpmast b on trim(a.gtacct) = trim(b.gmacct) 
  and b.gmyear = 2015
where trim(gtctl#) = '26418' 
order by gtacct


-- compare this to a sold car
-- inventory account (123706) is 0 if the voids are not excluded
Select a.gtacct, b.gmdesc,  sum(a.gttamt)
from RYDEDATA.GLPTRNS a 
left join rydedata.glpmast b on trim(a.gtacct) = trim(b.gmacct) 
  and b.gmyear = 2015
where trim(gtctl#) = '26418' 
  and gtpost <> 'V'
group by a.gtacct, b.gmdesc