select *--count(*)
from stpldata.sdprdet

select *--count(*)
from stpldata.sdprhdr

select left(ptdate, 4), count(*)
from stpldata.sdprhdr
group by left(ptdate, 4)
order by left(ptdate, 4)


select year(gtdate), count(*)
--select *
from stpldata.glptrns
group by year(gtdate)


select count(*)
from stpldata.glpmast


select imco#, count(*)
from stpldata.inpmast
group by imco#


select *
from stpldata.inpmast
where trim(imco#) <> 'ST1'


select  imco#, imvin, imstk#, imdoc#, imstat,  imgtrn, imtype, imfran, imyear, immake, 
  immcode, immodl, imbody, imcolr, imodom, 
  case 
    when imdinv = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imdinv), 4) || '-' || substr(digits(imdinv), 5, 2) || '-' || substr(digits(imdinv), 7, 2) as date) 
  end as imdinv,
  case
    when imdsvc = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imdsvc), 4) || '-' || substr(digits(imdsvc), 5, 2) || '-' || substr(digits(imdsvc), 7, 2) as date) 
  end as imdsvc,
  case
    when imddlv = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imddlv), 4) || '-' || substr(digits(imddlv), 5, 2) || '-' || substr(digits(imddlv), 7, 2) as date)
  end as imddlv,
  case
    when imdord = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imdord), 4) || '-' || substr(digits(imdord), 5, 2) || '-' || substr(digits(imdord), 7, 2) as date) 
  end as imdord, 
  imsact, imiact, imlic#, imkey, imodoma, imcost 
from stpldata.inpmast 
where imco# = 'ST1'


select *
from stpldata.inpmast
where imcost <> 0


select  imco#, imvin, imstk#, imdoc#, imstat,  imgtrn, imtype, imfran, imyear, immake, 
  immcode, immodl, imbody, imcolr, imodom, 
  case 
    when imdinv = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imdinv), 4) || '-' || substr(digits(imdinv), 5, 2) || '-' || substr(digits(imdinv), 7, 2) as date) 
  end as imdinv,
  case
    when imdsvc = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imdsvc), 4) || '-' || substr(digits(imdsvc), 5, 2) || '-' || substr(digits(imdsvc), 7, 2) as date) 
  end as imdsvc,
  case
    when imddlv = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imddlv), 4) || '-' || substr(digits(imddlv), 5, 2) || '-' || substr(digits(imddlv), 7, 2) as date)
  end as imddlv,
  case
    when imdord = 0 then cast('9999-12-31' as date)
    else cast(left(digits(imdord), 4) || '-' || substr(digits(imdord), 5, 2) || '-' || substr(digits(imdord), 7, 2) as date) 
  end as imdord, 
  imsact, imiact, imlic#, imkey, imodoma, imcost 
from stpldata.inpmast 
where imco# = 'ST1'
and trim(imvin) in ('1FAFP663XWK205712', '1FAFP663XWK218573')


select count(*)
from stpldata.inpmast

select *
from stpldata.inpmast
where imco# = 'ST1'
and trim(imvin) in ('1FAFP663XWK205712', '1FAFP663XWK218573')

select *
from stpldata.inpmast
where imco# = 'ST1'
  and (imdinv < 0 or imdinv < 0 or imdsvc < 0 or imddlv < 0)


select *
from stpldata.inpmast
where imco# = 'ST1'
  and (
    (imdinv > 0 and imdinv not between 19900000 and 20130000) 
    or (imdsvc > 0 and imdsvc not between 19900000 and 20130000) 
    or (imddlv > 0 and imddlv not between 19900000 and 20130000)  
    or (imdord > 0 and imdord not between 19900000 and 20130000))



select *
from stpldata.bopmast


