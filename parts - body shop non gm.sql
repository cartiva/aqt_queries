SELECT b.thedate, a.ro, a.partscost, c.*, d.* 
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND a.franchise = 'ot'
  AND c.ptinv# IS NOT NULL  


select *
from rydedata.pdpmast
where trim(pmsgrp) = '900'

select *
from rydedata.pdpmast
where trim(pmpart) = 'MMMM08115'

select *
from rydedata.pdpmast
where pmmanf = 'OT'


select a.ptro#, b.*
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  and b.ptsvctyp = 'BS'
left join (
  select ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  from rydedata.pdptdet
  where (left(trim(ptinv#), 4) = '1800' or left(trim(ptinv#), 4) = '1801')
    and ptmanf = 'OT'
    AND trim(ptsgrp) NOT IN ('501','510','511','512','514','515','518','900')) c ON trim(a.ptro#) = trim(c.ptinv#)
  
where a.ptfran = 'OT'
  and a.ptcdat between 20130000 and 20139999

-- 12/29/13 inventory discrepancy with accounting for body shop non gm parts
select * 
from rydedata.pdptdet
where trim(ptinv#) = '16061936'

select distinct a.ptro#, ptfcdt, b.ptpart, c.gtacct, c.gttamt
-- select *
from rydedata.sdprhdr a
inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
  and b.ptmanf = 'OT' 
left join rydedata.glptrns c on trim(a.ptro#) = trim(c.gtdoc#) 
  and left(trim(c.gtacct), 4) = '1242'
where left(trim(a.ptro#), 2) = '18'
  and ptptot <> 0 -- parts were sold on ro
--  and a.ptfcdt between 20130000 and 20139999
  and c.gtdate between '01/01/2013' and curdate()
  and c.gtacct is not null 

select * from rydedata.glptrns

select gtacct, sum(gttamt) from (
select distinct a.ptro#, ptfcdt, b.ptpart, c.gtacct, c.gttamt
-- select count(*) 
from rydedata.sdprhdr a
inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
  and b.ptmanf = 'OT' 
inner join rydedata.glptrns c on trim(a.ptro#) = trim(c.gtdoc#) 
  and left(trim(c.gtacct), 4) = '1242'
where left(trim(a.ptro#), 2) = '18'
  and ptptot <> 0 -- parts were sold on ro
--  and a.ptfcdt between 20130000 and 20139999
  and c.gtdate between '01/01/2013' and curdate()
  and c.gtacct is not null 
) x group by gtacct

-- assuming those transactions to 124200 are a mistake, should have been to 124205
-- rubber ducked with greg, this is all acct transactions on all ros that had any
-- ot parts
-- need to limit to ros with only ot parts

select f.gtacct, sum(f.gttamt)
from (
  select distinct a.ptro#, ptfcdt, b.ptpart, ptmanf
  -- select count(*) -- 5636
  from rydedata.sdprhdr a
  inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
    and b.ptmanf = 'OT' 
  where left(trim(a.ptro#), 2) = '18'
    and ptptot <> 0 -- parts were sold on ro
    and a.ptfcdt between 20130000 and 20139999
    and not exists (
      select 1
      from rydedata.sdprhdr c
      inner join rydedata.pdptdet d on trim(c.ptro#) = trim(d.ptinv#)
        and d.ptmanf <> 'OT' 
      where trim(c.ptro#) = trim(a.ptro#))) e
inner join rydedata.glptrns f on trim(e.ptro#) = trim(f.gtdoc#)
where left(Trim(f.gtacct), 4) = '1242'
group by f.gtacct


select distinct ptro#
from (
  select distinct a.ptro#, ptfcdt, b.ptpart, ptmanf
  -- select count(*) -- 5636
  from rydedata.sdprhdr a
  inner join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
    and b.ptmanf = 'OT' 
  where left(trim(a.ptro#), 2) = '18'
    and ptptot <> 0 -- parts were sold on ro
    and a.ptfcdt between 20130000 and 20139999
    and not exists (
      select 1
      from rydedata.sdprhdr c
      inner join rydedata.pdptdet d on trim(c.ptro#) = trim(d.ptinv#)
        and d.ptmanf <> 'OT' 
      where trim(c.ptro#) = trim(a.ptro#))) e
inner join rydedata.glptrns f on trim(e.ptro#) = trim(f.gtdoc#)
where Trim(f.gtacct) = '124200'


-- 12/14/14 what a suprise this is still an issue, body shop and parts dept do not reconcile
-- nobody knows wtf is going oin
-- one possible thread: non gm bs ros going to 124200 (GM PARTS & ACC) instead of 124205 (BS P&A NON-GM INVEN)

-- this is what i ran for jeri and dan yesterday as they were finishing up inventory
-- open non gm body shop ros

-- totals:
select sum(ptqty*pmcost) as cost, sum(ptqty*pmlist) as list
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
where a.ptdtyp = 'RO'
  and left(trim(a.ptdoc#), 2) = '18'
  and b.ptpart <> ''
  and c.pmmanf = 'OT'
  and ptqty > 0

-- and details  
--select ptdtyp, ptttyp, ptdoc#, b.ptpart, b.ptqty, c.pmmanf, c.pmcost, c.pmlist 
select ptdoc# as RO, ptpart as "Part Number", ptqty as Quantity, pmcost as Cost, pmlist as List
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
where a.ptdtyp = 'RO'
  and left(trim(a.ptdoc#), 2) = '18'
  and b.ptpart <> ''
  and c.pmmanf = 'OT'
  and ptqty > 0


-- assuming those transactions to 124200 are a mistake, should have been to 124205
-- rubber ducked with greg, this is all acct transactions on all ros that had any
-- ot parts
-- need to limit to ros with only ot parts

-- bs ros for non-gm vehicles with only OT parts
select a.ro_number
from rydedata.sdprhdr a
inner join rydedata.pdptdet b on trim(a.ro_number) = trim(b.ptinv#)
  and b.ptmanf = 'OT'
where a.final_close_date between 20140101 and 20141214
  and left(trim(a.ro_number), 2) = '18'
  and a.franchise_code = 'OT'
  and not exists (
    select 1
    from rydedata.pdptdet bb
    where trim(bb.ptinv#) = trim(b.ptinv#)
      and bb.ptmanf = 'GM')
group by a.ro_number

   
--On 1197 body shop ROs on non-GM vehicles that received ONLY non-GM parts that closed ytd 2014
--From accounting I show: 
--  Acct 124205:  $441,669
--  Acct 124200:    $46,704       
select count(*),
  sum(case when trim(d.gtacct) = '124205' then gttamt else 0 end) as "124205",
  sum(case when trim(d.gtacct) = '124200' then gttamt else 0 end) as "124200"
from ( 
  select a.ro_number
  from rydedata.sdprhdr a
  inner join rydedata.pdptdet b on trim(a.ro_number) = trim(b.ptinv#)
    and b.ptmanf = 'OT'
  where a.final_close_date between 20140101 and 20141214
    and left(trim(a.ro_number), 2) = '18'
    and a.franchise_code = 'OT'
    and not exists (
      select 1
      from rydedata.pdptdet bb
      where trim(bb.ptinv#) = trim(b.ptinv#)
        and bb.ptmanf = 'GM')
  group by a.ro_number) c        
left join rydedata.glptrns d on trim(c.ro_number) = trim(d.gtctl#)
  and trim(d.gtacct) in ('124205','124200')               
where d.gtctl# is not null 
-- group by c.ro_number



-- jeri: Will you send me the RO list that make up the 124200 amount so I can look at a few of them?
select ro_number, gtacct, -1 * gttamt
from ( 
  select a.ro_number
  from rydedata.sdprhdr a
  inner join rydedata.pdptdet b on trim(a.ro_number) = trim(b.ptinv#)
    and b.ptmanf = 'OT'
  where a.final_close_date between 20140101 and 20141214
    and left(trim(a.ro_number), 2) = '18'
    and a.franchise_code = 'OT'
    and not exists (
      select 1
      from rydedata.pdptdet bb
      where trim(bb.ptinv#) = trim(b.ptinv#)
        and bb.ptmanf = 'GM')
  group by a.ro_number) c        
left join rydedata.glptrns d on trim(c.ro_number) = trim(d.gtctl#)
  and trim(d.gtacct) in ('124205','124200')               
where d.gtctl# is not null 
  and d.gtacct = '124200'
order by ro_number


-- jeri is excited, the above helped her figure out what is happening, but it is only half
-- of the problem, what about non-gm parts on gm cars

-- looking at some sort of combination of pay type and stocking group
-- stocking group 920: body shop non gm parts
-- stocked non gm parts: adds a -1 to partnumber
-- pay types, i am confused, something about CP being different than Warr, Internal

select a.ptinv#, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, a.ptcost
-- select *
from rydedata.pdptdet a
where a.ptmanf = 'OT'
  and left(trim(a.ptinv#), 2) = '18'
  and a.ptdate between 20140101 and 20141216
  and a.ptcode in ('CP','IS','SC','WS')
  and trim(ptsgrp) <> '920'

-- group by paytype, stocking group
select ptcode, ptsgrp, sum(ptcost)
from (
  select a.ptinv#, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, a.ptcost
  -- select *
  from rydedata.pdptdet a
  where a.ptmanf = 'OT'
    and left(trim(a.ptinv#), 2) = '18'
    and a.ptdate between 20141201 and 20141216
    and a.ptcode in ('CP','IS','SC','WS')
    and trim(ptsgrp) <> '920') b
group by ptcode, ptsgrp

-- ro is on a GM vehicle
select a.ptinv#, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, a.ptcost
-- select *
from rydedata.pdptdet a
where a.ptmanf = 'OT'
  and left(trim(a.ptinv#), 2) = '18'
  and a.ptdate between 20140101 and 20141216
  and a.ptcode in ('CP','IS','SC','WS')
  and trim(ptsgrp) <> '920'
  and trim(ptinv#) in (
    select trim(ptro#)
    from rydedata.sdprhdr b
    where b.franchise_code = 'GM'
      and left(trim(ptro#), 2) = '18')

  
select a.ptro#, b.ptline, c.ptcode, c.ptdate, c.ptpart, c.ptsgrp, c.ptnet
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
left join rydedata.pdptdet c on trim(a.ptro#) = trim(c.ptinv#) and b.ptline = c.ptline
where a.franchise_code = 'GM'
  and left(trim(a.ptro#), 2) = '18'
  and a.final_close_date between 20140101 and 20141214
  and c.ptinv# is not null 
  and c.ptsgrp <> '920'
  and c.ptmanf = 'OT'
  and c.ptcode in ('IS','SC','WS')
group by a.ptro#, b.ptline, c.ptcode, c.ptdate, c.ptpart, c.ptsgrp, c.ptnet
order by a.ptro#


-- 12/22
-- group by paytype, stocking group
select ptcode, ptsgrp, sum(ptcost)
from (
  select a.ptinv#, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, a.ptcost
  -- select *
  from rydedata.pdptdet a
  where a.ptmanf = 'OT'
    and left(trim(a.ptinv#), 2) = '18'
    and a.ptdate between 20141201 and 20141216
    and a.ptcode in ('CP','IS','SC','WS')
    and trim(ptsgrp) <> '920') b
group by ptcode, ptsgrp

-- part is OT, stock grp <> 920
select a.ptinv#, b.franchise_code, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, a.ptcost
-- select *
from rydedata.pdptdet a
inner join rydedata.sdprhdr b on trim(a.ptinv#) = trim(b.ptro#)
where a.ptmanf = 'OT'
  and left(trim(a.ptinv#), 2) = '18'
  and a.ptdate between 20141201 and 20141216
  and a.ptcode in ('CP','IS','SC','WS')
  and trim(ptsgrp) <> '920'

-- this looks potentially interesting, but the glptrns amount looks like it is doubled  
-- 
select m.ptinv#, m.franchise_code as vehicle, m.ptcode, sum(m."parts amt") as "parts amt",
  sum(case when trim(n.gtacct) = '124200' then gttamt else 0 end) as "124200",
  sum(case when trim(n.gtacct) = '124205' then gttamt else 0 end) as "124205"
from (-- part is OT, stock grp <> 920
  select a.ptinv#, b.franchise_code, a.ptcode, a.ptdate, a.ptpart, a.ptsgrp, 
    a.ptqty * a.ptnet as "parts amt"
  -- select *
  from rydedata.pdptdet a
  inner join rydedata.sdprhdr b on trim(a.ptinv#) = trim(b.ptro#)
  where a.ptmanf = 'OT'
    and left(trim(a.ptinv#), 2) = '18'
    and a.ptdate between 20140101 and 20141216
    and a.ptcode in ('CP','IS','SC','WS')
    and trim(ptsgrp) <> '920') m  
left join rydedata.glptrns n on trim(m.ptinv#) = trim(n.gtctl#)
  and trim(n.gtacct) in ('124200', '124205')
group by m.ptinv#, m.franchise_code, m.ptcode 
order by ptinv#   


-- this looks potentially interesting, but the glptrns amount looks like it is doubled  
-- try grouping the base query, to generate a single row per ro, but i need to 
-- clarify with jeri what pay types we are looking at
-- for now, eliminate cp
select m.ptinv#, m.franchise_code as vehicle, sum(m."parts amt") as "parts amt",
  -1 * sum(case when trim(n.gtacct) = '124200' then gttamt else 0 end) as "124200",
  -1 * sum(case when trim(n.gtacct) = '124205' then gttamt else 0 end) as "124205"
from (-- part is OT, stock grp <> 920 and paytype not CP
  select a.ptinv#, b.franchise_code,
    sum(a.ptqty * a.ptnet) as "parts amt"
  -- select *
  from rydedata.pdptdet a
  inner join rydedata.sdprhdr b on trim(a.ptinv#) = trim(b.ptro#)
  where a.ptmanf = 'OT'
    and left(trim(a.ptinv#), 2) = '18'
    and a.ptdate > 20141100
    and a.ptcode in ('IS','SC','WS')
    and trim(ptsgrp) <> '920'
  group by a.ptinv#, b.franchise_code) m  
left join rydedata.glptrns n on trim(m.ptinv#) = trim(n.gtctl#)
  and trim(n.gtacct) in ('124200', '124205')
group by m.ptinv#, m.franchise_code
order by ptinv#     


select m.ptinv#, m.franchise_code as vehicle, sum(m."parts amt") as "parts amt",
  -1 * sum(case when trim(n.gtacct) = '124200' then gttamt else 0 end) as "124200",
  -1 * sum(case when trim(n.gtacct) = '124205' then gttamt else 0 end) as "124205"
from (-- part is OT, stock grp <> 920 and paytype only cp
  select a.ptinv#, b.franchise_code,
    sum(a.ptqty * a.ptnet) as "parts amt"
  -- select *
  from rydedata.pdptdet a
  inner join rydedata.sdprhdr b on trim(a.ptinv#) = trim(b.ptro#)
  where a.ptmanf = 'OT'
    and left(trim(a.ptinv#), 2) = '18'
    and a.ptdate > 20141100
    and a.ptcode in ('CP')
    and trim(ptsgrp) <> '920'
  group by a.ptinv#, b.franchise_code) m  
left join rydedata.glptrns n on trim(m.ptinv#) = trim(n.gtctl#)
  and trim(n.gtacct) in ('124200', '124205')
group by m.ptinv#, m.franchise_code
order by ptinv#  