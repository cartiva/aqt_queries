--LKPRT# : Part Nbr
--LKLINKC : Link Code
-- all links by Manufacturer, PartNbr
select * 
from pdplink
order by lkmanf, lkprt#
-- all links by Link Code
select * 
from pdplink
order by lklinkc
-- all links by Part Nbr
select *
from pdplink 
order by lkprt#

-- per Lud, 12/11/09
update pdplink
set lkco# = 'ZZZ'
where trim(lklinkc) in('OTCOOLANT','OTENGHTR')

--Ghost ??? 026784 showing link bu no info


Select * from RYDEDATA.PDPLINK
where trim(lklinkc) = 'HN23565R18'
order by lkprt#

select lklinkc, lkprt#
from pdplink
group by lklinkc, lkprt#
having count(*) > 1
order by lklinkc, lkprt#

-- this helped to isolate most of the problems
select *
from pdplink
where lklinkc in (
	select lklinkc
	from pdplink
	group by lklinkc, lkprt#
	having count(*) > 1)
and lkprt# in (
	select lkprt#
	from pdplink
	group by lklinkc, lkprt#
	having count(*) > 1)	
and lkco# <> 'ZZZ'	
order by lklinkc, lkprt#	

select *
from pdplink
where trim(lklinkc) = 'HN22560R16'

select *
from pdplink
where trim(lklinkc) in ('OT245/65R17','HN22560R16')
and lkco# <> 'ZZZ'
order by lklinkc, lkprt#
