-- limit to inventory in april (date_in_invent)

select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
  b.num_field_value as ad_pack_amount
from rydedata.inpmast a 
inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
inner join rydedata.inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.status = 'I'
  and b.seq_number = 14
and  date_in_invent < 20170701


